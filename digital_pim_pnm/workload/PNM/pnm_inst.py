import sys
import numpy as np
import pickle
import copy

np.random.seed(0)

nChannel = 4
nRank = 4
nBankGroup = 2
nBank = 4
nRow = 2**5
nColumn = 2**10

# 
prefetch = 1
channel_width = 64
dq = 16
# 4 * 16 => 64 dat
nDat = 16
# 2 byte per dat
dataBytes = 4
register_size = 48

# 
nColumn = int(nColumn / prefetch / channel_width * dq)

# 128 byte

# 10 => 1024
bitRow = int(np.log2(nRow))                 # 5
# 7 => 128
bitColumn = int(np.log2(nColumn))           # 3
# 6 => 64
bitBankGroup = int(np.log2(nBankGroup))     # 1
# 4 => 16
bitBank = int(np.log2(nBank))               # 2
# 2 => 4
bitChannel = int(np.log2(nChannel))         # 2
bitRank = int(np.log2(nRank))               # 2

bitArr = [bitRow, bitColumn, bitBankGroup, bitBank, bitChannel, bitRank]

def vector_to_byte_addr(addr_vec):
    addr = 0
    for ind in range(len(addr_vec)):
        addr *= (1 << bitArr[ind])
        addr += addr_vec[ind]
    addr *= nDat * dataBytes
    return addr

def byte_addr_to_vector(addr):
    addr_vec = [0, 0, 0, 0, 0, 0]
    addr /= (nDat * dataBytes)
    addr = int(addr)
    for ind in range(len(addr_vec)):
        addr_vec[len(addr_vec) - ind - 1] = addr % (1 << bitArr[len(addr_vec) - ind - 1])
        addr /= (1 << bitArr[len(addr_vec) - ind - 1])
        addr = int(addr)
    return addr_vec


# Define the example embedding indirection table here
# Table Size
# Table Base = (ro, co, bg, ba, ch, ra)
memory = [[[[[[[0 for _ in range(nDat)]
                for _ in range(nRank)]
                for _ in range(nChannel)]
                for _ in range(nBank)]
                for _ in range(nBankGroup)]
                for _ in range(nColumn)]
                for _ in range(nRow)]
register = [[[[0 for _ in range(nDat)]
                for _ in range(register_size)] 
                for _ in range(nRank)]
                for _ in range(nChannel)]

#### The spec of the index and 
idxBase = [31, 0, 0, 0, 0, 0]
idxMax  = [32, 0, 0, 0, 0, 0]
idxSize = vector_to_byte_addr(idxMax) - vector_to_byte_addr(idxBase)

# Gather idxBase
tabBase = [0, 0, 0, 0, 0, 0]
tabMax  = [16, 0, 0, 0, 0, 0]
tabSize = vector_to_byte_addr(tabMax) - vector_to_byte_addr(tabBase)

# set the index
idxDat = np.random.randint(vector_to_byte_addr(tabBase) / (nChannel * nRank * nDat * dataBytes),
                           vector_to_byte_addr(tabMax) / (nChannel * nRank * nDat * dataBytes), 
                           size=idxSize)
idxDat = idxDat * (nChannel * nRank * nDat * dataBytes)

tabDat = np.random.randint(0, 100, size=tabSize)

# gather op outputBase
gthBase = [16, 0, 0, 0, 0, 0]
gthMax = [24, 0, 0, 0, 0, 0]


redOutBase = [24, 0, 0, 0, 0, 0]
redOutMax = [28, 0, 0, 0, 0, 0]

avgOutBase = [28, 0, 0, 0, 0, 0]
avgOutMax = [32, 0, 0, 0, 0, 0]


numUsedChannel = 0
numUsedRank = 0
numUsedRow = 0
inst = 0

f = open("pnm.trace", "w")
resultDict = {}

def setDict(dat):
    global inst
    global resultdict

    copy_dat = copy.deepcopy(dat)
    resultDict[inst] = copy_dat
    inst += 1


## initialize the index array
def idxInit():
    global inst
    global resultDict

    for addr in range(vector_to_byte_addr(idxBase), vector_to_byte_addr(idxMax), nDat * dataBytes):
        relative_addr = int((addr - vector_to_byte_addr(idxBase)) / dataBytes)
        f.write("WR" + ' 0x{0:08X}'.format(addr))
        addr_vec = byte_addr_to_vector(addr)

        ind = 0
        for val in idxDat[relative_addr:relative_addr+nDat]:
            #f.write(" " + str(val))

            # Actually set the data to the memory
            memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]][ind] = val
            ind += 1
        f.write("\n")
        setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])

## initialize the table
def tabInit():
    global inst
    global resultDict

    for addr in range(vector_to_byte_addr(tabBase), vector_to_byte_addr(tabMax), nDat * dataBytes):
        relative_addr = int((addr - vector_to_byte_addr(tabBase)) / dataBytes)
        f.write("WR" + ' 0x{0:08X}'.format(addr))
        addr_vec = byte_addr_to_vector(addr)

        ind = 0
        for val in tabDat[relative_addr:relative_addr+nDat]:
            #f.write(" " + str(val))

            # Actually set the data to the memory
            memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]][ind] = val
            ind += 1
        f.write("\n")
        setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])


def gatherOp(count):
    global inst
    global resultDict

    # Iterate over the count
    for c in range(count):
        # Access the indirection memory every nDat
        if (c % nDat == 0):
            addr = vector_to_byte_addr(idxBase) + c * nDat * dataBytes
            f.write("RD" + ' 0x{0:08X}'.format(addr) + "\n")
            
            # retrieve the proper data
            addr_vec = byte_addr_to_vector(addr)
            data_arr = memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]]

            setDict(data_arr)

        # Iterate over all the tid
        for channel in range(nChannel):
            for rank in range(nRank):
                tid = (channel * nRank + rank) * nDat * dataBytes

                addr = data_arr[c % nDat] + tid

                regAddr = (c % register_size) * nDat * dataBytes

                # Read register instruction
                tableAddr = addr + vector_to_byte_addr(tabBase)
                addr_vec = byte_addr_to_vector(tableAddr)

                f.write("RG" + ' 0x{0:08X} 0x{1:08X}\n'.format(tableAddr, regAddr))

                # set the register file
                register[channel][rank][int(regAddr / nDat / dataBytes)] = memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]]
                
                setDict(register[channel][rank][int(regAddr / nDat / dataBytes)])

                gthAddr = c * nChannel * nRank * nDat * dataBytes + vector_to_byte_addr(gthBase) + tid
                addr_vec = byte_addr_to_vector(gthAddr)

                # Write register instruction
                f.write("WG" + ' 0x{0:08X} 0x{1:08X}\n'.format(gthAddr, regAddr))

                # read and set the memory
                memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]] = register[channel][rank][int(regAddr / nDat / dataBytes)]

                setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])

                # Added for validation
                f.write("RD" + ' 0x{0:08X}\n'.format(gthAddr))

                # read and set the memory
                setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])


# assume same count as the gather operation
def reduceOp(count):
    global inst
    global resultdict
    assert(count % 2 == 0)
    count_half = int(count / 2)
    for c in range(count_half):
        count_upper = count_half + c
        count_lower = c
        nodeDim = nChannel * nRank

        inputBase1 = count_upper * nChannel * nRank * nDat * dataBytes
        inputBase2 = count_lower * nChannel * nRank * nDat * dataBytes
        outputBase = c * nChannel * nRank * nDat * dataBytes

        for channel in range(nChannel):
            for rank in range(nRank):
                tid = (channel * nRank + rank) * nDat * dataBytes
                regAddr1 = ((c * 3) % register_size) * nDat * dataBytes
                regAddr2 = ((c * 3 + 1) % register_size) * nDat * dataBytes
                regAddr3 = ((c * 3 + 2) % register_size) * nDat * dataBytes

                idxAddr1 = inputBase1 + vector_to_byte_addr(gthBase) + tid
                addr_vec1 = byte_addr_to_vector(idxAddr1)
                
                idxAddr2 = inputBase2 + vector_to_byte_addr(gthBase) + tid
                addr_vec2 = byte_addr_to_vector(idxAddr2)

                f.write("RG" + ' 0x{0:08X} 0x{1:08X}\n'.format(idxAddr1, regAddr1))
                register[channel][rank][int(regAddr1 / nDat / dataBytes)] = memory[addr_vec1[0]][addr_vec1[1]][addr_vec1[2]][addr_vec1[3]][addr_vec1[4]][addr_vec1[5]]

                setDict(register[channel][rank][int(regAddr1 / nDat / dataBytes)])

                f.write("RG" + ' 0x{0:08X} 0x{1:08X}\n'.format(idxAddr2, regAddr2))
                register[channel][rank][int(regAddr2 / nDat / dataBytes)] = memory[addr_vec2[0]][addr_vec2[1]][addr_vec2[2]][addr_vec2[3]][addr_vec2[4]][addr_vec2[5]]

                setDict(register[channel][rank][int(regAddr2 / nDat / dataBytes)])

                f.write("AR ADD" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} 0x{3:08X}\n'.format(tid, regAddr1, regAddr2, regAddr3))
                reg1 = register[channel][rank][int(regAddr1 / nDat / dataBytes)]
                reg2 = register[channel][rank][int(regAddr2 / nDat / dataBytes)]
                register[channel][rank][int(regAddr3 / nDat / dataBytes)] = [a + b for a, b in zip(reg1, reg2)]

                setDict(register[channel][rank][int(regAddr3 / nDat / dataBytes)])

                redAddr = outputBase + vector_to_byte_addr(redOutBase) + tid
                addr_vec = byte_addr_to_vector(redAddr)
                # Write register instruction
                f.write("WG" + ' 0x{0:08X} 0x{1:08X}\n'.format(redAddr, regAddr3))
                # read and set the memory
                memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]] = register[channel][rank][int(regAddr3 / nDat / dataBytes)]

                setDict(register[channel][rank][int(regAddr3 / nDat / dataBytes)])

                # Added for validation
                f.write("RD" + ' 0x{0:08X}\n'.format(redAddr))
                # read and set the memory

                setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])


def averageOp(count, averageNum):
    global inst
    global resultdict
    for c in range(count):
        for channel in range(nChannel):
            for rank in range(nRank):
                tid = (channel * nRank + rank) * nDat * dataBytes
                
                # Perform set operation
                regAddr3 = (c % register_size) * nDat * dataBytes
                f.write("SE" + ' 0x{0:08X} 0x{1:08X}'.format(tid, regAddr3))
                for ind in range(nDat):
                    register[channel][rank][int(regAddr3 / nDat / dataBytes)][ind] = 0
                    #f.write(" " + str(0))
                f.write("\n")

                setDict(register[channel][rank][int(regAddr3 / nDat / dataBytes)])
                
                for num in range(averageNum):
                    dataBase = (c * averageNum + num) * nChannel * nRank * nDat * dataBytes
                    redAddr = dataBase + vector_to_byte_addr(redOutBase) + tid
                    addr_vec = byte_addr_to_vector(redAddr)
                    # make sure that regAddr3 is reserved!
                    regAddr1 = ((c * averageNum + num) % (register_size - 1) + 1) * nDat * dataBytes

                    f.write("RG" + ' 0x{0:08X} 0x{1:08X}\n'.format(redAddr, regAddr1))
                    register[channel][rank][int(regAddr1 / nDat / dataBytes)] = memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]]

                    setDict(register[channel][rank][int(regAddr1 / nDat / dataBytes)])

                    f.write("AR ADD" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} 0x{3:08X}\n'.format(tid, regAddr1, regAddr3, regAddr3))
                    reg1 = register[channel][rank][int(regAddr1 / nDat / dataBytes)]
                    reg2 = register[channel][rank][int(regAddr3 / nDat / dataBytes)]
                    register[channel][rank][int(regAddr3 / nDat / dataBytes)] = [a + b for a, b in zip(reg1, reg2)]

                    setDict(register[channel][rank][int(regAddr3 / nDat / dataBytes)])

                f.write("AI DIV" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X}'.format(tid, regAddr3, regAddr3))
                for ind in range(nDat):
                    register[channel][rank][int(regAddr3 / nDat / dataBytes)][ind] = register[channel][rank][int(regAddr3 / nDat / dataBytes)][ind] / averageNum
                    #f.write(" " + str(averageNum))
                f.write("\n")

                setDict(register[channel][rank][int(regAddr3 / nDat / dataBytes)])

                avgAddr = c * nChannel * nRank * nDat * dataBytes + vector_to_byte_addr(avgOutBase) + tid
                addr_vec = byte_addr_to_vector(avgAddr)
                f.write("WG" + ' 0x{0:08X} 0x{1:08X}\n'.format(avgAddr, regAddr3))

                memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]] = register[channel][rank][int(regAddr3 / nDat / dataBytes)]

                setDict(memory[addr_vec[0]][addr_vec[1]][addr_vec[2]][addr_vec[3]][addr_vec[4]][addr_vec[5]])


if len(sys.argv) < 2:
    print("[PNM Error]: Please use the correct format as follows")
    print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
    print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
    exit()

elif sys.argv[1] == "Gen":
    idxInit()
    tabInit()
    gatherOp(128)   # 128 dat
    reduceOp(128)   # 128 -> 64 dat
    averageOp(8, 8) # 64 dat -> 8 dat
    #MVM()
    f.close()

    save_file = open("resultDict.pkl", "wb")
    pickle.dump(resultDict, save_file)
    save_file.close()

elif sys.argv[1] == "IR":
    if len(sys.argv) != 3:
        print("[PNM Error]: Please use the correct format as follows")
        print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
        print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
        exit()

    ir_file = open(sys.argv[2], "r")
    ir_lines = ir_file.readlines()

    for ir_inst in ir_lines:
        ir_source = ir_inst
        ir_inst = ir_inst.strip()
        ir_inst = ir_inst[4:]

        if ir_inst[:10] == "vector_imm":
            ir_inst = ir_inst[11:-1].split(", ")

            addr = ir_inst[1].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([row, col, bank_group, bank, channel, rank])
            reg_addr = 0
            f.write("RG" + ' 0x{0:08X} 0x{1:08X}'.format(byte_addr, reg_addr))
            f.write("\n")

            tid = (channel * nRank + rank) * nDat * dataBytes
            reg_addr3 = 0
            f.write("AI MUL" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} '.format(tid, reg_addr3, reg_addr3) + ir_source)

        elif ir_inst[:10] == "vector_acc":
            ir_inst = ir_inst[11:-1].split(", ")

            addr = ir_inst[0].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([row, col, bank_group, bank, channel, rank])
            reg_addr = 0
            f.write("RG" + ' 0x{0:08X} 0x{1:08X}'.format(byte_addr, reg_addr))
            f.write("\n")

            tid = (channel * nRank + rank) * nDat * dataBytes
            reg_addr3 = 0
            f.write("AR ADD" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} 0x{3:08X} '.format(tid, 0, 0, reg_addr3) + ir_source)

        elif ir_inst[:6] == "vector":
            ir_inst = ir_inst[7:-1].split(", ")

            addr = ir_inst[0].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([row, col, bank_group, bank, channel, rank])
            reg_addr = 0
            f.write("RG" + ' 0x{0:08X} 0x{1:08X}'.format(byte_addr, reg_addr))
            f.write("\n")

            addr = ir_inst[1].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([row, col, bank_group, bank, channel, rank])
            reg_addr = 0
            f.write("RG" + ' 0x{0:08X} 0x{1:08X}'.format(byte_addr, reg_addr))
            f.write("\n")

            tid = (channel * nRank + rank) * nDat * dataBytes
            reg_addr3 = 0
            f.write("AR MUL" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} 0x{3:08X} '.format(tid, 0, 0, reg_addr3) + ir_source)

        else:
            assert(False)


else:
    print("[PNM Error]: Please use the correct format as follows")
    print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
    print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
    exit()
