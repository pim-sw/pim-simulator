import sys
import numpy as np
import pickle

np.random.seed(0)

nChannel = 8
nRank = 1
nBankGroup = 4
nBank = 4
nRow = 2**5
nColumn = 2**3
# 4 * 16 => 64 dat
nDat = (2**2) * (2**4)
# 2 byte per dat
dataBytes = 2

register_size = nColumn

# 128 byte

bitChannel = int(np.log2(nChannel))
bitRank = int(np.log2(nRank))
bitRow = int(np.log2(nRow))
bitBankGroup = int(np.log2(nBankGroup))
bitBank = int(np.log2(nBank))
bitColumn = int(np.log2(nColumn))

bitArr = [bitChannel, bitRank, bitRow, bitBankGroup, bitBank, bitColumn]

def vector_to_byte_addr(addr_vec):
    addr = 0
    for ind in range(len(addr_vec)):
        addr *= (1 << bitArr[ind])
        addr += addr_vec[ind]
    addr = addr * (nDat * dataBytes)
    return addr

#addrOffsetChannel = bitRank + bitRow + bitBankGroup + bitBank + bitColumn
#addrOffsetRank = bitRow + bitBankGroup + bitBank + bitColumn
#addrOffsetRow = bitBankGroup + bitBank + bitColumn
#addrOffsetBankGroup = bitBank + bitColumn
#addrOffsetBank = bitColumn

inSize = 512            # nColumn * nDat
outSize = 1024

numUsedChannel = 0
numUsedRank = 0
numUsedRow = 0

weight = np.random.randint(-8, 8, size=(outSize, inSize))
act = np.random.randint(-8, 8, size=inSize)

inst = 0

assert(outSize <= nChannel * nRank * nBankGroup * nBank * nRow)

f = open("dpim.trace", "w")
resultDict = {}

def dramWrite():
    global numUsedChannel
    global numUsedRank
    global numUsedRow
    global inst

    numWrite = 0
    for row in range(nRow):
        if row + 1 > numUsedRow:
            numUsedRow = row + 1
        for channel in range(nChannel):
            if channel + 1 > numUsedChannel:
                numUsedChannel = channel + 1
            for rank in range(nRank):
                if rank + 1 > numUsedRank:
                    numUsedRank = rank + 1
                for bankGroup in range(nBankGroup):
                    for bank in range(nBank):
                        for column in range(nColumn):
                            addr_vec = [channel, rank, row, bankGroup, bank, column]
                            address_dec = vector_to_byte_addr(addr_vec)

                            f.write("WR" + ' 0x{0:08X}'.format(address_dec))
                            #for val in weight[numWrite][column*nDat:(column+1)*nDat]:
                            #    f.write(" " + str(val))
                            f.write("\n")
                            resultDict[inst] = weight[numWrite][column*nDat:(column+1)*nDat]
                            inst += 1

                        numWrite += 1
                        if numWrite == outSize:
                            return

def dramRead():
    global inst
    global resultDict

    numRead = 0
    for row in range(nRow):
        for channel in range(nChannel):
            for rank in range(nRank):
                for bankGroup in range(nBankGroup):
                    for bank in range(nBank):
                        for column in range(nColumn):
                            addr_vec = [channel, rank, row, bankGroup, bank, column]
                            address_dec = vector_to_byte_addr(addr_vec)
                            f.write("RD" + ' 0x{0:08X}'.format(address_dec) + "\n")
                            resultDict[inst] = weight[numRead][column*nDat:(column+1)*nDat]
                            inst += 1

                        numRead += 1
                        if numRead == outSize:
                            return

def MVM():
    global numUsedChannel
    global numUsedRank
    global numUsedRow
    global inst

    for channel in range(numUsedChannel):
        for rank in range(numUsedRank):
            addr_vec = [channel, rank, 0, 0, 0, 0]
            address_dec = vector_to_byte_addr(addr_vec)

            for column in range(nColumn):
                regAddr3 = (column % register_size) * nDat * dataBytes
                f.write("SE" + ' 0x{0:08X} 0x{1:08X}'.format(address_dec, regAddr3))
                #for val in act[column*nDat:(column+1)*nDat]:
                #    f.write(" " + str(val))
                f.write("\n")

                resultDict[inst] = act[column*nDat:(column+1)*nDat]
                inst += 1

    parallelism = nBankGroup * nBank
    numMVM = 0
    for row in range(numUsedRow):
        for channel in range(numUsedChannel):
            for rank in range(numUsedRank):
                addr_vec = [channel, rank, row, 0, 0, 0]
                address_dec = vector_to_byte_addr(addr_vec)
                f.write("MV" +' 0x{0:08X}'.format(address_dec) +"\n")
                resultDict[inst] = weight[parallelism*numMVM:parallelism*(numMVM+1)] @ act
                inst += 1

                numMVM += 1
                if numMVM == np.ceil(outSize / parallelism):
                    return


if len(sys.argv) < 2:
    print("[Digital PIM Error]: Please use the correct format as follows")
    print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
    print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
    exit()

elif sys.argv[1] == "Gen":
    dramWrite()
    dramRead()
    MVM()
    f.close()
    save_file = open("resultDict.pkl", "wb")
    pickle.dump(resultDict, save_file)
    save_file.close()

elif sys.argv[1] == "IR":
    if len(sys.argv) != 3:
        print("[Digital PIM Error]: Please use the correct format as follows")
        print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
        print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
        exit()

    ir_file = open(sys.argv[2], "r")
    ir_lines = ir_file.readlines()

    for ir_inst in ir_lines:
        ir_source = ir_inst
        ir_inst = ir_inst.strip()
        ir_inst = ir_inst[5:]

        if ir_inst[:4] == "load":
            ir_inst = ir_inst[5:-1].split(", ")

            addr = ir_inst[0].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([channel, rank, row, bank_group, bank, col])
            f.write("RG" + ' 0x{0:08X} 0x{1:08X} '.format(byte_addr, 0) + ir_source)
            #f.write("RG" + ' 0x{0:08X}'.format(byte_addr))
            #f.write("\n")

        elif ir_inst[:5] == "store":
            ir_inst = ir_inst[6:-1].split(", ")

            addr = ir_inst[0].split()
            channel = int(addr[2])
            rank = int(addr[4])
            bank_group = int(addr[6])
            bank = int(addr[8])
            row = int(addr[10])
            col = int(addr[12])

            byte_addr = vector_to_byte_addr([channel, rank, row, bank_group, bank, col])
            f.write("WG" + ' 0x{0:08X} 0x{1:08X} '.format(byte_addr, 0) + ir_source)
            #f.write("WR" + ' 0x{0:08X}'.format(byte_addr))
            #f.write("\n")

        elif ir_inst[:10] == "vector_acc":
            # dpim.vector_acc(x: ch 1 ra 0 bg 2 ba 2 ro 29 co 2, 16)
            ir_inst = ir_inst[11:-1].split(", ")
            addr = ir_inst[0].split()

            channel = int(addr[2])
            rank = int(addr[4])

            byte_addr = vector_to_byte_addr([channel, rank, 0, 0, 0, 0])
            reg_addr3 = 0
            f.write("AR ADD" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} 0x{3:08X} '.format(byte_addr, reg_addr3, reg_addr3, reg_addr3) + ir_source)


        elif ir_inst[:6] == "vector":
            ir_inst = ir_inst[7:-1].split(", ")

            addr = ir_inst[1].split()
            channel = int(addr[2])
            rank = int(addr[4])
            row = int(addr[10])

            byte_addr = vector_to_byte_addr([channel, rank, 0, 0, 0, 0])
            reg_addr3 = 0
            f.write("AI MUL" + ' 0x{0:08X} 0x{1:08X} 0x{2:08X} '.format(byte_addr, reg_addr3, reg_addr3) + ir_source)
            #f.write("AI MUL" + ' 0x{0:08X} 0x{1:08X}\n'.format(byte_addr, reg_addr3))
            #f.write("SE" + ' 0x{0:08X} 0x{1:08X}'.format(byte_addr, reg_addr3))
            #f.write("\n")

            #byte_addr = vector_to_byte_addr([channel, rank, row, 0, 0, 0])
            #f.write("MV" +' 0x{0:08X}'.format(byte_addr))
            #f.write("\n")

        else:
            assert(False)

else:
    print("[Digital PIM Error]: Please use the correct format as follows")
    print("  Generate the trace from scratch : \"python", sys.argv[0], "Gen\"")
    print("  Generate the trace from IR      : \"python", sys.argv[0], "IR <.trace file>\"")
    exit()
