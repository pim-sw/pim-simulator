import numpy as np
import pickle
import ast
import sys

f = open("./result/result.txt", "r")
save_file = open("resultDict.pkl", "rb")
ref = pickle.load(save_file)

result = f.readlines()
ind = 0
for res in result:
    res = res.split(" ")
    inst_id = int(res[0])
    inst_res = ast.literal_eval(res[1])

    ref_np_arr = np.asarray(ref[inst_id])
    inst_np_arr = np.asarray(inst_res)

    if( not (ref_np_arr == inst_np_arr).all()): 
        print(inst_id)
        print(inst_np_arr, ref_np_arr)
        sys.stdout.flush()
        assert(0)
        

print("SUCCESS")
