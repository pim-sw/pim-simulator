#!/bin/sh

if [[ $1 == "DPIM" ]]; then
    ./ramulator_pim configs/HBM_PIM-config.cfg ./workload/DPIM/dpim.trace
elif [[ $1 = "PNM" ]]; then
    ./ramulator_pim configs/DDR4_PNM-config.cfg ./workload/PNM/pnm.trace
fi
