#ifndef __PROCESSOR_H
#define __PROCESSOR_H

#include "Cache.h"
#include "Config.h"
#include "Memory.h"
#include "Request.h"
#include "Statistics.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <ctype.h>
#include <functional>

namespace ramulator 
{

class Trace {
public:
    Trace(const char* trace_fname);
    bool get_dramtrace_request(long& reqAddr, int& regAddr1, int& regAddr2, int& regAddr3
                               , Request::Type& reqType, Request::ALUType& aluType
                               , vector<double>& data
                               , string& source_code);

    long expected_limit_insts = 0;

private:
    std::ifstream file;
    std::string trace_name;
};

}
#endif /* __PROCESSOR_H */
