#ifndef __REQUEST_H
#define __REQUEST_H

#include <vector>
#include <queue>
#include <functional>
#include <fstream>

using namespace std;

namespace ramulator
{

class Request
{
public:
    bool is_first_command;  // this request has yet to issue any command
    // Three different addresses for each sub commands

    long addr = 0;          // inputBase
    // long addr_row;
    vector<int> addr_vec;

    vector<int> addr_bits;  // address bits for debugging purpose
    // specify which core this request sent from, for virtual address translation
    int coreid;
    int instid = -1;

    // PIM FIX
    // add the requests here
    enum class Type
    {
        READ,
        WRITE,
        REFRESH,
        POWERDOWN,
        SELFREFRESH,
        MVM,            // perform MVM operation using the global buffer & bank memory
        RDREG,
        WRREG,
        ALUR,
        ALUI,
        SET,
        EXTENSION,
        MAX
    } type;

    enum class ALUType
    {
        ADD,
        SUB,
        MUL,
        DIV,
        MAX
    } aluType;

    vector<double> data;

    long regAddr1;
    long regAddr2;
    long regAddr3;

    long arrive = -1;
    long issued = -1;
    long depart = -1;
    long returned = -1;

    string source_code = "";


    // return void, input Request&
    function<bool(Request&)> callback; // call back with more info

    bool useReg() {
        return type == Type::WRREG || type == Type::RDREG || type == Type::ALUR || type == Type::ALUI || type == Type::SET;
    }

    void printDat() {
        cout << "DATA " << instid << ": ";
        for (auto dat : data)
            cout << dat << " ";
        cout << endl;
    }

    void printAddr() {
        cout << "ADDR " << instid << ": ";
        for (auto addr : addr_vec)
            cout << addr << " ";
        cout << endl;
    }

    Request(Type type, long addr, int coreid = 0)
        : is_first_command(true), addr(addr), type(type)
        , coreid(coreid), callback([](Request& req)->bool{}) {}

    Request(Type type, function<bool(Request&)> callback
            , long addr, int coreid = 0)
        : is_first_command(true), addr(addr), type(type), callback(callback), coreid(coreid) {}

    Request(Type type, function<bool(Request&)> callback, vector<int>& addr_vec
            , int coreid = 0)
        : is_first_command(true), addr_vec(addr_vec), type(type), callback(callback), coreid(coreid) {}

    Request()
        : is_first_command(true), coreid(0) {}

private:
    int slice_lower_bits(long& addr, int bits)
    {
        int lbits = addr & ((1<<bits) - 1);
        addr >>= bits;
        return lbits;
    }
};

} /*namespace ramulator*/

#endif /*__REQUEST_H*/

