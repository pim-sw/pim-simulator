#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include <cassert>
#include <cstdio>
#include <deque>
#include <fstream>
#include <list>
#include <string>
#include <vector>
#include <limits>


#include "Config.h"
#include "DRAM.h"
#include "Refresh.h"
#include "Request.h"
#include "Scheduler.h"
#include "Statistics.h"

using namespace std;

namespace ramulator
{

    extern bool warmup_complete;


template <typename T>
class Controller
{
protected:
    // For counting bandwidth
    ScalarStat read_transaction_bytes;
    ScalarStat write_transaction_bytes;

    ScalarStat row_hits;
    ScalarStat row_misses;
    ScalarStat row_conflicts;
    VectorStat read_row_hits;
    VectorStat read_row_misses;
    VectorStat read_row_conflicts;
    VectorStat write_row_hits;
    VectorStat write_row_misses;
    VectorStat write_row_conflicts;
    ScalarStat useless_activates;

    ScalarStat read_latency_sum;



public:
    /* Member Variables */
    long clk = 0;
    DRAM<T>* channel;
    double** mem_dat;
    //double* global_buf;
    double*** register_file;

    int num_bg;
    int num_bank;
    int num_col;
    int num_dat;
    int* map_index;
    int* map_num_dat;

    // if the controller is computing -> other comps cannot interfere
    // FIXME: Can compute only when act queue is empty
    bool is_computing = false;

    bool is_receiving = false;
    int receive_cycles = 0;
    bool is_sending = false;
    bool send_available = true;
    int send_cycles = 0;

    Scheduler<T>* scheduler;  // determines the highest priority request whose commands will be issued
    RowPolicy<T>* rowpolicy;  // determines the row-policy (e.g., closed-row vs. open-row)
    RowTable<T>* rowtable;  // tracks metadata about rows (e.g., which are open and for how long)
    Refresh<T>* refresh;  // refresh control

    struct Queue {
        list<Request> q;
        unsigned int max = 32;
        unsigned int size() {return q.size();}
    };

    // PIM FIX: all the other operations including the global activation / read / gwrite / readres in readq
    Queue readq;  // queue for read requests
    Queue writeq;  // queue for write requests
    Queue actq; // read and write requests for which activate was issued are moved to
                   // actq, which has higher priority than readq and writeq.
                   // This is an optimization
                   // for avoiding useless activations (i.e., PRECHARGE
                   // after ACTIVATE w/o READ of WRITE command)
    Queue otherq;  // queue for all "other" requests (e.g., refresh)

    list<Request> pending;  // read requests that are about to receive data from DRAM
    bool write_mode = false;  // whether write requests should be prioritized over reads
    float wr_high_watermark = 0.8f; // threshold for switching to write mode
    float wr_low_watermark = 0.2f; // threshold for switching back to read mode
    //long refreshed = 0;  // last time refresh requests were generated

    /* Command trace for DRAMPower 3.1 */
    string cmd_trace_prefix = "cmd-trace-";
    vector<ofstream> cmd_trace_files;
    bool record_cmd_trace = false;
    /* Commands to stdout */
    bool print_cmd_trace = false;

    enum class QueueType {
        READQ,
        WRITEQ,
        OTHERQ
    };

    /* Constructor */
    Controller(const Config& configs, DRAM<T>* channel) :
        channel(channel),
        scheduler(new Scheduler<T>(this)),
        rowpolicy(new RowPolicy<T>(this)),
        rowtable(new RowTable<T>(this)),
        refresh(new Refresh<T>(this)),
        cmd_trace_files(channel->children.size())
    {
        record_cmd_trace = configs.record_cmd_trace();
        print_cmd_trace = configs.print_cmd_trace();
        if (record_cmd_trace){
            if (configs["cmd_trace_prefix"] != "") {
              cmd_trace_prefix = configs["cmd_trace_prefix"];
            }
            string prefix = cmd_trace_prefix + "chan-" + to_string(channel->id) + "-rank-";
            string suffix = ".cmdtrace";
            for (unsigned int i = 0; i < channel->children.size(); i++)
                cmd_trace_files[i].open(prefix + to_string(i) + suffix);
        }

        // regStats

        row_hits
            .name("row_hits_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row hits per channel per core")
            .precision(0);
        row_misses
            .name("row_misses_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row misses per channel per core")
            .precision(0);
        row_conflicts
            .name("row_conflicts_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row conflicts per channel per core")
            .precision(0);

        read_row_hits
            .init(configs.get_core_num())
            .name("read_row_hits_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row hits for read requests per channel per core")
            .precision(0);
        read_row_misses
            .init(configs.get_core_num())
            .name("read_row_misses_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row misses for read requests per channel per core")
            .precision(0);
        read_row_conflicts
            .init(configs.get_core_num())
            .name("read_row_conflicts_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row conflicts for read requests per channel per core")
            .precision(0);

        write_row_hits
            .init(configs.get_core_num())
            .name("write_row_hits_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row hits for write requests per channel per core")
            .precision(0);
        write_row_misses
            .init(configs.get_core_num())
            .name("write_row_misses_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row misses for write requests per channel per core")
            .precision(0);
        write_row_conflicts
            .init(configs.get_core_num())
            .name("write_row_conflicts_channel_"+to_string(channel->id) + "_core")
            .desc("Number of row conflicts for write requests per channel per core")
            .precision(0);

        useless_activates
            .name("useless_activates_"+to_string(channel->id)+ "_core")
            .desc("Number of useless activations. E.g, ACT -> PRE w/o RD or WR")
            .precision(0);

        read_transaction_bytes
            .name("read_transaction_bytes_"+to_string(channel->id))
            .desc("The total byte of read transaction per channel")
            .precision(0);
        write_transaction_bytes
            .name("write_transaction_bytes_"+to_string(channel->id))
            .desc("The total byte of write transaction per channel")
            .precision(0);

        read_latency_sum
            .name("read_latency_sum_"+to_string(channel->id))
            .desc("The memory latency cycles (in memory time domain) sum for all read requests in this channel")
            .precision(0);

    }

    ~Controller(){
        delete scheduler;
        delete rowpolicy;
        delete rowtable;
        delete channel;
        delete refresh;
        for (auto& file : cmd_trace_files)
            file.close();
        cmd_trace_files.clear();
    }

    /* Member Functions */
    Queue& get_queue(Request::Type type)
    {
        // PIM FIX: we do not consider prioritizing for now
        switch (int(type)) {
            case int(Request::Type::READ): return readq;
            case int(Request::Type::MVM): return readq;
            case int(Request::Type::RDREG): return readq;
            case int(Request::Type::WRREG): return writeq;
            case int(Request::Type::ALUR): return readq;
            case int(Request::Type::ALUI): return readq;
            case int(Request::Type::SET): return readq;
            case int(Request::Type::WRITE): return writeq;
            default: return otherq;
        }
    }

    bool enqueue(Request& req)
    {
        Queue& queue = get_queue(req.type);
        if (queue.max == queue.size())
            return false;

        req.arrive = clk;
        queue.q.push_back(req);

        // shortcut for read requests, if a write to same addr exists
        // necessary for coherence
        if (req.type == Request::Type::READ) {
            // reverse search for the elements
            auto wreq = find_if(writeq.q.rbegin(), writeq.q.rend(),
                                     [req](Request& wreq){ return req.addr == wreq.addr; });
            // if there is no match => return
            if (wreq == writeq.q.rend()) return true;
            // if the matched element is WRREG => cannot retrieve the data
            if (wreq->type == Request::Type::WRREG) return true;

            req.depart = clk + 1;
            req.data = wreq->data;
            pending.push_back(req);

            // pop from the queue right away
            readq.q.pop_back();
        }
        return true;
    }

    void tick()
    {
        clk++;

        /*** 1. Serve completed reads ***/
        if (pending.size()) {
            // should be popped in the departure order
            auto head = pending.begin();
            for (auto itr = next(pending.begin(), 1); itr != pending.end(); itr++)
                if (head->depart > itr->depart)
                    head = itr;
            Request& req = *head;

            if (req.depart <= clk) {
                req.returned = clk;
                if (req.callback(req)) {
                    if (req.depart - req.arrive > 1) { // this request really accessed a row
                        read_latency_sum += req.depart - req.arrive;
                        channel->update_serving_requests(req, -1, clk);
                    }

                    // pop the pending queue
                    pending.erase(head);
                    //pending.pop_front();
                }
            }
        }

        /*** 2. Refresh scheduler ***/
        refresh->tick_ref();

        /*** 3. Should we schedule writes? ***/
        if (!write_mode) {
            // yes -- write queue is almost full or read queue is empty
            if (writeq.size() > int(wr_high_watermark * writeq.max) || readq.size() == 0)
                write_mode = true;
        }
        else {
            // no -- write queue is almost empty and read queue is not empty
            if (writeq.size() < int(wr_low_watermark * writeq.max) && readq.size() != 0)
                write_mode = false;
        }

        /*** 4. Find the best command to schedule, if any ***/

        // PIM FIX: Please prioritize comp command if GAct has been done

        // First check the actq (which has higher priority) to see if there
        // are requests available to service in this cycle
        Queue* queue = &actq;
        typename T::Command cmd;
        bool is_last = false;
        auto req = scheduler->get_head(queue->q);

        bool is_valid_req = (req != queue->q.end());


        // get and check the request in actq
        if(is_valid_req) {
            cmd = get_first_cmd(req, is_last);
            is_valid_req = is_ready(cmd, req);
        }


        // get and check the request in readq or writeq
        if (!is_valid_req) {

            //queue = !write_mode ? &readq : &writeq;
            //if (otherq.size())
            //    queue = &otherq;  // "other" requests are rare, so we give them precedence over reads/writes
            //req = scheduler->get_head(queue->q);
            //is_valid_req = (req != queue->q.end());

            if (otherq.size()) {
                queue = &otherq;  // "other" requests are rare, so we give them precedence over reads/writes
                req = scheduler->get_head(queue->q);
                is_valid_req = (req != queue->q.end());
            }
            else {
                queue = !write_mode ? &readq : &writeq;

                req = scheduler->get_head(queue->q);
                is_valid_req = (req != queue->q.end());

                if (is_valid_req) {
                    QueueType queueType = write_mode ? QueueType::WRITEQ : QueueType::READQ;
                    // If there is a dependency => try out other mode
                    if (is_dependent(req, queueType)) {

                        // reverse the write mode
                        write_mode = !write_mode;
                        queue = !write_mode ? &readq : &writeq;
                        req = scheduler->get_head(queue->q);
                        queueType = write_mode ? QueueType::WRITEQ : QueueType::READQ;

                        // Check dependency for the given mode (read mode or write mode)
                        is_valid_req = (req != queue->q.end());
                        if (is_valid_req) {
                            // This should never happen; there should be advancing ...
                            if (is_dependent(req, queueType)) assert(0 && "The queue cannot advance ...");
                        }
                        else assert(0 && "The queue cannot advance ...");
                    }
                }
            }

            if(is_valid_req){
                cmd = get_first_cmd(req, is_last);
                is_valid_req = is_ready(cmd, req);
            }
        }

        if (!is_valid_req) {
            // we couldn't find a command to schedule -- let's try to be speculative => force precharge command
            auto cmd = T::Command::PRE;
            vector<int> victim = rowpolicy->get_victim(cmd);
            if (!victim.empty()){
                issue_cmd(cmd, victim, *req, is_last);
            }
            return;  // nothing more to be done this cycle
        }


        if (req->is_first_command) {
            req->is_first_command = false;

            // These are for reporting purpose => leave this for now
            int coreid = req->coreid;
            if (req->type == Request::Type::READ ||
                req->type == Request::Type::RDREG ||
                req->type == Request::Type::WRITE ||
                req->type == Request::Type::WRREG ||
                req->type == Request::Type::ALUR ||
                req->type == Request::Type::ALUI ||
                req->type == Request::Type::SET ||
                req->type == Request::Type::MVM) {
                channel->update_serving_requests(*req, 1, clk);
            }
            int tx = (channel->spec->prefetch_size * channel->spec->channel_width / 8);
            if (req->type == Request::Type::READ) {
                if (is_row_hit(req)) {
                    ++read_row_hits[coreid];
                    ++row_hits;
                } else if (is_row_open(req)) {
                    ++read_row_conflicts[coreid];
                    ++row_conflicts;
                } else {
                    ++read_row_misses[coreid];
                    ++row_misses;
                }
              read_transaction_bytes += tx;
            } else if (req->type == Request::Type::WRITE) {
              if (is_row_hit(req)) {
                  ++write_row_hits[coreid];
                  ++row_hits;
              } else if (is_row_open(req)) {
                  ++write_row_conflicts[coreid];
                  ++row_conflicts;
              } else {
                  ++write_row_misses[coreid];
                  ++row_misses;
              }
              write_transaction_bytes += tx;
            }
        }

        // issue command on behalf of request
        issue_cmd(cmd, get_addr_vec(cmd, req), *req, is_last);

        // check whether this is the last command (which finishes the request)
        // if not check if the next command should be prioritized
        // i.e., it caused activation => this is currently not needed (reason: cmd == request for pim ops)
        if (cmd != channel->spec->translate[int(req->type)]) {
            // The compute-related requests are kept in the readq
            // FIXME: This disables Register using requests from entering actq
            if(channel->spec->is_opening(cmd) && !(req->useReg())) {
                // promote the request that caused issuing activation to actq
                actq.q.push_back(*req);
                queue->q.erase(req);
            }
            return;
        }

        // This is only for the last command!
        // set a future completion time for read requests
        if (req->type == Request::Type::READ ||
            req->type == Request::Type::MVM) {
            req->issued = clk;
            if(req->issued == -1) assert(0);
            req->depart = req->type == Request::Type::READ ?
                    clk + channel->spec->read_latency : clk + channel->spec->mvm_latency;
            pending.push_back(*req);
        }

        if (req->type == Request::Type::WRITE ||
            req->type == Request::Type::RDREG ||
            req->type == Request::Type::WRREG ||
            req->type == Request::Type::ALUR ||
            req->type == Request::Type::ALUI ||
            req->type == Request::Type::SET) {

            req->issued = clk;
            channel->update_serving_requests(*req, -1, clk);
            // perform call back function right away
            req->callback(*req);
        }



        // remove request from queue
        queue->q.erase(req);
    }

    bool is_dependent(list<Request>::iterator req, QueueType queueType)
    {
        if (queueType == QueueType::READQ) {
            // iterate over the writeq to search for the match in the queue
            // return the value only if the arrival time is slower than the matched request in the writeq

            // find if the address match and arrive faster
            auto wreq = find_if(writeq.q.rbegin(), writeq.q.rend(), [req](Request& wreq) {
                // if req == RD/RDREG => WD/WDREG => check if the memory address match
                if (req->type == Request::Type::READ || req->type == Request::Type::RDREG) {
                    if (wreq.addr == req->addr) {
                        return req->arrive > wreq.arrive;
                    }
                }
                // if req == RDREG's dest => WDREG's source  => check if the
                if (req->type == Request::Type::RDREG ||
                    req->type == Request::Type::ALUR ||
                    req->type == Request::Type::ALUI ||
                    req->type == Request::Type::SET) {

                    if (wreq.regAddr1 == req->regAddr3 && wreq.addr_vec[int(T::Level::Rank)] == req->addr_vec[int(T::Level::Rank)]) {
                        return req->arrive > wreq.arrive;
                    }
                }

                if (req->type == Request::Type::MVM) {
                    if (wreq.addr_vec[int(T::Level::Channel)] == req->addr_vec[int(T::Level::Channel)] ||
                        wreq.addr_vec[int(T::Level::Rank)] == req->addr_vec[int(T::Level::Rank)] ||
                        wreq.addr_vec[int(T::Level::Row)] == req->addr_vec[int(T::Level::Row)]) {
                        return req->arrive > wreq.arrive;
                    }
                }
                return false;
            });
            if (wreq != writeq.q.rend()) return true;


            return false;
        }
        else if (queueType == QueueType::WRITEQ) {
            // if the request is simple write request => there is no dependency
            if (req->type == Request::Type::WRITE) return false;
            // iterate over the writeq to search for the match in the queue
            // return the value if the matched data is
            // 1) regAddr3 of the ALU request
            // 2) regAddr3 of the READREG request

            auto rreq = find_if(readq.q.rbegin(), readq.q.rend(), [req](Request& rreq){
                if (rreq.type == Request::Type::ALUR || 
                    rreq.type == Request::Type::ALUI || 
                    rreq.type == Request::Type::SET || 
                    rreq.type == Request::Type::RDREG) {
                    
                    if (rreq.regAddr3 == req->regAddr1 &&
                        rreq.addr_vec[int(T::Level::Rank)] == req->addr_vec[int(T::Level::Rank)]) {
                        return req->arrive > rreq.arrive;
                    }
                }
                return false;
            });
            if (rreq != readq.q.rend()) return true;


            // if there is a match => return
            return false;
        }
        else assert(0);
    }

    bool is_ready(list<Request>::iterator req)
    {
        bool is_last_temp;
        typename T::Command cmd = get_first_cmd(req, is_last_temp);
        return channel->check(cmd, clk, *req);
    }

    bool is_ready(typename T::Command cmd, list<Request>::iterator req)
    {
        return channel->check(cmd, clk, *req);
    }

    bool is_ready(typename T::Command cmd, const vector<int>& addr_vec)
    {
        return channel->check(cmd, clk, addr_vec.data());
    }

    bool is_row_hit(list<Request>::iterator req)
    {
        // cmd must be decided by the request type, not the first cmd
        typename T::Command cmd = channel->spec->translate[int(req->type)];
        return channel->check_row_hit(cmd, *req);
    }

    //bool is_row_hit(typename T::Command cmd, const vector<int>& addr_vec)
    //{
    //    return channel->check_row_hit(cmd, addr_vec.data());
    //}

    bool is_row_open(list<Request>::iterator req)
    {
        // cmd must be decided by the request type, not the first cmd
        typename T::Command cmd = channel->spec->translate[int(req->type)];
        return channel->check_row_open(cmd, *req);
    }

    //bool is_row_open(typename T::Command cmd, const vector<int>& addr_vec)
    //{
    //    return channel->check_row_open(cmd, addr_vec.data());
    //}

    // For telling whether this channel is busying in processing read or write
    bool is_active() {
      return (channel->cur_serving_requests > 0);
    }

    // For telling whether this channel is under refresh
    bool is_refresh() {
      return clk <= channel->end_of_refreshing;
    }

    void set_high_writeq_watermark(const float watermark) {
       wr_high_watermark = watermark; 
    }

    void set_low_writeq_watermark(const float watermark) {
       wr_low_watermark = watermark;
    }


private:
    typename T::Command get_first_cmd(list<Request>::iterator req, bool& is_last)
    {
        typename T::Command cmd = channel->spec->translate[int(req->type)];
        return channel->decode(cmd, *req, is_last);
    }

    // upgrade to an autoprecharge command
    void cmd_issue_autoprecharge(typename T::Command& cmd,
                                            const vector<int>& addr_vec) {

        // currently, autoprecharge is only used with closed row policy
        if(channel->spec->is_accessing(cmd) && rowpolicy->type == RowPolicy<T>::Type::ClosedAP) {
            // check if it is the last request to the opened row
            Queue* queue = write_mode ? &writeq : &readq;

            auto begin = addr_vec.begin();
            vector<int> rowgroup(begin, begin + int(T::Level::Row) + 1);

			int num_row_hits = 0;

            for (auto itr = queue->q.begin(); itr != queue->q.end(); ++itr) {
                if (is_row_hit(itr)) { 
                    auto begin2 = itr->addr_vec.begin();
                    vector<int> rowgroup2(begin2, begin2 + int(T::Level::Row) + 1);
                    if(rowgroup == rowgroup2)
                        num_row_hits++;
                }
            }

            if(num_row_hits == 0) {
                Queue* queue = &actq;
                for (auto itr = queue->q.begin(); itr != queue->q.end(); ++itr) {
                    if (is_row_hit(itr)) {
                        auto begin2 = itr->addr_vec.begin();
                        vector<int> rowgroup2(begin2, begin2 + int(T::Level::Row) + 1);
                        if(rowgroup == rowgroup2)
                            num_row_hits++;
                    }
                }
            }

            assert(num_row_hits > 0); // The current request should be a hit, 
                                      // so there should be at least one request 
                                      // that hits in the current open row
            if(num_row_hits == 1) {
                if(cmd == T::Command::RD)
                    cmd = T::Command::RDA;
                else if (cmd == T::Command::WR)
                    cmd = T::Command::WRA;
                else
                    assert(false && "Unimplemented command type.");
            }
        }

    }

    void issue_cmd(typename T::Command cmd, const vector<int>& addr_vec, Request& req, bool& is_last)
    {
        //cout << "Request: " << int(req.type) << ", ID: " << req.instid << " (";
        //for (auto addr: req.addr_vec){
        //    cout << addr << " ";
        //}
        //cout << ") " << "COMMAND: " << int(cmd) << endl;

        // Calculate the starting address of the target function
        auto calc_addr = [&] {
            int addr = 0;
            // the total number of data array for the given hierarchy
            int base_num_entries = 1;
            // start from the lowest hierarchy except the channel
            for (int i = int(T::Level::MAX) - 1; i >= 0; i--) {
                // level_index => the level index of the given memory hierarchy
                int target_index = map_index[i];
                if (target_index == int(T::Level::Channel)) continue;
                // accumulate the index
                addr += addr_vec[target_index] * base_num_entries;
                // the total number of entries for the given level hierarchy
                base_num_entries *= map_num_dat[target_index];
            }
            return addr;
        };

        // Read operation
        auto read_op = [&] {
            int addr = calc_addr();
            req.data.clear();
            for (int i = 0; i < num_dat; i++) {
                req.data.push_back(mem_dat[addr][i]);
                if (mem_dat[addr][i] == std::numeric_limits<double>::infinity()) {
                    assert(0);
                }
            }
        };

        // move the data from the request signal to the register file
        auto write_reg = [&] {
            assert(req.data.size() == num_dat);
            // Register Write
            for (int i = 0; i < num_dat; i++) {
                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] = req.data[i];
                if (req.data[i] == std::numeric_limits<double>::infinity()) {
                    assert(0);
                }
            }
        };

        // Write operation
        auto write_op = [&] {
            int addr = calc_addr();
            assert(req.data.size() == num_dat);
            for(int i = 0; i < num_dat; i++) {
                mem_dat[addr][i] = req.data[i];
                if (req.data[i] == std::numeric_limits<double>::infinity()) {
                    assert(0);
                }
            }
        };

        auto read_reg = [&] {
            req.data.clear();
            for (int i = 0; i < num_dat; i++){
                req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i]);
                if (register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] == std::numeric_limits<double>::infinity()) {
                    assert(0);
                }
            }
            assert(req.data.size() == num_dat);
        };

        // MVM operation
        auto mvm_op = [&] {
            req.data.clear();
            int addr = calc_addr();

            for(int i = 0; i < num_bg * num_bank; i++) {
                double dot_product = 0;
                for(int j = 0; j < num_col; j++) {
                    for(int k = 0; k < num_dat; k++) {
                        dot_product += mem_dat[addr][k] *
                                register_file[req.addr_vec[int(T::Level::Rank)]][j][k];
                    }
                    addr++;
                }
                req.data.push_back(dot_product);
            }
        };

        // ALU operation
        auto alur_op = [&] {
            // set the alu result to the req.data vector
            req.data.clear();
            switch (req.aluType) {
                case Request::ALUType::ADD:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] +
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr2][i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::SUB:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] -
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr2][i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::MUL:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] *
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr2][i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::DIV:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] /
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr2][i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                default: assert(0);
            }
        };

        auto alui_op = [&] {
            // set the alu result to the req.data vector
            req.data.clear();
            switch (req.aluType) {
                case Request::ALUType::ADD:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] + req.data[i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::SUB:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] - req.data[i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::MUL:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] * req.data[i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                case Request::ALUType::DIV:
                    for (int i = 0; i < num_dat; i++) {
                        register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] =
                                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr1][i] / req.data[i];
                        req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
                    }
                    break;
                default: assert(0);
            }
        };

        auto set_op = [&] {
            // set the alu result to the req.data vector
            req.data.clear();
            for (int i = 0; i < num_dat; i++) {
                register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i] = req.data[i];
                req.data.push_back(register_file[req.addr_vec[int(T::Level::Rank)]][req.regAddr3][i]);
            }
        };



        // Main Function: this triggers the changes in the dram (i.e., channel)
        cmd_issue_autoprecharge(cmd, addr_vec);
        assert(is_ready(cmd, addr_vec));
        // See here!
        // add request => as it is used to update the addresses on demand
        channel->update(cmd, clk, req);

        if(cmd == T::Command::PRE) {
            if(rowtable->get_hits(addr_vec, true) == 0) {
                useless_activates++;
            }
        }
 
        rowtable->update(cmd, addr_vec, clk, req);
        if (record_cmd_trace) {
            // select rank
            auto& file = cmd_trace_files[addr_vec[1]];
            string& cmd_name = channel->spec->command_name[int(cmd)];
            file<<clk<<','<<cmd_name;
            // TODO bad coding here
            if (cmd_name == "PREA" || cmd_name == "REF")
                file<<endl;
            else {
                int bank_id = addr_vec[int(T::Level::Bank)];
                if (channel->spec->standard_name == "DDR4" || channel->spec->standard_name == "GDDR5")
                    bank_id += addr_vec[int(T::Level::Bank) - 1] * channel->spec->org_entry.count[int(T::Level::Bank)];
                file<<','<<bank_id<<endl;
            }
        }

        //if (is_last){
        //    switch (req.type) {
        //        case Request::Type::READ:
        //            read_op();
        //            return;
        //        case Request::Type::WRITE:
        //            write_op();
        //            return;
        //        case Request::Type::MVM:
        //            mvm_op();
        //            return;
        //        case Request::Type::RDREG:
        //            read_op();
        //            write_reg();
        //            return;
        //        case Request::Type::WRREG:
        //            read_reg();
        //            write_op();
        //            return;
        //        case Request::Type::ALUR:
        //            alur_op();
        //            return;
        //        case Request::Type::ALUI:
        //            alui_op();
        //            return;
        //        case Request::Type::SET:
        //            set_op();
        //            return;
        //        default: return;
        //    }
        //}
        return;
    }
    vector<int> get_addr_vec(typename T::Command cmd, list<Request>::iterator req){
        return req->addr_vec;
    }
};


} /*namespace ramulator*/

#endif /*__CONTROLLER_H*/
