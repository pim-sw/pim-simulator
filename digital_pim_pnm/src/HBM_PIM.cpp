#include "HBM_PIM.h"
#include "DRAM.h"

#include <vector>
#include <functional>
#include <cassert>

using namespace std;
using namespace ramulator;

string HBM_PIM::standard_name = "HBM_PIM";
string HBM_PIM::level_str [int(Level::MAX)] = {"Ch", "Ra", "Bg", "Ba", "Ro", "Co"};

map<string, enum HBM_PIM::Org> HBM_PIM::org_map = {
    {"HBM_PIM_NEWTON", HBM_PIM::Org::HBM_PIM_NEWTON}
};

map<string, enum HBM_PIM::Speed> HBM_PIM::speed_map = {
    {"HBM_PIM_1Gbps", HBM_PIM::Speed::HBM_PIM_1Gbps},
};

HBM_PIM::HBM_PIM(Org org, Speed speed)
    : org_entry(org_table[int(org)]),
    speed_entry(speed_table[int(speed)]),
    read_latency(speed_entry.nCL + speed_entry.nBL),
    mvm_latency(speed_entry.nBL * org_entry.count[int(Level::Column)]),
    org_ver(int(org))
{
    init_speed();
    init_prereq();
    init_rowhit(); // SAUGATA: added row hit function
    init_rowopen();
    init_lambda();
    init_timing();
}

HBM_PIM::HBM_PIM(const string& org_str, const string& speed_str) :
    HBM_PIM(org_map[org_str], speed_map[speed_str])
{
    org_entry.register_size = org_entry.count[int(Level::Column)] / prefetch_size;
}

void HBM_PIM::set_channel_number(int channel) {
  org_entry.count[int(Level::Channel)] = channel;
}

void HBM_PIM::set_rank_number(int rank) {
  org_entry.count[int(Level::Rank)] = rank;
}


void HBM_PIM::init_speed()
{
    const static int RFC_TABLE[int(Speed::MAX)][int(Org::MAX)] = {
        {130}
    };
    const static int REFI1B_TABLE[int(Speed::MAX)][int(Org::MAX)] = {
        {256}
    };
    const static int XS_TABLE[int(Speed::MAX)][int(Org::MAX)] = {
        {135}
    };

    int speed = 0, density = 0;
    switch (speed_entry.rate) {
        case 1000: speed = 0; break;
        default: assert(false);
    };
    switch (org_ver){
        case 0: density = 0; break;
        default: assert(false);
    }
    speed_entry.nRFC = RFC_TABLE[speed][density];
    speed_entry.nREFI1B = REFI1B_TABLE[speed][density];
    speed_entry.nXS = XS_TABLE[speed][density];
}


void HBM_PIM::init_prereq()
{
    prereq[int(Level::Rank)][int(Command::ALUR)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        is_last = true;
        return cmd;
    };
    prereq[int(Level::Rank)][int(Command::ALUI)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        is_last = true;
        return cmd;
    };
    // PIM FIX: Add command for the
    // 1) GWrite (Rank)
    // 2) GActA (Bankgroup)
    // 3) MVM (Rank)
    prereq[int(Level::Rank)][int(Command::SET)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        is_last = true;
        return cmd;
    };


    // MVM (Perform MVM only after GAct)
    // Note that, GACTA is never reused!
    prereq[int(Level::Rank)][int(Command::MVM)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        if (node->state == State::GOpened) {
            is_last = true;
            return cmd;
        }
        else {
            for (auto bg : node->children) {
                for (auto bank: bg->children) {
                    if (bank->state == State::Opened) {
                        return Command::PREA;
                    }
                }
            }
        }
        return Command::GACTA;
    };


    // RD
    prereq[int(Level::Rank)][int(Command::RD)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        switch (int(node->state)) {
            case int(State::GOpened): return Command::MAX;
            case int(State::PowerUp): return Command::MAX;
            case int(State::ActPowerDown): return Command::PDX;
            case int(State::PrePowerDown): return Command::PDX;
            case int(State::SelfRefresh): return Command::SRX;
            default: assert(false);
        }};
    prereq[int(Level::Bank)][int(Command::RD)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        switch (int(node->state)) {
            case int(State::Closed): return Command::ACT;
            // check if the opened row is what I'm looking row
            case int(State::Opened):
                // if I found the row => return the command
                if (node->row_state.find(id) != node->row_state.end()){
                    is_last = true;
                    return cmd;
                }
                // else, precharge and then act
                else return Command::PRE;
            default: assert(false);
        }};

    // WR
    prereq[int(Level::Rank)][int(Command::WR)] = prereq[int(Level::Rank)][int(Command::RD)];
    prereq[int(Level::Bank)][int(Command::WR)] = prereq[int(Level::Bank)][int(Command::RD)];

    // PIMFIX : add prereq for row
    prereq[int(Level::Rank)][int(Command::RDREG)] = prereq[int(Level::Rank)][int(Command::RD)];
    prereq[int(Level::Bank)][int(Command::RDREG)] = prereq[int(Level::Bank)][int(Command::RD)];
    prereq[int(Level::Rank)][int(Command::WRREG)] = prereq[int(Level::Rank)][int(Command::RD)];
    prereq[int(Level::Bank)][int(Command::WRREG)] = prereq[int(Level::Bank)][int(Command::RD)];

    // REF
    prereq[int(Level::Rank)][int(Command::REF)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        // check if all the banks are closed
        // else => precharge all the banks before the refresh
        for (auto bg : node->children)
            for (auto bank: bg->children) {
                if (bank->state == State::Closed)
                    continue;
                return Command::PREA;
            }
        is_last = true;
        return Command::REF;
    };

    // REFSB
    prereq[int(Level::Bank)][int(Command::REFSB)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        if (node->state == State::Closed) {
            is_last = true;
            return Command::REFSB;
        }
        return Command::PRE;};

    // PD
    prereq[int(Level::Rank)][int(Command::PDE)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        switch (int(node->state)) {
            case int(State::GOpened):
                is_last = true;
                return Command::PDE;
            case int(State::PowerUp):
                is_last = true;
                return Command::PDE;
            case int(State::ActPowerDown):
                is_last = true;
                return Command::PDE;
            case int(State::PrePowerDown):
                is_last = true;
                return Command::PDE;
            case int(State::SelfRefresh): return Command::SRX;
            default: assert(false);
        }};

    // SR
    prereq[int(Level::Rank)][int(Command::SRE)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id, Request& req, bool& is_last) {
        switch (int(node->state)) {
            case int(State::GOpened):
                is_last = true;
                return Command::SRE;
            case int(State::PowerUp):
                is_last = true;
                return Command::SRE;
            case int(State::ActPowerDown): return Command::PDX;
            case int(State::PrePowerDown): return Command::PDX;
            case int(State::SelfRefresh):
                is_last = true;
                return Command::SRE;
            default: assert(false);
        }};
}

// SAUGATA: added row hit check functions to see if the desired location is currently open
void HBM_PIM::init_rowhit()
{
    // RD
    rowhit[int(Level::Bank)][int(Command::RD)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id) {
        switch (int(node->state)) {
            case int(State::Closed): return false;
            // the opened row is what I'm looking row
            case int(State::Opened):
                if (node->row_state.find(id) != node->row_state.end())
                    return true;
                return false;
            default: assert(false);
        }};

    // WR
    rowhit[int(Level::Bank)][int(Command::WR)] = rowhit[int(Level::Bank)][int(Command::RD)];
    rowhit[int(Level::Bank)][int(Command::RDREG)] = rowhit[int(Level::Bank)][int(Command::RD)];
    rowhit[int(Level::Bank)][int(Command::WRREG)] = rowhit[int(Level::Bank)][int(Command::RD)];
}

void HBM_PIM::init_rowopen()
{
    // RD
    rowopen[int(Level::Bank)][int(Command::RD)] = [] (DRAM<HBM_PIM>* node, Command cmd, int id) {
        switch (int(node->state)) {
            case int(State::Closed): return false;
            case int(State::Opened):
                return true;
            default: assert(false);
        }};

    // WR
    rowopen[int(Level::Bank)][int(Command::WR)] = rowopen[int(Level::Bank)][int(Command::RD)];
    rowopen[int(Level::Bank)][int(Command::RDREG)] = rowopen[int(Level::Bank)][int(Command::RD)];
    rowopen[int(Level::Bank)][int(Command::WRREG)] = rowopen[int(Level::Bank)][int(Command::RD)];
}

void HBM_PIM::init_lambda()
{
    // PIM FIX: Add command for the
    // 1) GWrite (Rank)
    // 2) GActA (Rank)
    // 3) MVM (Rank)

    // PIM FIX
    lambda[int(Level::Bank)][int(Command::RDREG)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Bank)][int(Command::WRREG)] = [] (DRAM<HBM_PIM>* node, Request& req) {};

    // SET => this is blank for now without functionality
    lambda[int(Level::Rank)][int(Command::ALUR)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Rank)][int(Command::ALUI)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Rank)][int(Command::SET)] = [] (DRAM<HBM_PIM>* node, Request& req) {};

    // GACTA
    lambda[int(Level::Rank)][int(Command::GACTA)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        // row_id for each bank in the bankgroup
        node->state = State::GOpened;
        int row_id = req.addr_vec[int(Level::Bank)+1];
        // Iterate over all the bg -> bank
        for (auto bg : node->children) {
            for (auto bank : bg->children) {
                bank->state = State::Opened;
                bank->row_state[row_id] = State::Opened;
            }
        }};

    // MVM (assume a ganged 32 computation)
    lambda[int(Level::Rank)][int(Command::MVM)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        // The state is turned to a normal power up state
        node->state = State::PowerUp;
    };

    // ACT
    lambda[int(Level::Bank)][int(Command::ACT)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::Opened;
        int id = req.addr_vec[int(Level::Bank)+1];
        node->row_state[id] = State::Opened;};

    // PRE
    lambda[int(Level::Bank)][int(Command::PRE)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::Closed;
        node->row_state.clear();};

    lambda[int(Level::Rank)][int(Command::PRE)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::PowerUp;
    };

    // PREA: precharges all the banks in a rankV
    lambda[int(Level::Rank)][int(Command::PREA)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::PowerUp;
        for (auto bg : node->children)
            for (auto bank : bg->children) {
                bank->state = State::Closed;
                bank->row_state.clear();
            }};

    lambda[int(Level::Rank)][int(Command::REF)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Bank)][int(Command::RD)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Bank)][int(Command::WR)] = [] (DRAM<HBM_PIM>* node, Request& req) {};
    lambda[int(Level::Bank)][int(Command::RDA)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::Closed;
        node->row_state.clear();};
    lambda[int(Level::Bank)][int(Command::WRA)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::Closed;
        node->row_state.clear();};
    lambda[int(Level::Rank)][int(Command::PDE)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        for (auto bg : node->children)
            for (auto bank : bg->children) {
                if (bank->state == State::Closed)
                    continue;
                node->state = State::ActPowerDown;
                return;
            }
        node->state = State::PrePowerDown;};
    lambda[int(Level::Rank)][int(Command::PDX)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::PowerUp;};
    lambda[int(Level::Rank)][int(Command::SRE)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::SelfRefresh;};
    lambda[int(Level::Rank)][int(Command::SRX)] = [] (DRAM<HBM_PIM>* node, Request& req) {
        node->state = State::PowerUp;};
}


void HBM_PIM::init_timing()
{
    SpeedEntry& s = speed_entry;
    vector<TimingEntry> *t;

    // there can be some cases where the function affects the siblings (not shown here...)
    // please refer to DDR4 case for details

    /*** Channel ***/
    t = timing[int(Level::Channel)];

    // PIM FIX:
    // 1) GWrite (Rank)     => May affect the channel level
    //                      => affects the data channel
    // 2) GAct (Rank)       => Does not affect the channel level (or not?)
    //                      => affects the cmd channel only
    // 3) MVM (Rank)        => May affect the channel level (or not?)
    //                      => affects the data channel

    // CAS <-> CAS
    // The latency between rd / wr operation is channel-level CAS latency
    t[int(Command::RD)].push_back({Command::RD, 1, s.nBL});
    t[int(Command::RD)].push_back({Command::RDA, 1, s.nBL});

    t[int(Command::RDA)].push_back({Command::RD, 1, s.nBL});
    t[int(Command::RDA)].push_back({Command::RDA, 1, s.nBL});
    
    t[int(Command::WR)].push_back({Command::WR, 1, s.nBL});
    t[int(Command::WR)].push_back({Command::WRA, 1, s.nBL});
    t[int(Command::WR)].push_back({Command::ALUI, 1, s.nBL});
    t[int(Command::WR)].push_back({Command::SET, 1, s.nBL});
    
    t[int(Command::WRA)].push_back({Command::WR, 1, s.nBL});
    t[int(Command::WRA)].push_back({Command::WRA, 1, s.nBL});
    t[int(Command::WRA)].push_back({Command::ALUI, 1, s.nBL});
    t[int(Command::WRA)].push_back({Command::SET, 1, s.nBL});

    // PIM FIX:
    // MVM -> The channel-level latency is bound by nBL
    t[int(Command::RD)].push_back({Command::MVM, 1, s.nBL});
    t[int(Command::RDA)].push_back({Command::MVM, 1, s.nBL});

    t[int(Command::MVM)].push_back({Command::RD, 1, s.nBL});
    t[int(Command::MVM)].push_back({Command::RDA, 1, s.nBL});
    t[int(Command::MVM)].push_back({Command::MVM, 1, s.nBL});

    t[int(Command::ALUI)].push_back({Command::WR, 1, s.nBL});
    t[int(Command::SET)].push_back({Command::WR, 1, s.nBL});
    t[int(Command::ALUI)].push_back({Command::WRA, 1, s.nBL});
    t[int(Command::SET)].push_back({Command::WRA, 1, s.nBL});

    /*** Rank ***/
    t = timing[int(Level::Rank)];

    // PIM FIX:
    // Possible Dependency
    // Note that,
    // 1) GWrite does not affect other ops except channel
    // 1) MVM never succeeds other operations (except GACTA)
    // 2) GACTA never precedes other operations (except MVM)
    // RD/WR        => GACTA (X)
    // PRE/PREA     => GACTA (O)
    // ACT          => GACTA (O)
    // MVM          => RD/WR (X) (cannot activate before precharge)
    // MVM          => PRE/PREA (O)
    // MVM          => ACT (X) (cannot activate before precharge)
    //
    // 1) SET (Rank)     => May affect the rank level
    // 2) GACTA (Rank)      =>
    // 3) MVM (Rank)        =>

    // CAS <-> CAS
    t[int(Command::RD)].push_back({Command::RD, 1, s.nCCDS});
    t[int(Command::RD)].push_back({Command::RDA, 1, s.nCCDS});
    t[int(Command::RD)].push_back({Command::RDREG, 1, s.nCCDS});
    
    t[int(Command::RDA)].push_back({Command::RD, 1, s.nCCDS});
    t[int(Command::RDA)].push_back({Command::RDA, 1, s.nCCDS});
    t[int(Command::RDA)].push_back({Command::RDREG, 1, s.nCCDS});

    t[int(Command::RDREG)].push_back({Command::RD, 1, s.nCCDS});
    t[int(Command::RDREG)].push_back({Command::RDA, 1, s.nCCDS});
    t[int(Command::RDREG)].push_back({Command::RDREG, 1, s.nCCDS});

    t[int(Command::WR)].push_back({Command::WR, 1, s.nCCDS});
    t[int(Command::WR)].push_back({Command::WRA, 1, s.nCCDS});
    t[int(Command::WR)].push_back({Command::WRREG, 1, s.nCCDS});

    t[int(Command::WRA)].push_back({Command::WR, 1, s.nCCDS});
    t[int(Command::WRA)].push_back({Command::WRA, 1, s.nCCDS});
    t[int(Command::WRA)].push_back({Command::WRREG, 1, s.nCCDS});

    t[int(Command::WRREG)].push_back({Command::WR, 1, s.nCCDS});
    t[int(Command::WRREG)].push_back({Command::WRA, 1, s.nCCDS});
    t[int(Command::WRREG)].push_back({Command::WRREG, 1, s.nCCDS});

    t[int(Command::RD)].push_back({Command::WR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
    t[int(Command::RD)].push_back({Command::WRA, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
    t[int(Command::RD)].push_back({Command::WRREG, 1, s.nCL + s.nBL + 2 - s.nCWL});

    t[int(Command::RDA)].push_back({Command::WR, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
    t[int(Command::RDA)].push_back({Command::WRA, 1, s.nCL + s.nCCDS + 2 - s.nCWL});
    t[int(Command::RDA)].push_back({Command::WRREG, 1, s.nCL + s.nBL + 2 - s.nCWL});

    t[int(Command::RDREG)].push_back({Command::WR, 1, s.nCL + s.nBL + 2 - s.nCWL});
    t[int(Command::RDREG)].push_back({Command::WRA, 1, s.nCL + s.nBL + 2 - s.nCWL});
    t[int(Command::RDREG)].push_back({Command::WRREG, 1, s.nCL + s.nBL + 2 - s.nCWL});

    t[int(Command::WR)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WR)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WR)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRS});

    t[int(Command::WRA)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WRA)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WRA)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRS});

    t[int(Command::WRREG)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WRREG)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRS});
    t[int(Command::WRREG)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRS});


    t[int(Command::RD)].push_back({Command::PREA, 1, s.nRTP});
    t[int(Command::WR)].push_back({Command::PREA, 1, s.nCWL + s.nBL + s.nWR});

    // CAS <-> PD
    t[int(Command::RD)].push_back({Command::PDE, 1, s.nCL + s.nBL + 1});
    t[int(Command::RDA)].push_back({Command::PDE, 1, s.nCL + s.nBL + 1});
    t[int(Command::RDREG)].push_back({Command::PDE, 1, s.nCL + s.nBL * + 1});

    t[int(Command::WR)].push_back({Command::PDE, 1, s.nCWL + s.nBL + s.nWR});
    t[int(Command::WRA)].push_back({Command::PDE, 1, s.nCWL + s.nBL + s.nWR + 1}); // +1 for pre
    t[int(Command::WRREG)].push_back({Command::PDE, 1, s.nCWL + s.nBL + s.nWR + 1}); // +1 for pre
    
    t[int(Command::PDX)].push_back({Command::RD, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::RDA, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::RDREG, 1, s.nXP});
    
    t[int(Command::PDX)].push_back({Command::WR, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::WRA, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::WRREG, 1, s.nXP});

    // CAS <-> SR: none (all banks have to be precharged)

    // RAS <-> RAS
    t[int(Command::ACT)].push_back({Command::ACT, 1, s.nRRDS});
    t[int(Command::ACT)].push_back({Command::ACT, 4, s.nFAW});
    t[int(Command::ACT)].push_back({Command::PREA, 1, s.nRAS});
    t[int(Command::PREA)].push_back({Command::ACT, 1, s.nRP});

    // PIM FIX: Added
    // 3 * s.nFAW => ganged activation transfer
    // s.nRCDR => the latency of an activation
    // s.nCL => the latency to retrieve the data from the row buffer
    // * org_table->count[int(Level::Column)]
    t[int(Command::GACTA)].push_back({Command::MVM, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL});
    // force other operations to be issued slower!! (Force scheduling MVM after GACTA)
    t[int(Command::GACTA)].push_back({Command::PREA, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL + 1});
    t[int(Command::GACTA)].push_back({Command::PRE, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL + 1});
    t[int(Command::GACTA)].push_back({Command::ACT, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL + 1});
    t[int(Command::GACTA)].push_back({Command::GACTA, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL + 1});
    t[int(Command::GACTA)].push_back({Command::SET, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRCDR + s.nCL + 1});


    t[int(Command::PREA)].push_back({Command::GACTA, 1, s.nRP});
    t[int(Command::PRE)].push_back({Command::GACTA, 1, s.nRP});

    // The latency would be nFAW if the ACT does not activate the bank in the first bank group
    // But we assumed that for now
    t[int(Command::ACT)].push_back({Command::GACTA, 1, s.nRC});
    t[int(Command::GACTA)].push_back({Command::GACTA, 1, s.nFAW *
        org_entry.count[int(Level::BankGroup)]});
    // There can be two cases and we set the worst case latency (i.e., case 1)
    // 1) when ACT activates the bank in the last bankgroup
    // 2) when ACT activates other bank ...
    t[int(Command::GACTA)].push_back({Command::ACT, 1, s.nFAW *
        (org_entry.count[int(Level::BankGroup)] - 1) + s.nRC});

    // nBL is the latency for each column (multiply and add operation is hidden in the nBL)
    t[int(Command::MVM)].push_back({Command::PREA, 1, s.nBL * org_entry.count[int(Level::Column)] / prefetch_size});
    t[int(Command::MVM)].push_back({Command::PRE, 1, s.nBL * org_entry.count[int(Level::Column)] / prefetch_size});
    t[int(Command::MVM)].push_back({Command::SET, 1, s.nBL * org_entry.count[int(Level::Column)] / prefetch_size});
    // PIM FIX End

    // RAS <-> REF
    t[int(Command::PRE)].push_back({Command::REF, 1, s.nRP});
    t[int(Command::PREA)].push_back({Command::REF, 1, s.nRP});
    t[int(Command::REF)].push_back({Command::ACT, 1, s.nRFC});

    // RAS <-> PD
    t[int(Command::ACT)].push_back({Command::PDE, 1, 1});
    t[int(Command::PDX)].push_back({Command::ACT, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::PRE, 1, s.nXP});
    t[int(Command::PDX)].push_back({Command::PREA, 1, s.nXP});

    // RAS <-> SR
    t[int(Command::PRE)].push_back({Command::SRE, 1, s.nRP});
    t[int(Command::PREA)].push_back({Command::SRE, 1, s.nRP});
    t[int(Command::SRX)].push_back({Command::ACT, 1, s.nXS});

    // REF <-> REF
    t[int(Command::REF)].push_back({Command::REF, 1, s.nRFC});

    // REF <-> PD
    t[int(Command::REF)].push_back({Command::PDE, 1, 1});
    t[int(Command::PDX)].push_back({Command::REF, 1, s.nXP});

    // REF <-> SR
    t[int(Command::SRX)].push_back({Command::REF, 1, s.nXS});

    // PD <-> PD
    t[int(Command::PDE)].push_back({Command::PDX, 1, s.nPD});
    t[int(Command::PDX)].push_back({Command::PDE, 1, s.nXP});

    // PD <-> SR
    t[int(Command::PDX)].push_back({Command::SRE, 1, s.nXP});
    t[int(Command::SRX)].push_back({Command::PDE, 1, s.nXS});

    // SR <-> SR
    t[int(Command::SRE)].push_back({Command::SRX, 1, s.nCKESR});
    t[int(Command::SRX)].push_back({Command::SRE, 1, s.nXS});

    t[int(Command::SET)].push_back({Command::SET, 1, s.nREG});

    // Register-Level Latency
    t[int(Command::RDREG)].push_back({Command::RDREG, 1, s.nREG});
    t[int(Command::WRREG)].push_back({Command::WRREG, 1, s.nREG});
    t[int(Command::WRREG)].push_back({Command::RDREG, 1, s.nREG});
    t[int(Command::RDREG)].push_back({Command::WRREG, 1, s.nREG});

    t[int(Command::RDREG)].push_back({Command::ALUR, 1, s.nREG});
    t[int(Command::RDREG)].push_back({Command::ALUI, 1, s.nREG});
    t[int(Command::RDREG)].push_back({Command::SET, 1, s.nREG});

    t[int(Command::WRREG)].push_back({Command::ALUR, 1, s.nREG});
    t[int(Command::WRREG)].push_back({Command::ALUI, 1, s.nREG});
    t[int(Command::WRREG)].push_back({Command::SET, 1, s.nREG});

    // READ + MODIFY + WRITE
    t[int(Command::ALUR)].push_back({Command::RDREG, 1, s.nREG * 2 + s.nALU});
    t[int(Command::ALUR)].push_back({Command::WRREG, 1, s.nREG * 2 + s.nALU});
    t[int(Command::ALUR)].push_back({Command::ALUR, 1, s.nREG * 2 + s.nALU});
    t[int(Command::ALUR)].push_back({Command::ALUI, 1, s.nREG * 2 + s.nALU});
    t[int(Command::ALUR)].push_back({Command::SET, 1, s.nREG * 2 + s.nALU});

    t[int(Command::ALUI)].push_back({Command::RDREG, 1, s.nREG * 1 + s.nALU});
    t[int(Command::ALUI)].push_back({Command::WRREG, 1, s.nREG * 1 + s.nALU});
    t[int(Command::ALUI)].push_back({Command::ALUR, 1, s.nREG * 1 + s.nALU});
    t[int(Command::ALUI)].push_back({Command::ALUI, 1, s.nREG * 1 + s.nALU});
    t[int(Command::ALUI)].push_back({Command::SET, 1, s.nREG * 1 + s.nALU});

    t[int(Command::SET)].push_back({Command::RDREG, 1, s.nREG * 1});
    t[int(Command::SET)].push_back({Command::WRREG, 1, s.nREG * 1});
    t[int(Command::SET)].push_back({Command::ALUR, 1, s.nREG * 1});
    t[int(Command::SET)].push_back({Command::ALUI, 1, s.nREG * 1});
    t[int(Command::SET)].push_back({Command::SET, 1, s.nREG * 1});

    /*** Bank Group ***/
    t = timing[int(Level::BankGroup)];

    //


    // CAS <-> CAS
    t[int(Command::RD)].push_back({Command::RD, 1, s.nCCDL});
    t[int(Command::RD)].push_back({Command::RDA, 1, s.nCCDL});
    t[int(Command::RD)].push_back({Command::RDREG, 1, s.nCCDL});

    t[int(Command::RDA)].push_back({Command::RD, 1, s.nCCDL});
    t[int(Command::RDA)].push_back({Command::RDA, 1, s.nCCDL});
    t[int(Command::RDA)].push_back({Command::RDREG, 1, s.nCCDL});

    t[int(Command::RDREG)].push_back({Command::RD, 1, s.nCCDL});
    t[int(Command::RDREG)].push_back({Command::RDA, 1, s.nCCDL});
    t[int(Command::RDREG)].push_back({Command::RDREG, 1, s.nCCDL});
    
    t[int(Command::WR)].push_back({Command::WR, 1, s.nCCDL});
    t[int(Command::WR)].push_back({Command::WRA, 1, s.nCCDL});
    t[int(Command::WR)].push_back({Command::WRREG, 1, s.nCCDL});
    
    t[int(Command::WRA)].push_back({Command::WR, 1, s.nCCDL});
    t[int(Command::WRA)].push_back({Command::WRA, 1, s.nCCDL});
    t[int(Command::WRA)].push_back({Command::WRREG, 1, s.nCCDL});

    t[int(Command::WRREG)].push_back({Command::WR, 1, s.nCCDL});
    t[int(Command::WRREG)].push_back({Command::WRA, 1, s.nCCDL});
    t[int(Command::WRREG)].push_back({Command::WRREG, 1, s.nCCDL});
    
    t[int(Command::WR)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WR)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WR)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRL});
    
    t[int(Command::WRA)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WRA)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WRA)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRL});

    t[int(Command::WRREG)].push_back({Command::RD, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WRREG)].push_back({Command::RDA, 1, s.nCWL + s.nBL + s.nWTRL});
    t[int(Command::WRREG)].push_back({Command::RDREG, 1, s.nCWL + s.nBL + s.nWTRL});

    // RAS <-> RAS
    t[int(Command::ACT)].push_back({Command::ACT, 1, s.nRRDL});

    /*** Bank ***/
    t = timing[int(Level::Bank)];

    // CAS <-> RAS
    t[int(Command::ACT)].push_back({Command::RD, 1, s.nRCDR});
    t[int(Command::ACT)].push_back({Command::RDA, 1, s.nRCDR});
    t[int(Command::ACT)].push_back({Command::RDREG, 1, s.nRCDR});

    t[int(Command::ACT)].push_back({Command::WR, 1, s.nRCDW});
    t[int(Command::ACT)].push_back({Command::WRA, 1, s.nRCDW});
    t[int(Command::ACT)].push_back({Command::WRREG, 1, s.nRCDW});

    t[int(Command::RD)].push_back({Command::PRE, 1, s.nRTP});
    t[int(Command::WR)].push_back({Command::PRE, 1, s.nCWL + s.nBL + s.nWR});

    t[int(Command::RDA)].push_back({Command::ACT, 1, s.nRTP + s.nRP});
    t[int(Command::WRA)].push_back({Command::ACT, 1, s.nCWL + s.nBL + s.nWR + s.nRP});

    t[int(Command::RDREG)].push_back({Command::ACT, 1, s.nRTP + s.nRP});
    t[int(Command::WRREG)].push_back({Command::ACT, 1, s.nCWL + s.nBL + s.nWR + s.nRP});

    // RAS <-> RAS
    t[int(Command::ACT)].push_back({Command::ACT, 1, s.nRC});
    t[int(Command::ACT)].push_back({Command::PRE, 1, s.nRAS});
    t[int(Command::PRE)].push_back({Command::ACT, 1, s.nRP});

    // REFSB
    t[int(Command::PRE)].push_back({Command::REFSB, 1, s.nRP});
    t[int(Command::REFSB)].push_back({Command::REFSB, 1, s.nRFC});
    t[int(Command::REFSB)].push_back({Command::ACT, 1, s.nRFC});
}
