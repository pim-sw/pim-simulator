#ifndef __HBM_PIM_H
#define __HBM_PIM_H

#include "DRAM.h"
#include "Request.h"
#include <vector>
#include <functional>

using namespace std;

namespace ramulator
{

class HBM_PIM
{
public:
    static string standard_name;
    enum class Org;
    enum class Speed;
    HBM_PIM(Org org, Speed speed);
    HBM_PIM(const string& org_str, const string& speed_str);

    static map<string, enum Org> org_map;
    static map<string, enum Speed> speed_map;

    /* Level */
    // PIM FIX
    // Note that,
    // channel == rank in Newton
    // rank == channel in Newton
    // bank group == quarter in Newton
    // bank == bank in Newton!
    enum class Level : int
    {
        Channel, Rank, BankGroup, Bank, Row, Column, MAX
    };
    
    static std::string level_str [int(Level::MAX)];

    /* Command */
    enum class Command : int
    {
        ACT,    // activate => connects a row (open row) to the bitline and moves the charge from the cap to the sa
                // the opened row is not closed until the precharge command #0
        PRE,    // precharge => deactivates the currently open row for the bank
                // #1
        PREA,   // precharge all
                // #2
        RD,     // read
                // #3
        WR,     // write
                // #4
        RDA,    // maybe a read auto precharge command (read + precharge)
                // #5
        WRA,    // maybe a write auto precharge command (write + precharge)
                // #6
        REF,    // refresh all the banks in a rank => refresh the last refreshed row
                // #7
        REFSB,  // refresh a single bank in a rank ?? => refresh the last refreshed row
                // #8
        PDE,    // powerdown... don't know the usage
                // #9
        PDX,    // #10
        SRE,    // self refresh... don't know the usage
                // #11
        SRX,    // #12
        GACTA,  // perform ganged activation for all the bankgroups
                // #13
        MVM,    // perform multiply across all the banks in the rank & read the result
                // #14
        SET,  // for SETI operation
        RDREG,
        WRREG,
        ALUR,
        ALUI,
        MAX
    };

    // REFSB and REF is not compatible, choose one or the other.
    // REFSB can be issued to banks in any order, as long as REFI1B
    // is satisfied for all banks

    string command_name[int(Command::MAX)] = {
        "ACT", "PRE",   "PREA",
        "RD",  "WR",    "RDA",  "WRA",
        "REF", "REFSB", "PDE",  "PDX",  "SRE", "SRX",
        // additional functions for PIM
        "GACTA", "MVM", "SET", "RDREG", "WRREG",
        "ALUR", "ALUI"
    };

    Level scope[int(Command::MAX)] = {
        Level::Row,         Level::Bank,    Level::Rank,
        Level::Column,      Level::Column,  Level::Column,  Level::Column,
        Level::Rank,        Level::Bank,    Level::Rank,    Level::Rank,   Level::Rank,   Level::Rank,
        Level::Rank,        Level::Rank,    Level::Rank
    };

    // FIXME: change the is_opening and is_closing for scheduler debugging

    bool is_opening(Command cmd)
    {
        switch(int(cmd)) {
            case int(Command::ACT):
            case int(Command::GACTA):
                return true;
            default:
                return false;
        }
    }

    bool is_accessing(Command cmd)
    {
        switch(int(cmd)) {
            case int(Command::RD):
            case int(Command::WR):
            case int(Command::RDA):
            case int(Command::WRA):
            case int(Command::MVM):
            case int(Command::RDREG):
            case int(Command::WRREG):
                return true;
            default:
                return false;
        }
    }

    bool is_closing(Command cmd)
    {
        switch(int(cmd)) {
            case int(Command::RDA):
            case int(Command::WRA):
            case int(Command::PRE):
            case int(Command::PREA):
                return true;
            default:
                return false;
        }
    }

    bool is_refreshing(Command cmd)
    {
        switch(int(cmd)) {
            case int(Command::REF):
            case int(Command::REFSB):
                return true;
            default:
                return false;
        }
    }

    // PIM FIX: add new states for the written result exists?
    /* State */
    enum class State : int
    {
        Opened,         //
        Closed,         //
        PowerUp,        //
        ActPowerDown,   //
        PrePowerDown,   //
        SelfRefresh,    //
        GOpened,        // opended due to gact
        MAX
    } start[int(Level::MAX)] = {
        State::MAX,         // Channel
        State::PowerUp,     // Rank
        State::MAX,         // BankGroup
        State::Closed,      // Bank
        State::Closed,      // Row
        State::MAX          // Column
    };
    
    // PIM FIX: fix the translation table!
    /* Translate */
    Command translate[int(Request::Type::MAX)] = {
        Command::RD,    // READ
        Command::WR,    // WRITE
        Command::REF,   // REFRESH
        Command::PDE,   // POWERDOWN
        Command::SRE,   // SELFREFRESH
        Command::MVM,   // MVM
        Command::RDREG, // RDREG
        Command::WRREG, // WRREG
        Command::ALUR,   // ALUR
        Command::ALUI,  // ALUI
        Command::SET    // SET
    };


    /* Prereq */
    function<Command(DRAM<HBM_PIM>*, Command cmd, int, Request&, bool&)> prereq[int(Level::MAX)][int(Command::MAX)];

    // SAUGATA: added function object container for row hit status
    /* Row hit */
    function<bool(DRAM<HBM_PIM>*, Command cmd, int)> rowhit[int(Level::MAX)][int(Command::MAX)];
    function<bool(DRAM<HBM_PIM>*, Command cmd, int)> rowopen[int(Level::MAX)][int(Command::MAX)];

    /* Timing */
    struct TimingEntry
    {
        // target command
        Command cmd;
        int dist;
        // value of the timing constraints (# of cycles)
        int val;
        // if sibling is true => this timing entry is for the case which affects the sibling nodes
        bool sibling;
    };
    // Main Function:
    // each timing vector for the given level & command indicates the following
    // the timing constraints between
        // 1) the command written in the entry
        // 2) the command to index the timing array
    vector<TimingEntry> timing[int(Level::MAX)][int(Command::MAX)];

    /* Lambda */
    // Main Function: the two hierarchies determine
    // 1) at which level the function is activated
    // 2) which command activates the lambda
    function<void(DRAM<HBM_PIM>*, Request&)> lambda[int(Level::MAX)][int(Command::MAX)];

    /* Organization */
    enum class Org : int
    { // per channel density here. Each stack comes with 8 channels
        HBM_PIM_NEWTON,
        MAX
    };

    struct OrgEntry {
        int dq;
        int count[int(Level::MAX)];
        int register_size;
    } org_table[int(Org::MAX)] = {
        // The number of channels and ranks are externally set
        // Channel / Rank / BankGroup / Bank / Row / Column (128)
        // Size is set for a single rank
        // DQ is the number of bits for a single read / write
        // 8 Gb per channel
        {256, {0, 0, 4, 4, 1<<5, 1<<5}, 0}
    }, org_entry;

    void set_channel_number(int channel);
    void set_rank_number(int rank);

    /* Speed */
    enum class Speed : int
    {
        HBM_PIM_1Gbps,
        MAX
    };

    int prefetch_size = 4; // burst length could be 2 and 4 (choose 4 here), 2n prefetch
    int channel_width = 256;
    int data_bits = 16;
    int host_memory_bw = 256; // bits/cycle



    struct SpeedEntry {
        int rate;
        double freq, tCK;
        int nBL;        // CAS to CAS access latency (Channel-level) => Burst Length
        int nCCDS;      // CAS to CAS access latency (Rank-level)
        int nCCDL;      // CAS to CAS access latency (BankGroup-level)

        int nCL;        // The latency to retrieve the data from the row buffer
        int nRCDR;      // time it takes between the activation of the row => activation of the column
        int nRCDW;      // time it takes between the activation of the row => activation of the column
        int nRP;        // precharge to activate latency
        int nCWL;       //

        int nRAS;       // activate to precharge latency
        int nRC;        //

        int nRTP;       //
        int nWTRS;      //
        int nWTRL;      //
        int nWR;        //

        int nRRDS;      //
        int nRRDL;      //
        int nFAW;       //

        int nRFC;       // Refresh cycle time
        int nREFI;      // Refresh intervals
        int nREFI1B;    //

        int nPD;
        int nXP;
        int nCKESR;
        int nXS;
        // FIXME: fix two parameters
        int nREG;       // FIXME: the time to read / write from the global buffer in the NEWTON architecture
        int nALU;       // FIXME: the time to perform an ALU operation (using the data stored in the global buffer)
    } speed_table[int(Speed::MAX)] = {
        {1000,
        500,
        2.0,
        2,
        2,
        3,
        7,
        7,
        6,
        7,
        4,
        17,
        24,
        7,
        2,
        4,
        8,
        4,
        5,
        20,
        0,
        1950,
        0,
        5,
        5,
        5,
        0,
        1,
        1}
    }, speed_entry;

    int read_latency;
    int mvm_latency;
    int org_ver;

private:
    void init_speed();
    void init_lambda();
    void init_prereq();
    void init_rowhit();  // SAUGATA: added function to check for row hits
    void init_rowopen();
    void init_timing();
};

} /*namespace ramulator*/

#endif /*__HBM_PIM_H*/
