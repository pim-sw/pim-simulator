#include "Processor.h"
#include "Config.h"
#include "Controller.h"
#include "Memory.h"
#include "DRAM.h"
#include "Statistics.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdlib.h>
#include <functional>
#include <map>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <limits>

/* Standards */
#include "DDR4.h"
#include "DDR4_TSDIMM.h"
#include "HBM.h"
#include "HBM_PIM.h"



using namespace std;
using namespace ramulator;

bool ramulator::warmup_complete = false;


string print_req(const Request& req) {
    switch (req.type) {
        case Request::Type::READ:
            return "RD";
        case Request::Type::WRITE:
            return "WR";
        case Request::Type::MVM:
            return "MV";
        case Request::Type::RDREG:
            return "RG";
        case Request::Type::WRREG:
            return "WG";
        case Request::Type::ALUR:
            return "AR";
        case Request::Type::ALUI:
            return "AI";
        case Request::Type::SET:
            return "SE";
        default:
            assert(false);
    }
}

void clear_lower_bits(long& addr, int bits)
{
    addr >>= bits;
}

int slice_lower_bits(long& addr, int bits)
{
    int lbits = addr & ((1<<bits) - 1);
    addr >>= bits;
    return lbits;
}

template<typename T>
void run_trace(const Config& configs, T* spec, Memory<T, Controller>& memory, const char* tracename) {

    /* initialize DRAM trace */
    Trace trace(tracename);

    /* run simulation */
    bool stall = false, end = false;
    int clks = 0;

    long addr = 0;
    vector<double> data;
    Request::Type type = Request::Type::READ;
    Request::ALUType aluType = Request::ALUType::MAX;
    // default address
    int regAddr1 = -1;
    int regAddr2 = -1;
    int regAddr3 = -1;
    string source_code = "";

    map<int, int> latencies;

    int instid = 0;
    Request req(type, addr);

    // FILE IO
    string res_filename = "./result/result.txt";
    remove(res_filename.c_str());
    ofstream res_os;
    res_os.open(res_filename, ios_base::app);

    string time_filename = "./result/timing.txt";
    remove(time_filename.c_str());
    ofstream time_os;
    time_os.open(time_filename, ios_base::app);


    // Memory Array
    vector<int> addr_bits = memory.addr_bits;

    int num_channel = 1 << addr_bits[int(T::Level::Channel)];
    int num_rank = 1 << addr_bits[int(T::Level::Rank)];
    int num_bg = 1 << addr_bits[int(T::Level::BankGroup)];
    int num_bank = 1 << addr_bits[int(T::Level::Bank)];
    int num_col = 1 << addr_bits[int(T::Level::Column)];


    // this is because the data format is 16 bit fp
    int num_dat = spec->channel_width / spec->data_bits * spec->prefetch_size;

    // Mapping Hierarchy and Number of Data in Each Hierarchy
    int num_index = 1;
    int* map_num_dat = new int[int(T::Level::MAX)];
    for (int i = 0; i < int(T::Level::MAX); i++) {
        map_num_dat[i] = 1 << addr_bits[i];
        num_index *= map_num_dat[i];
    }
    int* map_index = memory.map_index;

    double*** mem_dat;
    //double** global_buffers;
    double**** register_files;
    mem_dat = new double **[num_channel];
    for (int channel = 0; channel < num_channel; channel++){
        mem_dat[channel] = new double *[num_index];
        for(int index = 0; index < num_index; index++){
            mem_dat[channel][index] = new double [num_dat];
            for (int dat = 0; dat < num_dat; dat++) mem_dat[channel][index][dat] = std::numeric_limits<double>::infinity();
        }
    }

    register_files = new double ***[num_channel];
    for (int channel = 0; channel < num_channel; channel++) {
        register_files[channel] = new double **[num_rank];
        for (int rank = 0; rank < num_rank; rank++){
            register_files[channel][rank] = new double *[spec->org_entry.register_size];
            for (int reg_index = 0; reg_index < spec->org_entry.register_size; reg_index++) {
                register_files[channel][rank][reg_index] = new double[num_dat];
                for (int dat = 0; dat < num_dat; dat++) register_files[channel][rank][reg_index][dat] = std::numeric_limits<double>::infinity();
            }
        }
    }

    int index = 0;
    for (auto ctrl: memory.ctrls) {
        ctrl->mem_dat = mem_dat[index];
        //ctrl->global_buf = global_buffers[index];
        ctrl->register_file = register_files[index];

        ctrl->num_bg = num_bg;
        ctrl->num_bank = num_bank;
        ctrl->num_col = num_col;
        ctrl->num_dat = num_dat;
        ctrl->map_index = map_index;
        ctrl->map_num_dat = map_num_dat;
        index++;
    }

    vector<list<Request>> req_queues(num_channel);
    int max_queue_length = 8;

    while (!end || memory.pending_requests()){
        for (auto &req_queue : req_queues) {
            if (!req_queue.empty()) {
                auto head = req_queue.front();
                if (memory.send(head)) {
                    req_queue.pop_front();
                }
            }
        }

        if (!end && !stall)
            end = !trace.get_dramtrace_request(addr, regAddr1, regAddr2, regAddr3, type, aluType, data, source_code);

        if (!end){
            req.addr = addr;
            req.regAddr1 = regAddr1;
            req.regAddr2 = regAddr2;
            req.regAddr3 = regAddr3;

            req.type = type;
            req.data = data;
            req.instid = instid;

            req.aluType = aluType;

            req.source_code = source_code;

            req.callback = [&] (Request& req) ->bool {
                // Receive data from memory
                if (req.type == Request::Type::READ ||
                    req.type == Request::Type::MVM) {

                    if (!memory.receive(req)) {
                        return false;
                    }
                }

                res_os.precision(20);
                time_os.precision(20);

                // for statistics
                //char hex_addr[20];
                //sprintf(hex_addr, " 0x%08x", int(req.addr));
                //time_os << req.instid << " ";
                //time_os << print_req(req);
                //time_os << hex_addr << " ";
                if (!req.source_code.empty()) {
                    time_os << req.source_code;
                    time_os << " ISSUED: " << req.issued << endl;

                    cout << req.source_code;
                    cout << " ISSUED: " << req.issued << endl;
                }
                

                // for debugging
                if (req.type == Request::Type::READ ||
                    req.type == Request::Type::WRITE ||
                    req.type == Request::Type::MVM ||
                    req.type == Request::Type::ALUR ||
                    req.type == Request::Type::ALUI ||
                    req.type == Request::Type::SET ||
                    req.type == Request::Type::RDREG ||
                    req.type == Request::Type::WRREG) {

                    res_os << req.instid;
                    res_os << " [";
                    for (auto &dat: req.data) {
                        res_os << dat;
                        if (&dat != &req.data.back()) res_os << ",";
                    }
                    res_os << "]" << endl;
                }

                return true;
            };

            req.addr_bits.assign(addr_bits.begin(), addr_bits.end());
            req.addr_vec.resize(addr_bits.size());
            long addr = req.addr;

            // Each transaction size is 2^tx_bits, so first clear the lowest tx_bits bits
            clear_lower_bits(addr, memory.tx_bits);
            clear_lower_bits(req.regAddr1, memory.tx_bits);
            clear_lower_bits(req.regAddr2, memory.tx_bits);
            clear_lower_bits(req.regAddr3, memory.tx_bits);

            for(int i = int(T::Level::MAX) - 1; i >= 0; i--) {
                string string_dat = memory.mapping_type.substr(i * 2, 2);
                int target_level = memory.string_to_level[string_dat];
                req.addr_vec[target_level] = slice_lower_bits(addr, addr_bits[target_level]);
            }

            int channel = req.addr_vec[0];
            if (req_queues[channel].size() < max_queue_length) {
                req_queues[channel].push_back(req);
                stall = false;
                instid++;
            }
            else {
                stall = true;
            }

            /*
            // this process sets the address and etc of the request
            stall = !memory.send(req);
            if (!stall){
                // PIM FIX: add new requests here
                instid++;
            }
            */
        }
        else {
            memory.set_high_writeq_watermark(0.0f); // make sure that all write requests in the
                                                    // write queue are drained
        }

        memory.tick();
        clks ++;
        Stats::curTick++; // memory clock, global, for Statistics

    }
    // This a workaround for statistics set only initially lost in the end
    memory.finish();
    Stats::statlist.printall();

    for(int channel = 0; channel < num_channel; channel++){
        for(int index = 0; index < num_index; index++) {
            delete [] mem_dat[channel][index];
        }
        delete [] mem_dat[channel];
    }
    delete [] mem_dat;


}

/*
template <typename T>
void run_cputrace(const Config& configs, Memory<T, Controller>& memory, const std::vector<const char *>& files)
{
    int cpu_tick = configs.get_cpu_tick();
    int mem_tick = configs.get_mem_tick();
    auto send = bind(&Memory<T, Controller>::send, &memory, placeholders::_1);
    Processor proc(configs, files, send, memory);

    long warmup_insts = configs.get_warmup_insts();
    bool is_warming_up = (warmup_insts != 0);

    for(long i = 0; is_warming_up; i++){
        proc.tick();
        Stats::curTick++;
        if (i % cpu_tick == (cpu_tick - 1))
            for (int j = 0; j < mem_tick; j++)
                memory.tick();

        is_warming_up = false;
        for(int c = 0; c < proc.cores.size(); c++){
            if(proc.cores[c]->get_insts() < warmup_insts)
                is_warming_up = true;
        }

        if (is_warming_up && proc.has_reached_limit()) {
            printf("WARNING: The end of the input trace file was reached during warmup. "
                    "Consider changing warmup_insts in the config file. \n");
            break;
        }

    }

    warmup_complete = true;
    printf("Warmup complete! Resetting stats...\n");
    Stats::reset_stats();
    proc.reset_stats();
    assert(proc.get_insts() == 0);

    printf("Starting the simulation...\n");

    int tick_mult = cpu_tick * mem_tick;
    for (long i = 0; ; i++) {
        if (((i % tick_mult) % mem_tick) == 0) { // When the CPU is ticked cpu_tick times,
                                                 // the memory controller should be ticked mem_tick times
            proc.tick();
            Stats::curTick++; // processor clock, global, for Statistics

            if (configs.calc_weighted_speedup()) {
                if (proc.has_reached_limit()) {
                    break;
                }
            } else {
                if (configs.is_early_exit()) {
                    if (proc.finished())
                    break;
                } else {
                if (proc.finished() && (memory.pending_requests() == 0))
                    break;
                }
            }
        }

        if (((i % tick_mult) % cpu_tick) == 0) // TODO_hasan: Better if the processor ticks the memory controller
            memory.tick();

    }
    // This a workaround for statistics set only initially lost in the end
    memory.finish();
    Stats::statlist.printall();
}
*/

template<typename T>
void start_run(const Config& configs, T* spec, const vector<const char*>& files) {
    // initiate controller and memory
    int C = configs.get_channels(), R = configs.get_ranks();
    // Check and Set channel, rank number
    spec->set_channel_number(C);
    spec->set_rank_number(R);
    std::vector<Controller<T>*> ctrls;
    for (int c = 0 ; c < C ; c++) {
        DRAM<T>* channel = new DRAM<T>(spec, T::Level::Channel);
        channel->id = c;
        channel->regStats("");
        Controller<T>* ctrl = new Controller<T>(configs, channel);
        ctrls.push_back(ctrl);
    }
    Memory<T, Controller> memory(configs, ctrls);
    
    assert(files.size() != 0);
    run_trace(configs, spec, memory, files[0]);

}

int main(int argc, const char *argv[])
{

    if (argc < 2) {
        printf("Usage: %s <configs-file> --mode=cpu,dram <trace-filename1> <trace-filename2>\n"
            "Example: %s ramulator-configs.cfg --mode=cpu cpu.trace cpu.trace\n", argv[0], argv[0]);
        return 0;
    }

    Config configs(argv[1]);

    const std::string& standard = configs["standard"];
    assert(standard != "" || "DRAM standard should be specified.");

    int trace_start = 2;

    string stats_out = "./result/stat.txt";
    Stats::statlist.output(stats_out);

    // A separate file defines mapping for easy config.
    if (strcmp(argv[trace_start], "--mapping") == 0) {
        configs.add("mapping", argv[trace_start+1]);
        trace_start += 2;
    } else {
        configs.add("mapping", "defaultmapping");
    }

    std::vector<const char*> files(&argv[trace_start], &argv[argc]);
    configs.set_core_num(argc - trace_start);

    if (standard == "DDR4") {
        DDR4* ddr4 = new DDR4(configs["org"], configs["speed"]);
        start_run(configs, ddr4, files);
    } else if (standard == "DDR4_TSDIMM") {
        DDR4_TSDIMM* ddr4_tsdimm = new DDR4_TSDIMM(configs["org"], configs["speed"]);
        start_run(configs, ddr4_tsdimm, files);
    } else if (standard == "HBM") {
        HBM* hbm = new HBM(configs["org"], configs["speed"]);
        start_run(configs, hbm, files);
    } else if (standard == "HBM_PIM") {
        HBM_PIM* hbm_pim = new HBM_PIM(configs["org"], configs["speed"]);
        start_run(configs, hbm_pim, files);
    }


    printf("Simulation done. Results written to timing.txt\n");

    return 0;
}
