#include "Processor.h"
#include <cassert>

using namespace std;
using namespace ramulator;


Trace::Trace(const char* trace_fname) : file(trace_fname), trace_name(trace_fname)
{
    if (!file.good()) {
        std::cerr << "Bad trace file: " << trace_fname << std::endl;
        exit(1);
    }
}


// FIXME: Supports only dramtrace requests
bool Trace::get_dramtrace_request(long& reqAddr, int& regAddr1, int& regAddr2, int& regAddr3
                                  , Request::Type& reqType, Request::ALUType& aluType
                                  , vector<double>& data
                                  , string& source_code)
{

    size_t pos;
    string line;
    getline(file, line);
    if (file.eof()) return false;

    // function to check the opcode
    auto get_opcode = [&] {
        string opcode = line.substr(0, 2);
        if (opcode == "RD")
            reqType = Request::Type::READ;
        else if (opcode == "WR")
            reqType = Request::Type::WRITE;
        else if (opcode == "MV")
            reqType = Request::Type::MVM;
        else if (opcode == "RG")
            reqType = Request::Type::RDREG;
        else if (opcode == "WG")
            reqType = Request::Type::WRREG;
        else if (opcode == "AR")
            reqType = Request::Type::ALUR;
        else if (opcode == "AI")
            reqType = Request::Type::ALUI;
        else if (opcode == "SE")
            reqType = Request::Type::SET;
        else assert(false);
        line = line.substr(2, line.size() - 2);
    };

    // function to check the opcode
    auto get_aluopcode = [&] {
        string opcode = line.substr(0, 3);
        if (opcode == "ADD")
            aluType = Request::ALUType::ADD;
        else if (opcode == "SUB")
            aluType = Request::ALUType::SUB;
        else if (opcode == "MUL")
            aluType = Request::ALUType::MUL;
        else if (opcode == "DIV")
            aluType = Request::ALUType::DIV;
        else assert(false);
        line = line.substr(4, line.size() - 4);
    };

    // address parsing function
    auto parse_addr = [&] (int dec) {
        // get the first two
        long addr_temp = std::stoul(line, &pos, dec);
        line = line.substr(pos, line.size() - pos);
        return addr_temp;
    };

    get_opcode();
    if(reqType == Request::Type::MAX) assert(false);

    if (reqType == Request::Type::ALUR) {
        line = line.substr(1, line.size() - 1);
        get_aluopcode();
        reqAddr = parse_addr(16);

        regAddr1 = parse_addr(16);
        regAddr2 = parse_addr(16);
        regAddr3 = parse_addr(16);
    }
    else if (reqType == Request::Type::ALUI) {
        line = line.substr(1, line.size() - 1);
        get_aluopcode();
        reqAddr = parse_addr(16);

        regAddr1 = parse_addr(16);
        regAddr3 = parse_addr(16);
    }
    else if (reqType == Request::Type::SET) {
        reqAddr = parse_addr(16);
        regAddr3 = parse_addr(16);
    }
    else if (reqType == Request::Type::RDREG) {
        reqAddr = parse_addr(16);
        regAddr3 = parse_addr(16);
    }
    else if (reqType == Request::Type::WRREG) {
        reqAddr = parse_addr(16);
        regAddr1 = parse_addr(16);
    }
    else {
        reqAddr = parse_addr(16);
    }

    // Data read
    data.clear();
    //if (reqType == Request::Type::WRITE ||
    //    reqType == Request::Type::WRREG ||
    //    reqType == Request::Type::ALUI ||
    //    reqType == Request::Type::SET) {

    //    pos = line.find(' ');
    //    string data_str = line.substr(pos + 1);
    //    size_t start;
    //    size_t end = 0;
    //    while ((start = data_str.find_first_not_of(' ', end)) != string::npos) {
    //        end = data_str.find(' ', start);
    //        string temp = data_str.substr(start, end - start);
    //        double dat = stod(data_str.substr(start, end - start));
    //        data.push_back(dat);
    //    }
    //}

    pos = line.find(' ');
    source_code = line.substr(pos + 1);

    return true;
}
