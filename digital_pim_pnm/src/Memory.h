#ifndef __MEMORY_H
#define __MEMORY_H

#include "Config.h"
#include "DRAM.h"
#include "Request.h"
#include "Controller.h"
#include "Statistics.h"
#include "HBM.h"
#include "HBM_PIM.h"
#include <vector>
#include <functional>
#include <cmath>
#include <cassert>
#include <tuple>

using namespace std;

typedef vector<unsigned int> MapSrcVector;
typedef map<unsigned int, MapSrcVector > MapSchemeEntry;
typedef map<unsigned int, MapSchemeEntry> MapScheme;

namespace ramulator
{

class MemoryBase{
public:
    MemoryBase() {}
    virtual ~MemoryBase() {}
    virtual double clk_ns() = 0;
    virtual void tick() = 0;
    virtual bool send(Request req) = 0;     // a virtual function for sending the data
    virtual bool receive(Request req) = 0;  // a virtual function for receiving the data
    virtual int pending_requests() = 0;
    virtual void finish(void) = 0;
    virtual void set_high_writeq_watermark(const float watermark) = 0;
    virtual void set_low_writeq_watermark(const float watermark) = 0;
};

template <class T, template<typename> class Controller = Controller >
class Memory : public MemoryBase
{
protected:
  ScalarStat dram_capacity;

  ScalarStat maximum_bandwidth;



  long max_address;
  MapScheme mapping_scheme;
  
public:
    // Type should be the shown ordering

    // This is for HBM_PIM
    //string type = "ChRaRoBgBaCo";
    // This is for HBM_TSDIMM
    //string type = "RoCoBgBaChRa";
    string mapping_type;


    //enum class Type {
    //    ChRaBaRoCo,     // column first mapping
    //    RoBaRaCoCh,     // channel first mapping
    //    MAX,
    //} type = Type::RoBaRaCoCh;

    // level to string mapping
    map<string, int> string_to_level = {
        {"Ch", 0},
        {"Ra", 1},
        {"Bg", 2},
        {"Ba", 3},
        {"Ro", 4},
        {"Co", 5}
    };

    int* map_index;


    vector<int> free_physical_pages;
    long free_physical_pages_remaining;
    map<pair<int, long>, long> page_translation;

    vector<Controller<T>*> ctrls;
    T * spec;
    vector<int> addr_bits;

    int tx_bits;

    Memory(const Config& configs, vector<Controller<T>*> ctrls)
        : ctrls(ctrls),
          spec(ctrls[0]->channel->spec),
          addr_bits(int(T::Level::MAX))
    {
        mapping_type = configs.get_mapping_type();
        map_index = new int [int(T::Level::MAX)];
        // Set mapping info to the controller

        // make sure 2^N channels/ranks
        // TODO support channel number that is not powers of 2
        int *sz = spec->org_entry.count;
        assert((sz[0] & (sz[0] - 1)) == 0);
        assert((sz[1] & (sz[1] - 1)) == 0);
        // validate size of one transaction
        // tx => transfer in bytes => assumes byte addressing
        // single address contains byte of prefetch size * channel_width
        int tx = (spec->prefetch_size * spec->channel_width / 8);
        tx_bits = calc_log2(tx);
        assert((1<<tx_bits) == tx);


        max_address = spec->channel_width / 8;

        for (unsigned int lev = 0; lev < addr_bits.size(); lev++) {
            addr_bits[lev] = calc_log2(sz[lev]);
            max_address *= sz[lev];
        }

        addr_bits[int(T::Level::MAX) - 1] -= calc_log2(spec->prefetch_size);
        // FIXME add commands
        addr_bits[int(T::Level::MAX) - 1] -= calc_log2(spec->channel_width / spec->org_entry.dq);

        for(int i = int(T::Level::MAX) - 1; i >= 0; i--) {
            string string_dat = mapping_type.substr(i * 2, 2);
            int target_level = string_to_level[string_dat];
            assert(string_to_level.find(string_dat) != string_to_level.end());
            map_index[i] = target_level;
        }

        dram_capacity
            .name("dram_capacity")
            .desc("Number of bytes in simulated DRAM")
            .precision(0);
        dram_capacity = max_address;
        maximum_bandwidth
            .name("maximum_bandwidth")
            .desc("The theoretical maximum bandwidth (Bps)")
            .precision(0);

    }

    ~Memory()
    {
        for (auto ctrl: ctrls)
            delete ctrl;
        delete spec;
    }

    double clk_ns()
    {
        return spec->speed_entry.tCK;
    }


    void tick()
    {
        bool is_active = false;
        for (auto ctrl : ctrls) {
            is_active = is_active || ctrl->is_active();
            ctrl->tick();
        }
    }

    bool send(Request req)
    {
        int channel = req.addr_vec[0];

        if (ctrls[channel]->is_receiving || ctrls[channel]->pending.size() > 0)
            return false;

        if (!ctrls[channel]->is_sending && ctrls[channel]->send_available) {
            assert(ctrls[channel]->send_cycles == 0);

            int data_size = req.data.size() * spec->data_bits;
            ctrls[channel]->send_cycles = data_size > 0 ? ceil(data_size / spec->host_memory_bw) : 1;
            ctrls[channel]->is_sending = true;
        }

        // Host to memory latency
        if (ctrls[channel]->send_cycles > 0) {
            if (--(ctrls[channel]->send_cycles) > 0)
                return false;
            else
                ctrls[channel]->is_sending = false;
        }

        // Request enqueue
        if(ctrls[channel]->enqueue(req)) { // addr_vec[0]: channel addr
            // tally stats here to avoid double counting for requests that aren't enqueued
            // PIM FIX: add instructions here!
            ctrls[channel]->send_available = true;
            return true;
        }
        else {
            ctrls[channel]->send_available = false;
            return false;
        }
    }

    bool receive(Request req)
    {
        int channel = req.addr_vec[0];

        if (ctrls[channel]->is_sending)
            return false;

        if (!ctrls[channel]->is_receiving) {
            assert(ctrls[channel]->receive_cycles == 0);

            int data_size = req.data.size() * spec->data_bits;
            ctrls[channel]->receive_cycles = data_size > 0 ? ceil(data_size / spec->host_memory_bw) : 1;
            ctrls[channel]->is_receiving = true;
        }

        if (ctrls[channel]->receive_cycles > 0) {
            ctrls[channel]->receive_cycles -= 1;
            if (ctrls[channel]->receive_cycles > 0)
                return false;
        }

        ctrls[channel]->is_receiving = false;
        return true;
    }


    int pending_requests()
    {
        int reqs = 0;
        for (auto ctrl: ctrls)
            reqs += ctrl->readq.size() + ctrl->writeq.size() + ctrl->otherq.size() + ctrl->actq.size() + ctrl->pending.size();
        return reqs;
    }

    void set_high_writeq_watermark(const float watermark) {
        for (auto ctrl: ctrls)
            ctrl->set_high_writeq_watermark(watermark);
    }

    void set_low_writeq_watermark(const float watermark) {
    for (auto ctrl: ctrls)
        ctrl->set_low_writeq_watermark(watermark);
    }

    void finish(void) {
        dram_capacity = max_address;
        int *sz = spec->org_entry.count;
        maximum_bandwidth = spec->speed_entry.rate * 1e6 * spec->channel_width * sz[int(T::Level::Channel)] / 8;
    }

private:

    int calc_log2(int val){
        int n = 0;
        while ((val >>= 1))
            n ++;
        return n;
    }
    bool get_bit_at(long addr, int bit)
    {
        return (((addr >> bit) & 1) == 1);
    }
    long lrand(void) {
        if(sizeof(int) < sizeof(long)) {
            return static_cast<long>(rand()) << (sizeof(int) * 8) | rand();
        }

        return rand();
    }
};

} /*namespace ramulator*/

#endif /*__MEMORY_H*/
