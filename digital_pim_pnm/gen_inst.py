import os
import sys
import numpu as np
import pickle
import copy


if sys.argv[1] == "PNM":
    os.system('python pnm_inst.py IR ./workload/PNM/' + sys.argv[2])
elif sys.argv[1] == "DPIM":
    os.system('python dpim_inst.py IR ./workload/DPIM/' + sys.argv[2])
else:
    assert("Not available PIM Name" and 0)
