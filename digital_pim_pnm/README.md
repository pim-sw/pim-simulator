# Ramulator + DRAM PIM + PNM (TSDIMM)

to run dpim
1) python dpim_inst.py Gen               (Generate the trace from scratch)
  or
1) python dpim_inst.py IR <.trace file>  (Generate the trace from IR)

2) ./ramulator_pim configs/HBM_PIM-config.cfg dpim.trace

to run pnm
1) python pnm_inst.py Gen               (Generate the trace from scratch)
  or
1) python pnm_inst.py IR <.trace file>  (Generate the trace from IR)

2) ./ramulator_pim configs/DDR4_PNM-config.cfg pnm.trace
