import sys
import os
import numpy as np
import GlobalVars as gv
import Profile as pf
import NoC
import Tile
from collections import deque
from collections import defaultdict
from queue import PriorityQueue
from cProfile import Profile
from pstats import Stats
import config as cfg
import Operations
from pathlib import Path
import time


def init():
    # workload name
    cfg.change_config()
    gv.params["workload"] = sys.argv[1]
    gv.params["foldername"] = "workload/%s" % (sys.argv[1])
    workload = sys.argv[1]
    cfg.SIM_ver = sys.argv[2]
    assert (cfg.SIM_ver == "Clock" or cfg.SIM_ver == "Event")
    gv.params["result_foldername"] = "result/%s_%s" %(workload, cfg.SIM_ver)
    gv.params["tile_x"] = 12
    if not os.path.isdir(gv.params["foldername"]):
        raise ("No workload folder exists")
    path = Path(gv.params["result_foldername"])
    path.mkdir(parents=True, exist_ok=True)

    os.listdir()
    total_cores = 0
    total_tiles = 0

    ##
    for _, dirnames, filenames in os.walk(gv.params["foldername"]):
        for file in filenames:
            if "core_" in file and ".swp" not in file:
                total_cores += 1
        total_tiles += len(dirnames)

    gv.fifo_num = total_tiles

    assert (total_cores % total_tiles == 0)

    ##
    gv.params["tile_num"] = int(total_tiles)
    gv.params["core_num"] = int(total_cores / total_tiles)
    gv.params["total_core_num"] = int(total_cores)
    gv.params["active_core_num"] = 0

    pf.end_cyc_per_tile = [[] for _ in range(gv.params["tile_num"])]
    pf.end_cyc_per_core = [[[] for _ in range(gv.params["core_num"])] for _ in range(gv.params["tile_num"])]

    chip_num = (gv.params["tile_num"] - 1) // (cfg.max_tile_x_in_chip * cfg.max_tile_y_in_chip) + 1

    gv.params["max_chip_x"] = int(np.sqrt(chip_num))
    gv.params["max_chip_y"] = (chip_num - 1) // gv.params["max_chip_x"] + 1
    gv.params["max_tile_x"] = gv.params["max_chip_x"] * cfg.max_tile_x_in_chip
    gv.params["max_tile_y"] = gv.params["max_chip_y"] * cfg.max_tile_y_in_chip

    assert (cfg.max_tile_x_in_chip % 2 == 0 and cfg.max_tile_y_in_chip % 2 == 0)

    gv.params["max_switch_x"] = int(gv.params["max_tile_x"] / 2)
    gv.params["max_switch_y"] = int(gv.params["max_tile_y"] / 2)
    gv.params["switch_num"] = int(gv.params["tile_num"] / 4)

    gv.NoC = NoC.NoC()

    gv.max_buffer_size = [[0 for _ in range(gv.params["max_switch_y"])]
                          for _ in range(gv.params["max_switch_x"])]


def simulate():
    pf.cyc = 0
    # halted_num_prev = gv.halted_core_num
    print('simulation started')
    start_time = time.time()
    while gv.halted_core_num < gv.params["total_core_num"]:
        pf.cyc += 1
        gv.NoC.advance()
    end_time = time.time()
    elapsed_time_event = end_time - start_time
    return elapsed_time_event


def stat(elapsed_time_event):
    result_path = os.path.join(gv.params["result_foldername"], "func.txt")
    result_file = open(result_path, "w")
    util_path = os.path.join(gv.params["result_foldername"], "util.txt")
    util_file = open(util_path, "w")
    latency_path = os.path.join(gv.params["result_foldername"], "latency.txt")
    latency_file = open(latency_path, "w")
    cycle_path = os.path.join(gv.params["result_foldername"], "cycle.txt")
    cycle_file = open(cycle_path, "w")

    result_file.write("\n====Latency ====\n")
    result_file.write("total cyc: {}\n\n".format(pf.cyc))

    result_file.write("\n====Call Stack ====\n")
    for k, v in pf.call_stack.items():
        result_file.write("{} : {}\n".format(k, v))

    elapsed_path = os.path.join(gv.params["result_foldername"], "elapsed.txt")
    elapsed_file = open(elapsed_path, "w")
    elapsed_file.write("%f"%(elapsed_time_event))


    for img in range(1, cfg.image_num):
        total = (pf.end_cyc_per_tile[1][img] - pf.end_cyc_per_tile[1][img - 1]) * gv.params["active_core_num"]
        mvm_cyc = 0
        pseudo_mvm_cyc = 0
        for tile in gv.NoC.tiles:
            for core in tile.cores:
                mvm_cyc += core.mvmu.act_cycles[img]
                pseudo_mvm_cyc += core.mvmu.pseudo_act_cycles[img]
                pseudo_mvm_cyc += core.alu.act_cycles[img]

    print()
    print("Simulation done. Results written to timing.txt");


def testrun():
    init()
    elapsed_time_event = simulate()
    stat(elapsed_time_event)
    return elapsed_time_event

testrun()
