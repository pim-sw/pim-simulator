# Copy this file to compiler test folder where the .npy files are generated.
# Update SIMULATOR_PATH value and execute it.

# FIXME change here to the simulator root dir
SIMULATOR_PATH="/home/minsub/puma/inst-sim"

if [[ $SIMULATOR_PATH == "" ]] ; then
    print "Error, missing simulator path."
    exit
fi


PYTHON=python
WORKLOAD=$1

for g in $WORKLOAD*.puma; do
    echo $g
    # Parse tile and core ids
    ar=(${g//[-]/ })
    dataset=${ar[0]}
    tileid=$(echo $g | grep -o -E 'tile[0-9]+' | head -1)

    if [[ $g == *"core"* ]]; then
        #coredid=$(echo $g | cut -d " " -f $N)
        coreid="$(echo $g | grep -o -E 'core[0-9]+' | head -1)"
        coreid="$(echo $coreid | grep -o -E '[0-9]+')"
        filename='core_imem'$coreid
    else
        filename='tile_imem'
    fi
    mkdir -p $dataset/$tileid
    dir=$dataset/$tileid
    f=$dataset/$tileid/$g
    rm $f.py

    ./generate-py $g $f.py $SIMULATOR_PATH $dir $filename && $PYTHON $f.py &
done

    #echo "" > $f.py
    #echo "import sys, os" >> $f.py
    #echo "import numpy as np" >> $f.py
    #echo "import math" >> $f.py
    #echo "sys.path.insert (0, '$SIMULATOR_PATH/')" >> $f.py
    #echo "from data_convert import *" >> $f.py
    #echo "from instrn_proto import *" >> $f.py
    #echo "from tile_instrn_proto import *" >> $f.py
    #echo "dict_temp = {}" >> $f.py
    #echo "dict_list = []" >> $f.py
    #while read line
    #do
    #    echo "i_temp = i_$line" >> $f.py
    #    echo "dict_list.append(i_temp.copy())" >> $f.py
    #done < $g
    #echo "filename = '$dir/$filename.npy'" >> $f.py
    #echo "np.save(filename, dict_list)" >> $f.py
