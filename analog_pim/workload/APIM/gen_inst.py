import sys, os, glob
import shutil
import numpy as np

INITIAL_TILE_NUM = 2
CORE_NUM = 12
XBAR_NUM = 4

core_inst_list = [[[] for _ in range(CORE_NUM)] for _ in range(INITIAL_TILE_NUM)]
tile_inst_list = [[] for _ in range(INITIAL_TILE_NUM)]

ir_file = open(sys.argv[1], "r")
ir_lines = ir_file.readlines()

input_reg_offset = 0
output_reg_offset = input_reg_offset + 128 * XBAR_NUM
accum_reg_offset = output_reg_offset + 128 * XBAR_NUM
data_reg_offset = accum_reg_offset + 128 * XBAR_NUM

filelist = glob.glob('tile*')
for f in filelist:
    shutil.rmtree(f)

for ir_inst in ir_lines:
    ir_source = ir_inst
    ir_inst = ir_inst.strip();
    ir_inst = ir_inst[5:]

    if ir_inst[:4] == "load":
        ir_inst = ir_inst[5:-1].split(", ")

        tile_idx = int(ir_inst[0].split()[-1]) + 2
        core_idx = int(ir_inst[1].split()[-1])
        xbar_idx = int(ir_inst[2].split()[-1])
        reg_addr = data_reg_offset + int(ir_inst[3].split()[-1]) * 128
        vector_len = int(ir_inst[4])

        xbar_list = ['0' for _ in range(XBAR_NUM)]
        xbar_list[xbar_idx] = '1'

        if len(core_inst_list) < tile_idx + 1:
            for _ in range(tile_idx + 1 - len(core_inst_list)):
                core_inst_list.append([[] for _ in range(CORE_NUM)])
                tile_inst_list.append([])

        core_inst_list[tile_idx][core_idx].append({'opcode': 'xbar_ld', 'xb_nma': xbar_list})
        core_inst_list[tile_idx][core_idx].append({'opcode': 'cp', 'd1': reg_addr, 'r1': output_reg_offset + 128 * xbar_idx, \
                                                    'vec': vector_len, 'intermediate': False, "source_code" : ir_source})

    elif ir_inst[:5] == "store":
        ir_inst = ir_inst[6:-1].split(", ")

        tile_idx = int(ir_inst[0].split()[-1]) + 2
        core_idx = int(ir_inst[1].split()[-1])
        xbar_idx = int(ir_inst[2].split()[-1])
        vector_len = int(ir_inst[3])

        xbar_list = ['0' for _ in range(XBAR_NUM)]
        xbar_list[xbar_idx] = '1'

        if len(core_inst_list) < tile_idx + 1:
            for _ in range(tile_idx + 1 - len(core_inst_list)):
                core_inst_list.append([[] for _ in range(CORE_NUM)])
                tile_inst_list.append([])

        core_inst_list[tile_idx][core_idx].append({'opcode': 'cp', 'd1': input_reg_offset + 128 * xbar_idx, 'r1': accum_reg_offset, \
                                                    'vec': vector_len, 'intermediate': False})
        core_inst_list[tile_idx][core_idx].append({'opcode': 'xbar_st', 'xb_nma': xbar_list, "source_code" : ir_source})

    elif ir_inst[:10] == "vector_acc":
        ir_inst = ir_inst[11:-1].split(", ")

        tile_idx = int(ir_inst[0].split()[-1]) + 2
        core_idx = int(ir_inst[1].split()[-1])
        xbar_idx = int(ir_inst[2].split()[-1])
        vector_len = int(ir_inst[3])

        dst_reg_addr = accum_reg_offset + 128 * xbar_idx

        xbar_list = ['0' for _ in range(XBAR_NUM)]
        xbar_list[xbar_idx] = '1'

        if len(core_inst_list) < tile_idx + 1:
            for _ in range(tile_idx + 1 - len(core_inst_list)):
                core_inst_list.append([[] for _ in range(CORE_NUM)])
                tile_inst_list.append([])

        core_inst_list[tile_idx][core_idx].append({'opcode': 'xbar_ld', 'xb_nma': xbar_list})
        core_inst_list[tile_idx][core_idx].append({'opcode': 'alu', 'aluop': 'add', 'd1': dst_reg_addr, \
                                                    'r1': dst_reg_addr, 'r2': output_reg_offset + 128 * xbar_idx, \
                                                    'vec': 128, 'intermediate': False, "source_code" : ir_source})

    elif ir_inst[:6] == "vector":
        ir_inst = ir_inst[7:-1].split(", ")

        tile_idx = int(ir_inst[1].split()[-1]) + 2
        core_idx = int(ir_inst[2].split()[-1])
        xbar_idx = int(ir_inst[3].split()[-1])
        vector_len = int(ir_inst[4])
        operation = ir_inst[5]

        src = ir_inst[0].split()[0][:-1]
        if src == "reg":
            src_reg_addr = data_reg_offset + int(ir_inst[0].split()[-1]) * 128
        elif src == "imm":
            imm_val = int(float(ir_inst[0].split()[-1]))

        dst_reg_addr = accum_reg_offset + 128 * xbar_idx

        xbar_list = ['0' for _ in range(XBAR_NUM)]
        xbar_list[xbar_idx] = '1'

        if len(core_inst_list) < tile_idx + 1:
            for _ in range(tile_idx + 1 - len(core_inst_list)):
                core_inst_list.append([[] for _ in range(CORE_NUM)])
                tile_inst_list.append([])

        core_inst_list[tile_idx][core_idx].append({'opcode': 'xbar_ld', 'xb_nma': xbar_list})
        if src == "reg":
            core_inst_list[tile_idx][core_idx].append({'opcode': 'alu', 'aluop': 'mul', 'd1': output_reg_offset + 128 * xbar_idx, \
                                                        'r1': output_reg_offset + 128 * xbar_idx, 'r2': src_reg_addr, \
                                                        'vec': 128, 'intermediate': False})
        elif src == "imm":
            core_inst_list[tile_idx][core_idx].append({'opcode': 'alui', 'aluop': 'mul', 'd1': output_reg_offset + 128 * xbar_idx, \
                                                        'r1': output_reg_offset + 128 * xbar_idx, 'imm': imm_val, 'r2': -1, \
                                                        'vec': 128, 'intermediate': False})
        core_inst_list[tile_idx][core_idx].append({'opcode': 'alu', 'aluop': 'add', 'd1': dst_reg_addr, \
                                                    'r1': dst_reg_addr, 'r2': output_reg_offset + 128 * xbar_idx, \
                                                    'vec': 128, 'intermediate': False, "source_code" : ir_source})

    else:
        assert(False)


for tile_idx in range(len(core_inst_list)):
    os.mkdir("tile" + str(tile_idx))

    for core_idx, inst_list in enumerate(core_inst_list[tile_idx]):
        inst_list.append({'opcode': 'hlt'})
        filename = "tile" + str(tile_idx) + "/core_imem" + str(core_idx) + ".npy"
        np.save(filename, inst_list)

    tile_inst_list[tile_idx].append({'opcode': 'halt'})
    filename = "tile" + str(tile_idx) + "/tile_imem.npy"
    np.save(filename, tile_inst_list[tile_idx])
