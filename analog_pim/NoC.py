import GlobalVars as gv
import config as cfg
from queue import PriorityQueue
from collections import deque
import Tile
import Profile as pf
import sys
import math

import Operations
import Controller


class Switch:
    # CMESH structure
    # |tile2|tile3|
    #       S
    # |tile0|tile1|
    def __init__(self, switch_x, switch_y, noc):
        self.noc = noc

        # Switch position
        self.switch_x = switch_x
        self.switch_y = switch_y

        # Switch position inside the chip
        self.switch_x_in_chip = self.switch_x % cfg.max_switch_x_in_chip
        self.switch_y_in_chip = self.switch_y % cfg.max_switch_y_in_chip

        # chip index of the switch
        self.chip_x = self.switch_x // cfg.max_switch_x_in_chip
        self.chip_y = self.switch_y // cfg.max_switch_y_in_chip

        # is Switch edge?
        self.is_edge = [False, False, False, False]  # LEFT, BOT, TOP, RIGHT
        if self.switch_x_in_chip == 0:
            self.is_edge[gv.Direction.LEFT.value] = True
        if self.switch_y_in_chip == 0:
            self.is_edge[gv.Direction.BOT.value] = True
        if self.switch_y_in_chip == cfg.max_switch_y_in_chip - 1:
            self.is_edge[gv.Direction.TOP.value] = True
        if self.switch_x_in_chip == cfg.max_switch_x_in_chip - 1:
            self.is_edge[gv.Direction.RIGHT.value] = True

        # Connected tile
        self.tiles = [None, None, None, None]
        # Adjacent switches
        self.switches = [None, None, None, None]

        ######
        # 0~3 : LEFT  |  BOT  |  TOP  | RIGHT
        # 4~7 : TILE0 | TILE1 | TILE2 | TILE3
        # adjacent data to transfer
        self.o_data = [None for _ in range(8)]
        # fifo queue to keep transient packets
        self.o_buf = [deque() for _ in range(8)]
        # the capacity of the fifo queue
        self.o_buf_cap = [0 for _ in range(8)]

        # to emulate transfer latency
        self.o_lat_buf = [deque() for _ in range(8)]
        # to emulate bandwidth
        self.o_buf_avail = [-1, -1, -1, -1]

    # check if one of the buffer is full
    def isFull(self):
        for d in range(8):
            if self.o_buf_cap[d] >= cfg.tile_buf_size:
                return True
        return False

    # profile the buffer capacity
    def profile_buf_size(self):
        for d in range(8):
            if self.o_buf_cap[d] > gv.max_buffer_size[self.switch_x][self.switch_y]:
                gv.max_buffer_size[self.switch_x][self.switch_y] = self.o_buf_cap[d]

    # calculate the direction to transfer the packet
    def calc_direction(self, dst_tile_id):
        dst_switch_x = self.noc.tiles[dst_tile_id].switch.switch_x
        dst_switch_y = self.noc.tiles[dst_tile_id].switch.switch_y

        switch_dx = dst_switch_x - self.switch_x
        switch_dy = dst_switch_y - self.switch_y

        if switch_dx < 0:
            direction = gv.Direction.LEFT.value
        elif switch_dx > 0:
            direction = gv.Direction.RIGHT.value
        elif switch_dy < 0:
            direction = gv.Direction.BOT.value
        elif switch_dy > 0:
            direction = gv.Direction.TOP.value
        elif dst_tile_id % 4 == 0:
            direction = 4
        elif dst_tile_id % 4 == 1:
            direction = 5
        elif dst_tile_id % 4 == 2:
            direction = 6
        elif dst_tile_id % 4 == 3:
            direction = 7
        else:
            assert 0

        return direction

    # receive the data from the adjacent switches / connected tiles
    def put_buf(self, pck):
        if pck is None:
            return

        direction = self.calc_direction(pck[gv.Packet.DST_ID.value])

        packet_num = pck[gv.Packet.NUM_PCK.value]
        if direction <= 3:
            self.o_buf[direction].append(pck)
            self.o_buf_cap[direction] += packet_num

            # FIXME (Event-Driven Mode)
            assert (type(self) is Switch)
            self.noc.ctrl.insert_switch(1, self)
        else:
            wrapped_packet = [None for _ in range(len(gv.Wrapped_Packet))]
            wrapped_packet[gv.Wrapped_Packet.CYC.value] = gv.cyc + packet_num
            wrapped_packet[gv.Wrapped_Packet.PACKET.value] = pck

            self.o_buf[direction].append(wrapped_packet)
            self.o_buf_cap[direction] += packet_num
            assert (type(self) is Switch)
            self.noc.ctrl.insert_switch(packet_num, self)

    # receive the data from the adjacent switches
    def advance(self):
        # Receive packets iterate over the four directions (router)
        # get from adjacent switches
        for d in range(4):
            # From neighbor switch
            if not self.is_edge[d]:
                self.put_buf(self.switches[d].o_data[3 - d])
                self.switches[d].o_data[3 - d] = None

            # From neighbor MS_block
            else:
                if d == 0 or d == 3:
                    is_horizontal = True
                else:
                    is_horizontal = False

                if is_horizontal:
                    self.put_buf(self.switches[d].o_data[self.switch_y_in_chip])
                    self.switches[d].o_data[self.switch_y_in_chip] = None
                else:
                    self.put_buf(self.switches[d].o_data[self.switch_x_in_chip])
                    self.switches[d].o_data[self.switch_x_in_chip] = None

        # get from connected tiles
        for d in range(4):
            if self.tiles[d] is not None:
                self.put_buf(self.tiles[d].o_data)
                self.tiles[d].o_data = None

            if self.o_data[4 + d] is not None:
                assert self.tiles[d] is not None

                vtile_id = self.o_data[4 + d][gv.Packet.SRC_ID.value]
                packet_num = self.o_data[4 + d][gv.Packet.NUM_PCK.value]
                self.tiles[d].fifo.allocate(vtile_id, packet_num, self.o_data[4 + d])
                # self.noc.ctrl.insert_tile(1, self.tiles[d])
                self.o_data[4 + d] = None

        # profile the buffer size after advance
        self.profile_buf_size()

    # calc_next: packets to send in the next cycle
    def calc_next(self):
        # prepare o_data for the adjacent switches
        for d in range(4):
            # first send to the latency buffer (lat_buf)
            if self.o_buf[d]:
                # check if the buffer is available!
                if gv.cyc >= self.o_buf_avail[d]:
                    # pop the data
                    pop_dat = self.o_buf[d].popleft()
                    packet_num = pop_dat[gv.Packet.NUM_PCK.value]

                    self.o_buf_cap[d] -= packet_num

                    # the wrapped packet has (arrival time, packet)
                    wrapped_packet = [0 for _ in range(len(gv.Wrapped_Packet))]
                    wrapped_packet[gv.Wrapped_Packet.CYC.value] = gv.cyc + cfg.tile_delay
                    wrapped_packet[gv.Wrapped_Packet.PACKET.value] = pop_dat

                    self.o_lat_buf[d].append(wrapped_packet)
                    self.o_buf_avail[d] = gv.cyc + packet_num

                    # activate self after latency
                    assert (type(self) is Switch)
                    self.noc.ctrl.insert_switch(cfg.tile_delay, self)
                else:
                    assert (type(self) is Switch)
                    # activate again after the buffer becomes available
                    self.noc.ctrl.insert_switch(self.o_buf_avail[d] - gv.cyc, self)

            # check the latency buffer
            if self.o_lat_buf[d]:
                dat_check = self.o_lat_buf[d][0]
                if dat_check[gv.Wrapped_Packet.CYC.value] <= gv.cyc:
                    self.o_data[d] = self.o_lat_buf[d].popleft()[gv.Wrapped_Packet.PACKET.value]

                    # activate the neighboring switches
                    if type(self.switches[d]) is Switch:
                        assert (type(self.switches[d]) is Switch)
                        self.noc.ctrl.insert_switch(1, self.switches[d])
                    elif type(self.switches[d]) is MergeSplit:
                        assert (type(self.switches[d]) is MergeSplit)
                        self.noc.ctrl.insert_merge_split(1, self.switches[d])

                else:
                    self.noc.ctrl.insert_switch(dat_check[gv.Wrapped_Packet.CYC.value] - gv.cyc, self)
                    self.o_data[d] = None
            else:
                self.o_data[d] = None

        # prepare o_data for the connected tiles
        for d in range(4):
            if self.o_buf[4 + d] and gv.cyc >= self.o_buf[4 + d][0][gv.Wrapped_Packet.CYC.value]:
                pop_dat = self.o_buf[4 + d].popleft()[gv.Wrapped_Packet.PACKET.value]
                packet_num = pop_dat[gv.Packet.NUM_PCK.value]
                self.o_buf_cap[4 + d] -= packet_num

                wrapped_packet = [0 for _ in range(len(gv.Wrapped_Packet))]
                wrapped_packet[gv.Wrapped_Packet.CYC.value] = gv.cyc + cfg.tile_delay
                wrapped_packet[gv.Wrapped_Packet.PACKET.value] = pop_dat

                self.o_lat_buf[4 + d].append(wrapped_packet)

                assert (type(self) is Switch)
                self.noc.ctrl.insert_switch(cfg.tile_delay, self)

            if self.o_lat_buf[4 + d]:
                dat_check = self.o_lat_buf[4 + d][0]
                if dat_check[gv.Wrapped_Packet.CYC.value] <= gv.cyc:
                    self.o_data[4 + d] = self.o_lat_buf[4 + d].popleft()[gv.Wrapped_Packet.PACKET.value]

                    assert (type(self) is Switch)
                    self.noc.ctrl.insert_switch(1, self)

                else:
                    self.noc.ctrl.insert_switch(dat_check[gv.Wrapped_Packet.CYC.value] - gv.cyc, self)
                    self.o_data[4 + d] = None
            else:
                self.o_data[4 + d] = None

        self.profile_buf_size()


class MergeSplit:
    def __init__(self, chip_x, chip_y, direction, noc):
        self.noc = noc
        # Chip position
        self.chip_x = chip_x
        self.chip_y = chip_y

        # Direction
        self.direction = direction
        self.is_horizontal = True if direction == 0 or direction == 3 else False
        self.length = cfg.max_switch_y_in_chip \
            if self.is_horizontal else cfg.max_switch_x_in_chip

        # Neighbor switch & ms_block
        self.merging_switches = []

        # neighboring block
        self.neighbor_block = None

        # Tile-side data & buffer
        self.o_data = [None for _ in range(self.length)]
        self.o_buf = [deque() for _ in range(self.length)]
        self.o_buf_cap = [0 for _ in range(self.length)]
        self.o_lat_buf = [deque() for _ in range(self.length)]

        self.o_buf_avail = [-1 for _ in range(self.length)]

        # Chip-side data & buffer
        self.chip_o_data = None
        self.chip_o_buf = deque()
        self.chip_o_lat_buf = deque()
        self.chip_o_lat_cap = 0

        self.chip_o_buf_avail = -1

    def isFull(self):
        for d in range(self.length):
            if self.o_buf_cap[d] >= cfg.tile_buf_size:
                return True
        if self.o_chip_buf_cap >= cfg.tile_buf_size * self.length:
            return True
        return False

    def put_buf(self, pck, packet_pos):
        if pck is None:
            return

        packet_num = pck[gv.Packet.NUM_PCK]
        if packet_pos >= 0:
            pck.append(packet_pos)
            self.chip_o_buf.append(pck)
            self.chip_o_lat_cap += packet_num
            # self.noc.ctrl.insert_switch(0, self)
            assert (type(self) is MergeSplit)
            self.noc.ctrl.insert_merge_split(1, self)
        else:
            packet_pos = pck.pop()
            self.o_buf[packet_pos].append(pck)
            self.o_buf_cap[packet_pos] += packet_num

            assert (type(self) is MergeSplit)
            self.noc.ctrl.insert_merge_split(1, self)

    def advance(self):

        for switch in self.merging_switches:
            if self.is_horizontal:
                packet_pos = switch.switch_y_in_chip
            else:
                packet_pos = switch.switch_x_in_chip

            # switch.o_data[self.direction] == None => problem
            self.put_buf(switch.o_data[self.direction], packet_pos)
            switch.o_data[self.direction] = None

        if self.neighbor_block is not None:
            # self.neighbor_block.chip_o_data == None => problem
            assert (type(self.neighbor_block) is MergeSplit)

            self.put_buf(self.neighbor_block.chip_o_data, -1)
            self.neighbor_block.chip_o_data = None

    def calc_next(self):
        for l in range(self.length):
            if self.o_buf[l]:
                if gv.cyc >= self.o_buf_avail[l]:
                    packet_num = self.o_buf[l][0][gv.Packet.NUM_PCK]

                    pop_dat = self.o_buf[l].popleft()
                    self.o_buf_cap[l] -= packet_num

                    wrapped_packet = [0 for _ in range(len(gv.Wrapped_Packet))]
                    wrapped_packet[gv.Wrapped_Packet.CYC.value] = gv.cyc + cfg.chip_delay
                    wrapped_packet[gv.Wrapped_Packet.PACKET.value] = pop_dat

                    self.o_lat_buf[l].append(wrapped_packet)
                    self.o_buf_avail[l] = gv.cyc + packet_num

                    # activate self after latency
                    assert (type(self) is MergeSplit)
                    self.noc.ctrl.insert_merge_split(cfg.chip_delay, self)
                else:
                    assert (type(self) is MergeSplit)
                    self.noc.ctrl.insert_merge_split(self.o_buf_avail[l] - gv.cyc, self)

            if self.o_lat_buf[l]:
                dat_check = self.o_lat_buf[l][0]
                if dat_check[gv.Wrapped_Packet.CYC.value] <= gv.cyc:
                    self.o_data[l] = self.o_lat_buf[l].popleft()[gv.Wrapped_Packet.PACKET.value]

                    assert (type(self.merging_switches[l]) is Switch)
                    self.noc.ctrl.insert_switch(1, self.merging_switches[l])

                else:
                    self.noc.ctrl.insert_merge_split(dat_check[gv.Wrapped_Packet.CYC.value] - gv.cyc, self)
                    self.o_data[l] = None
            else:
                self.o_data[l] = None

        if self.chip_o_buf and gv.cyc >= self.chip_o_buf_avail:
            packet_num = self.chip_o_buf[0][gv.Packet.NUM_PCK.value]

            pop_dat = self.chip_o_buf.popleft()
            self.chip_o_lat_cap -= packet_num

            wrapped_packet = [0 for _ in range(len(gv.Wrapped_Packet))]
            wrapped_packet[gv.Wrapped_Packet.CYC.value] = gv.cyc + cfg.chip_delay
            wrapped_packet[gv.Wrapped_Packet.PACKET.value] = pop_dat

            self.chip_o_lat_buf.append(wrapped_packet)
            self.chip_o_buf_avail = gv.cyc + int(math.ceil(packet_num * cfg.chip_bw[1] / cfg.chip_bw[0]))

            assert (type(self) is MergeSplit)
            self.noc.ctrl.insert_merge_split(cfg.chip_delay, self)
        elif self.chip_o_buf:
            assert (type(self) is MergeSplit)
            self.noc.ctrl.insert_merge_split(self.chip_o_buf_avail - gv.cyc, self)

        if self.chip_o_lat_buf:
            dat_check = self.chip_o_lat_buf[0]
            if dat_check[gv.Wrapped_Packet.CYC.value] <= gv.cyc:
                self.chip_o_data = self.chip_o_lat_buf.popleft()[gv.Wrapped_Packet.PACKET.value]

                assert (type(self.neighbor_block) is MergeSplit)
                self.noc.ctrl.insert_merge_split(1, self.neighbor_block)

            else:
                self.noc.ctrl.insert_merge_split(dat_check[gv.Wrapped_Packet.CYC.value] - gv.cyc, self)
                self.chip_o_data = None
        else:
            self.chip_o_data = None


class NoC:

    def __init__(self):

        # Initialize tiles
        self.tiles = [Tile.Tile(i, self) for i in range(gv.params["tile_num"])]

        # set the number of total instructions
        ################################################
        cyc_file = open(gv.params["result_foldername"] + "/timing.txt", "w")
        tile_inst_total = 0
        core_inst_total = 0
        for i in range(gv.params["tile_num"]):
            self.tiles[i].cyc_file = cyc_file
            for j in range(gv.params["core_num"]):
                self.tiles[i].cores[j].cyc_file = cyc_file
        for tile in self.tiles:
            tile_inst_total += len(tile.inst_list)
            gv.total_inst += len(tile.inst_list) * cfg.image_num
            for core in tile.cores:
                #core.cyc_file = cyc_file
                if len(core.inst_list) > 1:
                    gv.params["active_core_num"] += 1
                core_inst_total += len(core.inst_list)
                gv.total_inst += len(core.inst_list) * cfg.image_num
                for inst in core.inst_list:
                    if inst['opcode'] == 'mvm':
                        core.layer_name = inst['name']


        pf.call_stack["tile_inst"] = tile_inst_total * cfg.image_num
        pf.call_stack["core_inst"] = core_inst_total * cfg.image_num
        ################################################

        # Initialize switches
        self.switches = [[Switch(switch_x, switch_y, self)
                          for switch_y in range(gv.params["max_switch_y"])]
                         for switch_x in range(gv.params["max_switch_x"])]

        # Initialize MS_blocks
        self.merge_split = [[[MergeSplit(chip_x, chip_y, d, self) for d in range(4)]
                             for chip_y in range(gv.params["max_chip_y"])]
                            for chip_x in range(gv.params["max_chip_x"])]

        self.ctrl = Controller.Controller(self)

        # Switch-Tile connections
        for switch_x in range(gv.params["max_switch_x"]):
            for switch_y in range(gv.params["max_switch_y"]):
                chip_x = switch_x // cfg.max_switch_x_in_chip
                chip_y = switch_y // cfg.max_switch_y_in_chip
                chip_ind = chip_x * gv.params["max_chip_y"] + chip_y

                switch_ind = chip_ind * cfg.max_switch_x_in_chip * cfg.max_switch_y_in_chip + \
                             cfg.max_switch_x_in_chip * (switch_y % cfg.max_switch_y_in_chip) + \
                             switch_x % cfg.max_switch_x_in_chip

                tile_ind0 = switch_ind * 4
                tile_ind1 = switch_ind * 4 + 1
                tile_ind2 = switch_ind * 4 + 2
                tile_ind3 = switch_ind * 4 + 3

                if tile_ind0 < gv.params["tile_num"]:
                    self.switches[switch_x][switch_y].tiles[0] = self.tiles[tile_ind0]
                    self.tiles[tile_ind0].switch = self.switches[switch_x][switch_y]
                if tile_ind1 < gv.params["tile_num"]:
                    self.switches[switch_x][switch_y].tiles[1] = self.tiles[tile_ind1]
                    self.tiles[tile_ind1].switch = self.switches[switch_x][switch_y]
                if tile_ind2 < gv.params["tile_num"]:
                    self.switches[switch_x][switch_y].tiles[2] = self.tiles[tile_ind2]
                    self.tiles[tile_ind2].switch = self.switches[switch_x][switch_y]
                if tile_ind3 < gv.params["tile_num"]:
                    self.switches[switch_x][switch_y].tiles[3] = self.tiles[tile_ind3]
                    self.tiles[tile_ind3].switch = self.switches[switch_x][switch_y]

        # Switch-Switch connections
        for switch_x in range(gv.params["max_switch_x"]):
            for switch_y in range(gv.params["max_switch_y"]):
                left_switch_x = switch_x - 1
                right_switch_x = switch_x + 1
                bot_switch_y = switch_y - 1
                top_switch_y = switch_y + 1

                if left_switch_x != -1:
                    self.switches[switch_x][switch_y].switches[0] = \
                        self.switches[left_switch_x][switch_y]
                if right_switch_x != gv.params["max_switch_x"]:
                    self.switches[switch_x][switch_y].switches[3] = \
                        self.switches[right_switch_x][switch_y]
                if bot_switch_y != -1:
                    self.switches[switch_x][switch_y].switches[1] = \
                        self.switches[switch_x][bot_switch_y]
                if top_switch_y != gv.params["max_switch_y"]:
                    self.switches[switch_x][switch_y].switches[2] = \
                        self.switches[switch_x][top_switch_y]

        # Switch-MS_block connections
        for switch_x in range(gv.params["max_switch_x"]):
            for switch_y in range(gv.params["max_switch_y"]):
                for d in range(4):
                    if self.switches[switch_x][switch_y].is_edge[d]:
                        switch = self.switches[switch_x][switch_y]
                        msblock = self.merge_split[switch.chip_x][switch.chip_y][d]

                        msblock.merging_switches.append(switch)
                        switch.switches[d] = msblock

        # MS_block-MS_block connections
        for chip_x in range(gv.params["max_chip_x"]):
            for chip_y in range(gv.params["max_chip_y"]):
                for d in range(4):
                    msblock = self.merge_split[chip_x][chip_y][d]

                    if d == 0:
                        if chip_x - 1 >= 0:
                            msblock.neighbor_block = self.merge_split[chip_x - 1][chip_y][3]
                    elif d == 1:
                        if chip_y - 1 >= 0:
                            msblock.neighbor_block = self.merge_split[chip_x][chip_y - 1][2]
                    elif d == 2:
                        if chip_y + 1 < gv.params["max_chip_y"]:
                            msblock.neighbor_block = self.merge_split[chip_x][chip_y + 1][1]
                    elif d == 3:
                        if chip_x + 1 < gv.params["max_chip_x"]:
                            msblock.neighbor_block = self.merge_split[chip_x + 1][chip_y][0]

    def advance(self):
        #if gv.curr_inst % 5 == 0:
        #    pf.progress(gv.curr_inst, gv.total_inst)

        if cfg.SIM_ver == "Clock":
            # retrieves packets from the adjacent SWs and MSs
            for switch_x in range(gv.params["max_switch_x"]):
                for switch_y in range(gv.params["max_switch_y"]):
                    self.switches[switch_x][switch_y].advance()
            for chip_x in range(gv.params["max_chip_x"]):
                for chip_y in range(gv.params["max_chip_y"]):
                    for d in range(4):
                        self.merge_split[chip_x][chip_y][d].advance()
            
            for tile in self.tiles:
                for core in tile.cores:
                    core.advance()

            # advance the tile & corresponding cores
            for tile in self.tiles:
                tile.advance()

            # calculate the connected packet info for SWs and MSs
            for switch_x in range(gv.params["max_switch_x"]):
                for switch_y in range(gv.params["max_switch_y"]):
                    self.switches[switch_x][switch_y].calc_next()
            for chip_x in range(gv.params["max_chip_x"]):
                for chip_y in range(gv.params["max_chip_y"]):
                    for d in range(4):
                        self.merge_split[chip_x][chip_y][d].calc_next()
        else:
            # retrieves packets from the adjacent SWs and MSs
            comp_index = gv.cyc % cfg.max_latency
            for switch in self.ctrl.switches_to_compute[comp_index]:
                switch.advance()
            for merge_split in self.ctrl.merge_split_to_compute[comp_index]:
                merge_split.advance()

            # advance the tile & corresponding cores
            comp_index = gv.cyc % self.ctrl.compute_length
            # added for same functionality as the clock-driven
            self.ctrl.cores_to_compute[comp_index].sort(key=lambda x: x.num)
            for core in self.ctrl.cores_to_compute[comp_index]:
                core.advance()
            self.ctrl.cores_to_compute[comp_index] = []

            self.ctrl.tiles_to_compute[comp_index].sort(key=lambda x: x.num)
            for tile in self.ctrl.tiles_to_compute[comp_index]:
                tile.advance()
            self.ctrl.tiles_to_compute[comp_index] = []

            # calculate the connected packet info for SWs and MSs
            comp_index = gv.cyc % cfg.max_latency
            for switch in self.ctrl.switches_to_compute[comp_index]:
                switch.calc_next()
            self.ctrl.switches_to_compute[comp_index] = []
            for merge_split in self.ctrl.merge_split_to_compute[comp_index]:
                merge_split.calc_next()
            self.ctrl.merge_split_to_compute[comp_index] = []

        # increment cycle
        gv.cyc += 1
        sys.stdout.flush()
