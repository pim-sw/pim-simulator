import config as cfg
import Profile as pf
import GlobalVars as gv
import Core
import sys
import Operations
import math
from data_convert import *


class XbarSet:
    def __init__(self, core):
        self.core = core
        self.ctrl = None
        self.num_XBAR = cfg.num_matrix

        self.busy = [0 for _ in range(cfg.num_matrix)]
        self.end_cyc = -1

        # Set dedicated destination registers

        # previous image id / previous depth
        self.prev_depth = -1
        self.prev_activated = -1

        self.duration = [[] for _ in range(cfg.image_num)]
        self.act_cycles = [0 for _ in range(cfg.image_num)]
        self.pseudo_act_cycles = [0 for _ in range(cfg.image_num)]

    def activate(self, inst):
        # check if the mvm ended
        if not self.end():
            self.ctrl.insert_core(self.end_cyc - gv.cyc, self.core)
            return False, None

        if self.prev_activated >= 0:
            self.duration[self.core.image_id].append(gv.cyc - self.prev_activated)
        self.prev_activated = gv.cyc

        # activate the crossbar arrays
        activated_xbars = 0
        for i in range(self.num_XBAR):
            self.busy[i] = int(inst['xb_nma'][i][0])
            if self.busy[i]:
                activated_xbars += 1

        # perform mvm * iterations

        # FIXME => count reg read and write here

        pf.call_stack["mvm_naive"] += activated_xbars * cfg.spatial_iter * cfg.temporal_iter
        # if int(inst['precision']) <= 8:
        #    pf.call_stack["mvm_opt_bits"] += activated_xbars * int(inst['precision'])
        #    pf.call_stack["mvm_opt_count"] += activated_xbars
        # else:
        pf.call_stack["mvm_unopt"] += activated_xbars * cfg.spatial_iter * cfg.temporal_iter

        self.end_cyc = gv.cyc + \
                       Operations.latency["mvm"] * cfg.temporal_iter

        self.pseudo_act_cycles[self.core.image_id] += (self.end_cyc - gv.cyc)

        assert ((self.end_cyc - gv.cyc) >= 0)
        self.act_cycles[self.core.image_id] += (self.end_cyc - gv.cyc)

        pf.call_stack["sna"] += cfg.xbar_out_size * (cfg.temporal_iter - 1)

        read_data_bits = cfg.xbar_in_size * activated_xbars
        write_data_bits = cfg.xbar_out_size * activated_xbars * cfg.intermediate_bits

        # num accesses: set to 1 for input read
        # iteration: input precision
        num_accesses_read = self.core.reg.calc_accesses(self.core.reg.input_offset, read_data_bits)
        self.core.reg.read(self.core.reg.input_offset, num_accesses_read, cfg.input_prec)
        # num accesses: data_bits / reg buswidth (the register bus width is 16bit in OR)
        # iteration: iterate over temporal iteration
        num_accesses_write = self.core.reg.calc_accesses(self.core.reg.output_offset, write_data_bits)
        self.core.reg.write(self.core.reg.output_offset, 0, num_accesses_write, cfg.temporal_iter)

        # activate right away (check for pipeline)
        self.ctrl.insert_core(1, self.core)

        return True, self.end_cyc - gv.cyc

    def end(self):
        return (gv.cyc >= self.end_cyc and gv.cyc >= self.core.reg.end_cyc)

    def check_dependency(self, operands):
        if self.end():
            return False
        for operand in operands:
            for i, is_busy in enumerate(self.busy):
                if is_busy == 1 and operand in self.core.reg.dst_reg_set[i]:
                    return True
        return False


class ALU:
    def __init__(self, core):
        self.core = core
        self.ctrl = None
        self.num_ALU = cfg.num_ALU
        self.end_cyc = -1

        self.act_cycles = [0 for _ in range(cfg.image_num)]

    def end(self):
        return (gv.cyc >= self.end_cyc and gv.cyc >= self.core.reg.end_cyc)

    def activate(self, inst, mvmu):
        if mvmu.check_dependency([inst['d1'], inst['r1'], inst['r2']]):
            self.ctrl.insert_core(mvmu.end_cyc - gv.cyc, self.core)
            return False, None

        if not self.end():
            self.ctrl.insert_core(self.end_cyc - gv.cyc, self.core)
            return False, None

        vec = inst['vec']

        alu_latency = Operations.latency[inst['opcode']] * int(math.ceil(vec / self.num_ALU))
        self.ctrl.insert_core(alu_latency, self.core)

        self.end_cyc = gv.cyc + alu_latency
        self.act_cycles[self.core.image_id] += alu_latency

        if not inst['intermediate']:
            data_bits = vec * cfg.final_bits
        else:
            data_bits = vec * cfg.intermediate_bits

        # if aluop is add / sub / sna => simply use sna module
        if inst['aluop'] == 'add' or inst['aluop'] == 'sub' \
                or inst['aluop'] == 'sna' or inst['aluop'] == 'max':
            pf.call_stack["sna"] += vec
            # read twice / write once
            num_accesses_read = self.core.reg.calc_accesses(inst['r1'], data_bits)
            self.core.reg.read(inst['r1'], num_accesses_read)
            num_accesses_read = self.core.reg.calc_accesses(inst['r2'], data_bits)
            self.core.reg.read(inst['r2'], num_accesses_read)
            num_accesses_write = self.core.reg.calc_accesses(inst['d1'], data_bits)
            self.core.reg.write(inst['d1'], 0, num_accesses_write)
        # if not add / sub / sna => use VFU
        elif inst['opcode'] == 'alu':
            pf.call_stack["alu"] += vec
            # read twice / write once
            if inst['aluop'] == 'mul' or inst['aluop'] == 'div':
                num_accesses_read = self.core.reg.calc_accesses(inst['r1'], data_bits)
                self.core.reg.read(inst['r1'], num_accesses_read)
                num_accesses_read = self.core.reg.calc_accesses(inst['r2'], data_bits)
                self.core.reg.read(inst['r2'], num_accesses_read)
                num_accesses_write = self.core.reg.calc_accesses(inst['d1'], data_bits)
                self.core.reg.write(inst['d1'], 0, num_accesses_write)
            else:
                num_accesses_read = self.core.reg.calc_accesses(inst['r1'], data_bits)
                self.core.reg.read(inst['r1'], num_accesses_read)
                num_accesses_write = self.core.reg.calc_accesses(inst['d1'], data_bits)
                self.core.reg.write(inst['d1'], 0, num_accesses_write)
        # if an alui => use VFU
        else:
            pf.call_stack["alui"] += vec
            # read once / write once
            num_accesses_read = self.core.reg.calc_accesses(inst['r1'], data_bits)
            self.core.reg.read(inst['r1'], num_accesses_read)
            num_accesses_write = self.core.reg.calc_accesses(inst['d1'], data_bits)
            self.core.reg.write(inst['d1'], 0, num_accesses_write)

        return True, alu_latency


class REG:
    def __init__(self, core):
        self.core = core
        self.ctrl = None

        self.input_offset = 0
        self.src_reg = range(self.input_offset, self.input_offset +
                             cfg.num_matrix * cfg.xbar_in_size)
        self.output_offset = cfg.num_matrix * cfg.xbar_in_size
        self.dst_reg = range(self.output_offset, self.output_offset +
                             cfg.num_matrix * cfg.xbar_out_size)
        self.dst_reg_set = [range(self.output_offset + i * cfg.xbar_out_size,
                                  self.output_offset + (i + 1) * cfg.xbar_out_size)
                            for i in range(cfg.num_matrix)]
        self.data = {}
        self.end_cyc = -1

    def end(self):
        return (gv.cyc >= self.end_cyc and gv.cyc >= self.core.alu.end_cyc)

    # access type 0: normal access
    # access type 1: read input reg

    def calc_accesses(self, addr, data_bits):
        if addr in self.src_reg:
            num_accesses = int(math.ceil(data_bits / cfg.IR_width))
        elif addr in self.dst_reg:
            num_accesses = int(math.ceil(data_bits / cfg.OR_width))
        else:
            num_accesses = int(math.ceil(data_bits / cfg.DR_width))
        return num_accesses

    def read(self, addr, num_accesses, iterations=1):
        if addr in self.src_reg:
            # scale as the input register requires less accesses
            pf.call_stack["in_reg_read"] += num_accesses * iterations
        elif addr in self.dst_reg:
            pf.call_stack["out_reg_read"] += num_accesses * iterations
        else:
            pf.call_stack["data_reg_read"] += num_accesses * iterations

        assert (not ((addr in self.src_reg) and (addr in self.dst_reg)))
        return self.data[addr] if addr in self.data else 0

    # access type 0: normal access
    # access type 2: write output reg
    def write(self, addr, dat, num_accesses, iterations=1):
        if addr in self.src_reg:
            pf.call_stack["in_reg_write"] += num_accesses * iterations
        elif addr in self.dst_reg:
            pf.call_stack["out_reg_write"] += num_accesses * iterations
        else:
            pf.call_stack["data_reg_write"] += num_accesses * iterations

        assert (not ((addr in self.src_reg) and (addr in self.dst_reg)))
        self.data[addr] = dat

    def cp(self, inst, mvmu):
        if mvmu.check_dependency([inst['d1'], inst['r1']]):
            self.ctrl.insert_core(mvmu.end_cyc - gv.cyc, self.core)
            return False, None

        if not self.end():
            self.ctrl.insert_core(self.end_cyc - gv.cyc, self.core)
            return False, None

        vec = inst['vec']

        if not inst['intermediate']:
            data_bits = vec * cfg.final_bits
        else:
            data_bits = vec * cfg.intermediate_bits

        ##
        num_accesses_read = self.calc_accesses(inst['r1'], data_bits)
        data_temp = self.read(inst['r1'], num_accesses_read)

        ##
        num_accesses_write = self.calc_accesses(inst['d1'], data_bits)
        self.write(inst['d1'], data_temp, num_accesses_write)

        ##
        cp_latency = max(Operations.latency[inst['opcode']] * max(num_accesses_read, num_accesses_write), 1)
        self.ctrl.insert_core(cp_latency, self.core)
        self.end_cyc = gv.cyc + cp_latency

        return True, cp_latency

    def set(self, inst, mvmu):
        if mvmu.check_dependency([inst['d1']]):
            self.ctrl.insert_core(mvmu.end_cyc - gv.cyc, self.core)
            return False, None

        if not self.end():
            self.ctrl.insert_core(self.end_cyc - gv.cyc, self.core)
            return False, None

        imm = inst['imm_orig']
        vec = inst['vec']

        # FIXME: change num bits dynamically
        if not inst['intermediate']:
            data_bits = vec * cfg.final_bits
        else:
            data_bits = vec * cfg.intermediate_bits

        ##
        num_accesses_write = self.calc_accesses(inst['d1'], data_bits)
        # self.write(inst['d1'], bin2int(imm, cfg.addr_width), num_accesses_write)
        self.write(inst['d1'], imm, num_accesses_write)

        set_latency = max(Operations.latency[inst['opcode']] * num_accesses_write, 1)
        self.ctrl.insert_core(set_latency, self.core)
        self.end_cyc = gv.cyc + set_latency

        return True, set_latency
