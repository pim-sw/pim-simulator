import os
import time

# set the target workloads
validate_workloads = ["C_Func-to-IR_APIM_ISSAC", "mlp_l5", "resnet18", "vgg16_small"]

log_file = open("log.txt", "w")

for workload in validate_workloads:
    #elapsed_time = Main.testrun(workload, "Clock")
    #elapsed_time = Main.testrun(workload, "Event")
    log_file.write("Executing %s in an unoptimized mode\n" %(workload))
    print("Executing %s in an unoptimized mode" %(workload))
    os.system("python Main.py %s %s" %(workload, "Clock"))

    # open baseline result output
    clock_driven_elapsed_file = "result/%s_%s/elapsed.txt" %(workload, "Clock")
    f_clock = open(clock_driven_elapsed_file)
    elapsed_time_clock = float(f_clock.readlines()[0])

    print("Elapsed %s (s)" %(elapsed_time_clock))
    print()
    log_file.write("Elapsed %s (s)\n" %(elapsed_time_clock))
    log_file.write("\n")

    print("Executing %s in an optimized mode" %(workload))
    log_file.write("Executing %s in an optimized mode\n" %(workload))
    os.system("python Main.py %s %s" %(workload, "Event"))

    event_driven_elapsed_file = "result/%s_%s/elapsed.txt" %(workload, "Event")
    f_event = open(event_driven_elapsed_file)
    elapsed_time_event = float(f_event.readlines()[0])

    print("Elapsed %s (s)" %(elapsed_time_event))
    print()
    log_file.write("Elapsed %s (s)\n" %(elapsed_time_event))
    log_file.write("\n")

    # open baseline result output
    clock_driven_result_file = "result/%s_%s/func.txt" %(workload, "Clock")
    f_clock = open(clock_driven_result_file)
    event_driven_result_file = "result/%s_%s/func.txt" %(workload, "Event")
    f_event = open(event_driven_result_file)

    if (f_clock.readlines() == f_event.readlines()):
        log_file.write("\nPassed Functionality Check (%.2f%% speedup)\n\n" %((elapsed_time_clock - elapsed_time_event) / elapsed_time_event * 100))
        print("\nPassed Functionality Check (%.2f%% speedup)\n" %((elapsed_time_clock - elapsed_time_event) / elapsed_time_event * 100))
    else:
        log_file.write("\nFailed Functionality Check\n\n")
        print("\nFailed Functionality Check\n")
log_file.close()
