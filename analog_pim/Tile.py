import os
import numpy as np
import GlobalVars as gv
import Profile as pf
import NoC
import Core
import Memory
import Receive_Buffer
import sys

import config as cfg
import math


def get_inst(inst):
    if inst['opcode'] == 'send':
        inst_str = "send(mem_addr=%d, src_tile=%d, dst_tile=%d, send_width=%d, vec=%d, intermediate=%s)" \
                   % (inst['mem_addr'], inst['vtile_id'], inst['r2'], inst['r1'], inst['vec'],
                      str(inst['intermediate']))

    elif inst['opcode'] == 'receive':
        inst_str = "receive(mem_addr=%d, vtile_id=%d, receive_width=%d, vec=%d, counter=%d, intermediate=%s)" \
                   % (inst['mem_addr'], inst['vtile_id'], inst['r1'], inst['vec'], inst['r2'],
                      str(inst['intermediate']))

    elif inst['opcode'] == 'halt':
        inst_str = "halt()"

    else:
        assert 0

    return inst_str


class Tile:
    def __init__(self, num, noc):
        self.num = num
        self.noc = noc

        self.ctrl = None
        self.switch = None

        #
        self.end_cyc = -1
        self.send_cyc = -1

        # self.x, self.y = gv.ind_to_coord(num)
        self.inst_list = self.load_inst()
        self.pc = 0
        self.is_halted = False
        self.halted_core_num = 0

        # self.fifo_temp = [0 for _ in range(gv.fifo_num)] # (# of packets) in the fifo
        self.fifo = Receive_Buffer.Receive_Buffer(self)

        self.memory = Memory.Memory(self)
        self.cores = [Core.Core(i, self, self.memory)
                      for i in range(gv.params["core_num"])]

        self.o_data = None
        # self.o_data_hold = None # For emulating memory access latency
        self.o_data_hold = None  # For emulating memory access latency
        self.send_available = False
        self.send_inst_buffer = []
        self.image_id = 0

        self.cyc_file = None
        self.time_file = None
        # if self.num < 30:
        # if self.num < 20 or (70 <= self.num and self.num < 90):

    def load_inst(self):
        filename = "%s/tile%d/tile_imem.npy" % (gv.params["foldername"], self.num)
        inst_list = np.load(filename, allow_pickle=True)

        send_list = []
        recv_list = []
        recv_inter_list = []
        halt_list = []

        for inst in inst_list:
            if inst['opcode'] == 'send':
                send_list.append(inst)
            elif inst['opcode'] == 'receive':
                if not inst['intermediate']:
                    recv_list.append(inst)
                else:
                    recv_inter_list.append(inst)
            else:
                halt_list.append(inst)

        inst_list = send_list + recv_list + recv_inter_list + halt_list

        return inst_list

    def advance(self):
        if self.is_halted:
            return

        # Pop the Send Buffer is possible
        # if send buffer is not empty and there is not pending data
        if self.send_inst_buffer and not self.send_available:
            # decode inst
            inst = self.send_inst_buffer[0][0]
            image_id = self.send_inst_buffer[0][1]
            mem_addr = inst['mem_addr']
            send_width = inst['r1']
            vec = inst['vec']
            if not inst['intermediate']:
                data_bits = vec * send_width * cfg.final_bits
            else:
                data_bits = vec * send_width * cfg.intermediate_bits
            packet_num = int(math.ceil((data_bits / cfg.packet_size)))

            # retrieve from the memory (dummy)
            if self.memory.exist(mem_addr, image_id, True):
                left_cycles = self.memory.left_cycles()
                if left_cycles <= 0:
                    accessed, data, access_latency = self.memory.access(gv.cyc, mem_addr, data_bits, image_id)

                    vtile_id = inst['vtile_id']
                    target_tile_num = inst['r2']

                    self.o_data_hold = [-1 for _ in range(len(gv.Packet))]
                    self.o_data_hold[gv.Packet.SRC_ID.value] = vtile_id
                    self.o_data_hold[gv.Packet.DST_ID.value] = target_tile_num
                    self.o_data_hold[gv.Packet.NUM_PCK.value] = packet_num
                    self.o_data_hold[gv.Packet.GEN_CYC.value] = gv.cyc
                    self.o_data_hold[gv.Packet.INTERMEDIATE.value] = inst['intermediate']

                    self.ctrl.insert_tile(access_latency, self)

                    self.send_cyc = gv.cyc + access_latency
                    self.send_inst_buffer.pop(0)

                    assert (not self.send_available)
                    self.send_available = True

                    assert (vtile_id == self.num)
                    assert (packet_num <= cfg.max_latency)

                # else simply wait
                else:
                    self.ctrl.insert_tile(left_cycles, self)
        # if self.send_inst_buffer:
        # self.ctrl.insert_tile(1, self)

        # if send is available
        if self.send_available and gv.cyc >= self.send_cyc:
            assert (self.o_data is None)

            vtile_id = self.o_data_hold[gv.Packet.SRC_ID.value]
            packet_num = self.o_data_hold[gv.Packet.NUM_PCK.value]
            target_tile_num = self.o_data_hold[gv.Packet.DST_ID.value]
            is_intermediate = self.o_data_hold[gv.Packet.INTERMEDIATE.value]

            src_switch = self.noc.tiles[vtile_id].switch
            dst_switch = self.noc.tiles[target_tile_num].switch

            hops = abs(src_switch.switch_x - dst_switch.switch_x) + \
                   abs(src_switch.switch_y - dst_switch.switch_y) + 1

            src_chip_x = src_switch.switch_x // cfg.max_switch_x_in_chip
            src_chip_y = src_switch.switch_y // cfg.max_switch_y_in_chip
            dst_chip_x = dst_switch.switch_x // cfg.max_switch_x_in_chip
            dst_chip_y = dst_switch.switch_y // cfg.max_switch_y_in_chip
            chip_hops = int((src_chip_x != dst_chip_x) or (src_chip_y != dst_chip_y))
            # chip_hops = abs(src_chip_x - dst_chip_x) + \
            #            abs(src_chip_y - dst_chip_y)

            self.ctrl.insert_tile(1, self)

            # Lets add sth here
            pf.call_stack["router_activate"] += packet_num * hops
            pf.call_stack["ht_activate"] += packet_num * chip_hops
            if not is_intermediate:
                pf.call_stack["router_final_activate"] += packet_num

            self.o_data = self.o_data_hold
            self.ctrl.insert_switch(1, self.switch)

            self.send_available = False
            self.o_data_hold = None

        inst = self.inst_list[self.pc]
        pc_changed = False
        if inst['opcode'] == 'send':
            # memory access done
            send_width = inst['r1']
            vec = inst['vec']

            if not inst['intermediate']:
                data_bits = vec * send_width * cfg.final_bits
            else:
                data_bits = vec * send_width * cfg.intermediate_bits

            packet_num = int(math.ceil((data_bits / cfg.packet_size)))

            # first simply check if the data to send exists in the queue
            if gv.cyc >= self.end_cyc:
                left_cycles = self.memory.left_cycles()
                if left_cycles <= 0:
                    mem_addr = inst['mem_addr']
                    # the data is not currently available
                    if not self.memory.exist(mem_addr, self.image_id) and self.num != 0:
                        # send inst buffer to keep the pending send insts.
                        self.send_inst_buffer.append((inst, self.image_id))
                        self.memory.pending_mem_tile.append(mem_addr)
                    else:
                        accessed, data, access_latency = self.memory.access \
                            (gv.cyc, mem_addr, data_bits, self.image_id)
                        access_latency = access_latency if self.num != 0 else 1

                        vtile_id = inst['vtile_id']
                        assert (vtile_id == self.num)
                        target_tile_num = inst['r2']

                        # send_width: scalar #, 16: bit-precision, 32: packet size
                        # should be changed according to the bit width
                        assert (packet_num <= cfg.max_latency)
                        self.o_data_hold = [-1 for _ in range(len(gv.Packet))]
                        self.o_data_hold[gv.Packet.SRC_ID.value] = vtile_id
                        self.o_data_hold[gv.Packet.DST_ID.value] = target_tile_num
                        self.o_data_hold[gv.Packet.NUM_PCK.value] = packet_num
                        self.o_data_hold[gv.Packet.GEN_CYC.value] = gv.cyc

                        self.ctrl.insert_tile(access_latency, self)

                        self.send_cyc = gv.cyc + access_latency
                        assert (not self.send_available)
                        self.send_available = True
                    # proceed the simulation!
                    # without waiting
                    pc_changed = True
                    self.ctrl.insert_tile(1, self)
                    self.end_cyc = gv.cyc + 1
                else:
                    self.ctrl.insert_tile(left_cycles, self)
            else:
                self.ctrl.insert_tile(self.end_cyc - gv.cyc, self)

        elif inst['opcode'] == 'receive':
            if gv.cyc >= self.end_cyc:
                left_cycles = self.memory.left_cycles()
                if left_cycles <= 0:
                    # if self.memory.mem_wait == 0:
                    mem_addr = inst['mem_addr']
                    vtile_id = inst['vtile_id']
                    receive_width = inst['r1']
                    counter = inst['r2']
                    vec = inst['vec']

                    if not inst['intermediate']:
                        data_bits = vec * receive_width * cfg.final_bits
                    else:
                        data_bits = vec * receive_width * cfg.intermediate_bits

                    packet_num = int(math.ceil((data_bits / cfg.packet_size)))
                    assert (packet_num <= cfg.max_latency)

                    if self.fifo.exist(vtile_id, packet_num):

                        if self.num != 1:
                            allocated, allocate_latency = self.memory.allocate \
                                (gv.cyc, mem_addr, data_bits, counter, self.image_id)
                        else:
                            allocated = True
                            allocate_latency = 1

                        if allocated:
                            self.fifo.access(vtile_id, packet_num)

                            pc_changed = True
                            self.ctrl.insert_tile(allocate_latency, self)
                            self.end_cyc = gv.cyc + allocate_latency
                        else:
                            self.memory.receive_pending_mem_tile = mem_addr
                    else:
                        pass
                else:
                    self.ctrl.insert_tile(left_cycles, self)
            else:
                self.ctrl.insert_tile(self.end_cyc - gv.cyc, self)

        elif inst['opcode'] == 'halt':
            # if(not self.send_available and not self.send_inst_buffer):

            # if (self.send_available or self.send_inst_buffer) \
            #    or self.image_id != cfg.image_num - 1:

            # halt
            if self.image_id + 1 == cfg.image_num and not self.send_available and not self.send_inst_buffer:
                if self.halted_core_num == gv.params["core_num"]:
                    self.is_halted = True
                    gv.halted_tile_num += 1
                    gv.curr_inst += 1
                    self.image_id += 1
            # next image
            elif self.image_id + 1 != cfg.image_num:
                pc_changed = True
                self.image_id += 1
                self.pc = -1
                self.ctrl.insert_tile(1, self)

            if self.num == 1:
                pf.end_cyc.append(gv.cyc)
            pf.end_cyc_per_tile[self.num].append(gv.cyc)

        else:
            raise NotImplementedError

        if pc_changed:
            if self.cyc_file is not None and "source_code" in inst:
                self.cyc_file.write("%s ISSUED: %d\n" % (inst["source_code"].strip(), gv.cyc))
            '''if pc_changed or self.image_id == cfg.image_num: if self.image_id >= 2: if self.num == 25 \ 
            or self.num 
            == 26: print("    [{}] [{}]".format(self.image_id, self.num), "inst:", inst['opcode'], "/ pc:", self.pc, 
            "/ cyc:", gv.cyc) '''
            #if self.cyc_file is not None and "source_code" in inst:
            #    print("%s ISSUED: %d" % (inst["source_code"].strip(), gv.cyc))
            self.pc += 1
            gv.curr_inst += 1

