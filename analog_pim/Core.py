import os
import GlobalVars as gv
import numpy as np
import Operations
import config as cfg
from data_convert import *
import Profile as pf
import sys
from Core_module import *


def get_inst(inst):
    if inst['opcode'] == 'st':
        inst_str = "store(d1=%d, r1=%d, counter=%d, store_width=%d, vec=%d, dst=[" \
                   % (inst['d1'], inst['r1'], inst['r2'], inst['imm'], inst['vec'])
        for dst in inst['dst']:
            inst_str += str(dst) + ", "
        inst_str += "], intermediate=%s)" % inst['intermediate']

    elif inst['opcode'] == 'ld':
        inst_str = "load(d1=%d, r1=%d, load_width=%d, vec=%d, intermediate=%s)" \
                   % (inst['d1'], inst['r1'], inst['imm'], inst['vec'], str(inst['intermediate']))

    elif inst['opcode'] == 'gd':
        inst_str = "guard(r1=%d, guard_width=%d, vec=%d, intermediate=%s)" \
                   % (inst['r1'], inst['imm'], inst['vec'], str(inst['intermediate']))

    elif inst['opcode'] == 'alu' or inst['opcode'] == 'alui':
        inst_str = "alu(%s, d1=%d, r1=%d, r2=%d, intermediate=%s, vec=%d)" \
                   % (inst['aluop'], inst['d1'], inst['r1'], inst['r2'], str(inst['intermediate']), inst['vec'])

    elif inst['opcode'] == 'mvm':
        inst_str = "mvm(xb_nma=%s, name=%s, precision=%d, depth=%d, stacks=%d, slide_id=%d, isLast=%s" \
                   % (inst['xb_nma'], inst['name'], inst['precision'], inst['depth'], inst['stacks'], inst['slide_id'],
                      str(inst['isLast']))

    elif inst['opcode'] == 'cp':
        inst_str = "copy(d1=%d, r1=%d, vec=%d, intermediate=%s)" \
                   % (inst['d1'], inst['r1'], inst['vec'], str(inst['intermediate']))

    elif inst['opcode'] == 'set':
        inst_str = "set(d1=%d, imm=%s, vec=%d, intermediate=%s" \
                   % (inst['d1'], inst['imm_orig'], inst['vec'], str(inst['intermediate']))

    elif inst['opcode'] == 'hlt':
        inst_str = "hlt()"

    elif inst['opcode'] == 'xbar_ld':
        inst_str = "xbar_ld()"

    elif inst['opcode'] == 'xbar_st':
        inst_str = "xbar_st()"

    else:
        assert 0

    return inst_str


class Core:
    def __init__(self, num, tile, memory):
        self.num = num
        self.pending = [False, 0]
        self.end_cyc = 0

        # index
        self.tile = tile
        self.memory = memory
        self.ctrl = None

        # eDRAM hop
        core_index = self.num
        if cfg.num_ima == 24:
            core_index = core_index // 2

        if 0 <= core_index < 2:
            self.eDRAM_hop = 1
        elif 2 <= core_index < 6:
            self.eDRAM_hop = 2
        elif 6 <= core_index < 10:
            self.eDRAM_hop = 3
        else:
            self.eDRAM_hop = 4

        # initialize required modules
        self.mvmu = XbarSet(self)
        self.alu = ALU(self)
        self.reg = REG(self)

        # Simulation states
        self.inst_list = self.load_inst()
        self.pc = 0
        self.is_halted = False
        self.image_id = 0

        self.mvms = 0
        for inst in self.inst_list:
            if inst['opcode'] == 'mvm':
                self.mvms += 1

        self.layer_name = ''

        self.cyc_file = None
        self.time_file = None

    def load_inst(self):
        filename = "%s/tile%d/core_imem%d.npy" \
                   % (gv.params["foldername"], self.tile.num, self.num)
        inst_list = np.load(filename, allow_pickle=True)

        return inst_list

    # For cyc profile

    def advance(self):

        if self.is_halted == True:
            return

        inst = self.inst_list[self.pc]

        pc_changed = False

        if cfg.SIM_ver == "Clock":
            if gv.cyc < self.end_cyc:
                return

        if cfg.SIM_ver == "Event" or not self.pending[0]:
            if inst['opcode'] == 'st':
                is_dependent = self.mvmu.check_dependency([inst['d1'], inst['r1']])
                if is_dependent:
                    self.ctrl.insert_core(self.mvmu.end_cyc - gv.cyc, self)
                    return

                left_cycles = self.tile.memory.left_cycles()
                if left_cycles <= 0:
                    num_accesses_read = self.reg.calc_accesses(inst['d1'], cfg.addr_width)
                    mem_addr = self.reg.read(inst['d1'], num_accesses_read)
                    counter = inst['r2']
                    store_width = inst['imm']
                    vec = inst['vec']

                    if not inst['intermediate']:
                        data_bits = vec * store_width * cfg.final_bits
                    else:
                        data_bits = vec * store_width * cfg.intermediate_bits

                    num_accesses_read = self.reg.calc_accesses(inst['r1'], data_bits)
                    data = self.reg.read(inst['r1'], num_accesses_read)

                    allocated, allocate_latency = self.tile.memory.allocate \
                        (gv.cyc, mem_addr, data_bits, counter, self.image_id, data, self.eDRAM_hop)

                    if allocated:
                        pc_changed = True
                        self.end_cyc = gv.cyc + allocate_latency
                        self.ctrl.insert_core(allocate_latency, self)
                    else:
                        self.pending = [True, 0]
                        self.memory.store_pending_mem_core[self.num] = mem_addr

                else:
                    self.ctrl.insert_core(left_cycles, self)

            elif inst['opcode'] == 'ld':
                is_dependent = self.mvmu.check_dependency([inst['d1'], inst['r1']])
                if is_dependent:
                    self.ctrl.insert_core(self.mvmu.end_cyc - gv.cyc, self)
                    return

                left_cycles = self.tile.memory.left_cycles()
                if left_cycles <= 0:
                    num_accesses_read = self.reg.calc_accesses(inst['r1'], cfg.addr_width)
                    mem_addr = self.reg.read(inst['r1'], num_accesses_read, iterations = 1)
                    load_width = inst['imm']
                    vec = inst['vec']

                    if not inst['intermediate']:
                        data_bits = vec * load_width * cfg.final_bits
                    else:
                        data_bits = vec * load_width * cfg.intermediate_bits

                    accessed = self.tile.memory.exist(mem_addr, self.image_id)
                    if accessed:
                        accessed, data, access_latency = self.tile.memory.access \
                            (gv.cyc, mem_addr, data_bits, self.image_id, self.num, self.eDRAM_hop)

                        num_accesses_write = self.reg.calc_accesses(inst['d1'], data_bits)
                        self.reg.write(inst['d1'], data, num_accesses_write)

                        pc_changed = True
                        self.end_cyc = gv.cyc + access_latency
                        self.ctrl.insert_core(access_latency, self)
                    else:
                        self.pending = [True, 0]
                        self.memory.pending_mem_core[self.num] = mem_addr

                else:
                    self.ctrl.insert_core(left_cycles, self)

            elif inst['opcode'] == 'alu' or inst['opcode'] == 'alui':
                activated, alu_latency = self.alu.activate(inst, self.mvmu)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'mvm':
                activated, mvm_latency = self.mvmu.activate(inst)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'xbar_ld':
                activated, mvm_latency = self.mvmu.activate(inst)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'xbar_st':
                activated, mvm_latency = self.mvmu.activate(inst)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'cp':
                activated, cp_latency = self.reg.cp(inst, self.mvmu)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'set':
                activated, set_latency = self.reg.set(inst, self.mvmu)
                if activated:
                    pc_changed = True

            elif inst['opcode'] == 'hlt':
                self.image_id += 1
                # halt
                if self.image_id == cfg.image_num:
                    self.is_halted = True
                    self.tile.halted_core_num += 1
                    gv.halted_core_num += 1
                    gv.curr_inst += 1
                    self.ctrl.insert_tile(1, self.tile)
                        #self.cyc_file.close()
                # next image
                else:
                    pc_changed = True
                    self.pc = -1
                    self.ctrl.insert_core(1, self)
                    #if self.cyc_file is not None:
                    #    self.cyc_file.write("%d [%12s %12s cyc] " % (self.num, gv.cyc, gv.cyc + 1) + get_inst(inst) + "\n")
                    #    self.cyc_file.write(
                    #        "\n\n=========================== [Image %s] ===========================\n\n" % self.image_id)

                pf.end_cyc_per_core[self.tile.num][self.num].append(gv.cyc)

            else:
                raise NotImplementedError

        if pc_changed:
            if self.cyc_file is not None and "source_code" in inst:
                self.cyc_file.write("%s ISSUED: %d\n" % (inst["source_code"].strip(), gv.cyc))
            '''if pc_changed or self.image_id == cfg.image_num: if self.image_id >= 2: if self.tile.num == 25 and 
            self.num == 7 or self.tile.num == 25 and self.num == 0 or self.tile.num == 26 and self.num == 0: 
            print("[{}] [{} / {}]".format(self.image_id, self.tile.num, self.num), "inst:", inst['opcode'], "/ pc:", 
            self.pc, "/ cyc:", gv.cyc) '''
            #if self.cyc_file is not None and "source_code" in inst:
            #    print("%s ISSUED: %d" % (inst["source_code"].strip(), gv.cyc))
            self.pc += 1
            gv.curr_inst += 1
