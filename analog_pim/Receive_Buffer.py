import numpy as np
import GlobalVars as gv
import Profile as pf
import config as cfg
import Tile
import sys


class Receive_Buffer:
    def __init__(self, tile):
        self.tile = tile

        # map virtual address to the physical address
        self.virtual_to_physical = {}

        # do not need data
        # physical memory (number of packets (0), virtual tile (1))
        self.physical_mem = {}

        # the number of fifos
        self.max_fifo_num = 0
        # the number of fifo entries
        self.max_fifo_size = 0

    def allocate(self, virtual_tile, packet_num, dat=None):

        pf.call_stack["receive_buffer_write"] += packet_num

        self.tile.ctrl.insert_tile(0, self.tile)
        # if virtual tile has been allocated
        if virtual_tile in self.virtual_to_physical:
            # accumulate!
            self.physical_mem[self.virtual_to_physical[virtual_tile]][0] += packet_num
            fifo_size = self.physical_mem[self.virtual_to_physical[virtual_tile]][0]
            if fifo_size > self.max_fifo_size:
                self.max_fifo_size = fifo_size
            return

        # search for the empty space & allocate to the physical memory
        target_fifo = 0
        while True:
            # if the physical address has not been allocated or freed
            if target_fifo not in self.physical_mem:
                # then add a new entry to the dictionary
                self.virtual_to_physical[virtual_tile] = target_fifo
                # allocate the new entry to the physical mem
                self.physical_mem[target_fifo] = [packet_num, virtual_tile]
                break
            target_fifo += 1

        # renew the maximum num
        if target_fifo > self.max_fifo_num:
            self.max_fifo_num = target_fifo

    def exist(self, virtual_tile, packet_num):

        return ((virtual_tile in self.virtual_to_physical)
                and (self.physical_mem[self.virtual_to_physical[virtual_tile]][0] >= packet_num))

    def access(self, virtual_tile, packet_num):

        # if the virtual address exist & 
        # the physical memory's counter is larger than count
        assert (self.exist(virtual_tile, packet_num))

        physical_addr = self.virtual_to_physical[virtual_tile]
        self.physical_mem[physical_addr][0] -= packet_num

        pf.call_stack["receive_buffer_read"] += packet_num

        if self.physical_mem[physical_addr][0] == 0:
            self.virtual_to_physical.pop(self.physical_mem[physical_addr][1])
            assert (self.physical_mem[physical_addr][1] not in self.virtual_to_physical)
            self.physical_mem.pop(physical_addr)
            assert (physical_addr not in self.physical_mem)

        return True
