import sys

end_cyc = []
end_cyc_per_core = None
end_cyc_per_tile = None


def progress(count, total):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 3)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('progress: [%s] %.3f%s\r' % (bar, percents, '%'))
    sys.stdout.flush()


passed_data = None  # key:((src_x, src_y), (dst_x, dst_y)), value:data_bits
busiest_link = None
busiest_link_data = -1
cyc = None

call_stack = {
    # read / write entries (bit)
    "data_reg_read": 0,
    "data_reg_write": 0,
    "in_reg_read": 0,
    "in_reg_write": 0,
    "out_reg_read": 0,
    "out_reg_write": 0,

    # read / write entries (rows)
    "edram_read": 0,
    "edram_write": 0,
    "edram_bus_hop": 0,
    "attribute_only": 0,

    # tile fifo push / pop (# packets)
    "receive_buffer_read": 0,
    "receive_buffer_write": 0,

    # switch packet send / receive (# packets)
    "router_activate": 0,
    "router_final_activate": 0,

    # merge split packet send / receive (# packets)
    "ht_activate": 0,
    "ht_final_activate": 0,

    # activated crossbars (# crossbars)
    "mvm_naive": 0,
    # the number of bits
    "mvm_opt_bits": 0,
    "mvm_opt_count": 0,
    "mvm_unopt": 0,

    # activated alus (# operations)
    "alu": 0,
    "alui": 0,

    # activated shift and add
    "sna": 0,
    "core_inst": 0,
    "tile_inst": 0,
}
