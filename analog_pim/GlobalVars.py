from enum import IntEnum

max_buffer_size = None


class Packet(IntEnum):
    SRC_ID = 0
    DST_ID = 1
    NUM_PCK = 2
    WAIT_CYC = 3
    GEN_CYC = 4
    INTERMEDIATE = 5


class Wrapped_Packet(IntEnum):
    CYC = 0
    PACKET = 1


class Direction(IntEnum):
    LEFT = 0
    BOT = 1
    TOP = 2
    RIGHT = 3


# in nanoseconds
clock_period = 1

debug_enabled = True
last_debug_cyc = 0
cyc = 0

params = {}
NoC = None
halted_tile_num = 0
halted_core_num = 0
fifo_num = 1000

total_inst = 0
curr_inst = 0

end_cyc = []


def ind_to_coord(ind):
    return ind % params["tile_x"], ind // params["tile_x"]
