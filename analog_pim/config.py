import math

# FIXME PARAMS

# One of "Clock" or "Event"
SIM_ver = None
# One of "Analog", "Digital" (To be added), "PNM" (To be added)
MVMU_ver = "Analog"

image_num = 1

# Operand precision (fixed point allowed only): num_bits = int_bits + frac_bits
# data precision
final_bits = 5
intermediate_bits = 16
input_prec = 5
weight_prec = 5
data_width = final_bits

clock_freq = 1e9
clock_cyc = 1 / clock_freq  # clock period

addr_width = 12
# Set instrn width to 8 byte
instrn_width = 64  # (in bits)
# 512 entries fixed
core_instrnMem_size = 512
# 1024 entries fixed
tile_instrnMem_size = 1024

# tile_buf_size = float('inf')
packet_size = 10  # in bits
packet_bw = float('inf')

DR_width = None
OR_width = None
IR_width = None

edram_buswidth = 64  # in bits
edram_width = 128
tile_buf_size = float('inf')
edram_max_latency = 2

# assume 2KB memory capacity =>
# as the core generates more output data
dataMem_size = 1280  #
# set the width to 16 bits => minimum transfer granularity

# the edram bus width is set to 256
edram_size = 32  # in Kilobytes (64 KB - same as isaac)
refresh_period = 10000000
refresh_latency = 0

# tile to tile NoC
tile_bw = 1
tile_delay = 1

# chip to chip off chip config
chip_bw = (1, 1)  # every 10 clock cycles
chip_delay = 1

# Change here - Specify the Node parameters here
max_tile_x_in_chip = None
max_tile_y_in_chip = None
max_switch_x_in_chip = None
max_switch_y_in_chip = None
num_tile_max = None

# 2D / 3D SELECTIVE PARAMS
num_matrix = 0
xbar_in_size = None
xbar_out_size = None
num_ima = None
num_ALU = None
xbar_bits = None
in_bits = None
max_latency = None

temporal_iter = None
spatial_iter = None


def change_config(simulator=None):
    global num_matrix
    global xbar_in_size
    global xbar_out_size
    global num_ima
    global num_ALU
    global xbar_bits
    global in_bits
    global max_latency
    global temporal_iter
    global spatial_iter

    global max_tile_x_in_chip
    global max_tile_y_in_chip
    global max_switch_x_in_chip
    global max_switch_y_in_chip
    global num_tile_max

    global DR_width
    global IR_width
    global OR_width

    num_matrix = 4
    xbar_in_size = 128
    xbar_out_size = 128
    num_ima = 12
    num_ALU = num_matrix
    xbar_bits = 2
    in_bits = 1
    max_latency = xbar_in_size * 2

    temporal_iter = int(math.ceil(input_prec / in_bits))
    spatial_iter = int(math.ceil((weight_prec - 1) / xbar_bits * 2))

    max_tile_x_in_chip = 12
    max_tile_y_in_chip = 12
    max_switch_x_in_chip = int(max_tile_x_in_chip / 2)
    max_switch_y_in_chip = int(max_tile_y_in_chip / 2)
    num_tile_max = max_tile_x_in_chip * max_tile_y_in_chip

    DR_width = 20
    OR_width = 64
    IR_width = 20
