import numpy as np
import GlobalVars as gv
import Profile as pf
import config as cfg
import Tile
import sys
import math


class Memory:
    def __init__(self, tile):
        self.tile = tile

        # map virtual address to the physical address (physical_addr (0), num_rows (1))
        self.virtual_to_physical = {}

        # physical memory (counter (0), virtual addr (1), image_id (2), data (3))
        self.physical_mem = {}
        self.free_space = {}
        self.max_physical_size = 0

        # move mem wait inside mem
        # self.mem_wait = 0 # how many cycles left to finish serving current request
        self.end_cyc = -1

        # pending addresses (used for event-driven simulation)
        self.pending_mem_tile = []
        self.pending_mem_core = [-1 for _ in range(gv.params["core_num"])]

        # pending addresses (for store)
        self.receive_pending_mem_tile = -1
        self.store_pending_mem_core = [-1 for _ in range(gv.params["core_num"])]

    def left_cycles(self):
        return self.end_cyc - gv.cyc

    def search_empty(self, num_rows):
        # get the target address
        if num_rows not in self.free_space:
            # if there is no proper free space => extend the mem
            target_addr = self.max_physical_size
        else:
            # if there is a free space => get it
            target_addr = self.free_space[num_rows].pop(0)
            # if it was the last free space => remote num_rows
            if len(self.free_space[num_rows]) == 0:
                self.free_space.pop(num_rows)

        return target_addr

    def free(self, physical_addr, num_rows):
        deleted = False
        delete_addr = self.physical_mem[physical_addr][1]
        image_id = self.physical_mem[physical_addr][2]

        # iterate over the physical memory and decrement the counter
        # for i in range(num_rows):
        assert (self.physical_mem[physical_addr][0] > 0)
        self.physical_mem[physical_addr][0] -= 1
        if self.physical_mem[physical_addr][0] == 0:
            deleted = True
            self.physical_mem.pop(physical_addr)
            assert (physical_addr not in self.physical_mem)

        if deleted:

            if num_rows in self.free_space:
                self.free_space[num_rows].append(physical_addr)
            else:
                self.free_space[num_rows] = [physical_addr]
            self.virtual_to_physical[delete_addr].pop(image_id)
            assert (image_id not in self.virtual_to_physical[delete_addr])

            if self.receive_pending_mem_tile >= 0:
                self.tile.noc.ctrl.insert_tile(num_rows * cfg.edram_max_latency, self.tile)
                self.receive_pending_mem_tile = -1

            for index in range(len(self.store_pending_mem_core)):
                pending_addr = self.store_pending_mem_core[index]
                if pending_addr == delete_addr:
                    self.tile.noc.ctrl.insert_core(num_rows * cfg.edram_max_latency, self.tile.cores[index])
                    self.tile.cores[index].pending = [False, num_rows * cfg.edram_max_latency]
                    self.store_pending_mem_core[index] = -1

    def allocate(self, cyc, virtual_addr, data_bits, counter, image_id, data=0, eDRAM_hop=1):
        # refresh
        # if cyc % cfg.refresh_period < cfg.refresh_latency:
        #    return False, -1

        # check alignment
        assert (virtual_addr % min(cfg.xbar_in_size, cfg.xbar_out_size) == 0)

        # if already exist => return false (cannont allocate the data)
        '''
        if(virtual_addr in self.virtual_to_physical):
            assert(self.physical_mem[self.virtual_to_physical[virtual_addr][0]][0] > 0)
            return False, -1
        '''

        num_rows = int(math.ceil(data_bits / cfg.edram_width))

        # add profiling (allocate num_rows)
        pf.call_stack["edram_write"] += num_rows
        pf.call_stack["edram_bus_hop"] += eDRAM_hop * num_rows * cfg.edram_width / cfg.edram_buswidth

        # search for the empty space & allocate to the physical memory
        #################
        # search empty
        target_addr = self.search_empty(num_rows)

        # allocate
        if virtual_addr not in self.virtual_to_physical:
            self.virtual_to_physical[virtual_addr] = {}
        self.virtual_to_physical[virtual_addr][image_id] = [target_addr, num_rows]
        '''
        self.virtual_to_physical[virtual_addr] = [target_addr, num_rows]
        '''
        # for i in range(num_rows):
        self.physical_mem[target_addr] = \
            [counter, virtual_addr, image_id, data]

        # renew the maximum size
        if target_addr + num_rows > self.max_physical_size:
            self.max_physical_size = target_addr + num_rows

        ################

        # activate the tile and core for the pending mem
        self.tile.noc.ctrl.insert_tile(num_rows * cfg.edram_max_latency, self.tile)

        for index in range(len(self.pending_mem_core)):
            pending_addr = self.pending_mem_core[index]
            if pending_addr == virtual_addr:
                self.tile.noc.ctrl.insert_core(num_rows * cfg.edram_max_latency, self.tile.cores[index])
                self.tile.cores[index].pending = [False, num_rows * cfg.edram_max_latency]
                self.pending_mem_core[index] = -1

        self.end_cyc = gv.cyc + num_rows * cfg.edram_max_latency

        return True, num_rows * cfg.edram_max_latency

    def exist(self, virtual_addr, image_id, dummy=False):
        if not dummy:
            pf.call_stack["attribute_only"] += 1
        if virtual_addr in self.virtual_to_physical and \
                image_id in self.virtual_to_physical[virtual_addr]:
            physical_addr = self.virtual_to_physical[virtual_addr][image_id][0]
            if image_id == self.physical_mem[physical_addr][2]:
                return True
        return False

    def access(self, cyc, virtual_addr, data_bits, image_id, core_id=-1, eDRAM_hop=1):

        # if the virtual address exist & the physical memory's counter is not zero
        if virtual_addr in self.virtual_to_physical and \
                image_id in self.virtual_to_physical[virtual_addr]:
            physical_addr = self.virtual_to_physical[virtual_addr][image_id][0]
            if image_id != self.physical_mem[physical_addr][2]:
                return False, -1, -1

            num_rows = self.virtual_to_physical[virtual_addr][image_id][1]
            # add profiling (allocate num_rows)
            pf.call_stack["edram_read"] += num_rows
            pf.call_stack["edram_bus_hop"] += eDRAM_hop * num_rows * cfg.edram_width / cfg.edram_buswidth

            data = self.physical_mem[physical_addr][3]

            self.free(physical_addr, num_rows)

            self.end_cyc = gv.cyc + num_rows * cfg.edram_max_latency

            return True, data, num_rows * cfg.edram_max_latency
        else:
            return False, -1, -1
