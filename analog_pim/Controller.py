import numpy as np
import Operations
import config as cfg
import GlobalVars as gv


class Controller:
    def __init__(self, noc):
        # Tiles to compute
        self.compute_length = Operations.latency["mvm"] * \
            cfg.temporal_iter * cfg.spatial_iter
        self.tiles_to_compute = [[] for _ in range(self.compute_length)]
        self.cores_to_compute = [[] for _ in range(self.compute_length)]
        self.switches_to_compute = [[] for _ in range(cfg.max_latency)]
        self.merge_split_to_compute = [[] for _ in range(cfg.max_latency)]

        self.noc = noc
        self.tiles = self.noc.tiles
        
        # initialize tiles / cores to compute
        for tile in self.tiles: 
            tile.ctrl = self
            for core in tile.cores: 
                core.ctrl = self
                core.mvmu.ctrl = self
                core.alu.ctrl = self
                core.reg.ctrl = self

        for tile in self.tiles:
            self.tiles_to_compute[0].append(tile)
            for core in tile.cores:
                self.cores_to_compute[0].append(core)

        for switch_x in range(gv.params["max_switch_x"]):
            for switch_y in range(gv.params["max_switch_y"]):
                self.switches_to_compute[0].append(self.noc.switches[switch_x][switch_y])

        for chip_x in range(gv.params["max_chip_x"]):
            for chip_y in range(gv.params["max_chip_y"]):
                for d in range(4):
                    self.merge_split_to_compute[0].append(self.noc.merge_split[chip_x][chip_y][d])

    def insert_tile(self, delay, tile):
        if cfg.SIM_ver == "Clock":
            return
        assert(delay < self.compute_length)
        index = (gv.cyc + delay) % self.compute_length
        if not (tile in self.tiles_to_compute[index]):
            self.tiles_to_compute[index].append(tile)

    def insert_core(self, delay, core):
        if cfg.SIM_ver == "Clock":
            return
        assert(delay < self.compute_length)
        index = (gv.cyc + delay) % self.compute_length
        if not (core in self.cores_to_compute[index]):
            self.cores_to_compute[index].append(core)

    def insert_switch(self, delay, switch):
        if cfg.SIM_ver == "Clock":
            return
        assert(delay < cfg.max_latency)
        index = (gv.cyc + delay) % cfg.max_latency
        if not (switch in self.switches_to_compute[index]):
            self.switches_to_compute[index].append(switch)

    def insert_merge_split(self, delay, merge_split):
        if cfg.SIM_ver == "Clock":
            return
        assert(delay < cfg.max_latency)
        index = (gv.cyc + delay) % cfg.max_latency
        if not (merge_split in self.merge_split_to_compute[index]):
            self.merge_split_to_compute[index].append(merge_split)
