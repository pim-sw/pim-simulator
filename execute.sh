#!/bin/sh

if [[ $1 == "APIM" ]]; then
    SIM_FOLDER=analog_pim
elif [[ $1 == "DPIM" ]]; then
    SIM_FOLDER=digital_pim_pnm
elif [[ $1 == "PNM" ]]; then
    SIM_FOLDER=digital_pim_pnm
else
    echo "Invalid simulator type. Should be either APIM, DPIM, or PNM"
    exit 1
fi

TRACE_FILE=$2
if ! [ -f $TRACE_FILE ]; then
    echo "The trace file does not exist."
    exit 1
elif [[ $TRACE_FILE == "" ]]; then
    echo "The trace file does not exist."
    exit 1
fi

# copy trace to the workload folder
cp $TRACE_FILE $SIM_FOLDER/workload/$1/
cd $SIM_FOLDER/workload/$1/
python3 gen_inst.py $TRACE_FILE

cd ../../
bash run_sim.sh $1 

cd ..
mkdir -p result
mkdir -p result/$1
cp $SIM_FOLDER/result/timing.txt ./result/$1
